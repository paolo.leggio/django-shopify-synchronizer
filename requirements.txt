#shopify
ShopifyAPI==3.1.0
algoliasearch==1.17.0
tabulate==0.8.2
django-q>=0.8.1

# Additional requirements go here
django>=1.10.8
django-model-utils>=2.0
jsonfield>=2.0.2
gql==0.1.0
graphql-core==2.1

boto3>=1.9.62
botocore>=1.12.62
deepdiff==3.3.0

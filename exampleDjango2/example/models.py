from django.db import models


class Cultivar(models.Model):
    slug = models.SlugField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    flower_color = models.TextField()

    def get_name(self):
        return self.name

    def get_flower_color(self):
        return self.flower_color


class Rose(models.Model):
    slug = models.SlugField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    flower_shape = models.TextField()

    def get_name(self):
        return self.name

    def get_flower_shape(self):
        return self.flower_shape

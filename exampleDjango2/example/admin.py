# -*- coding: utf-8 -*-
#

from django.contrib import admin
from .models import Cultivar, Rose

admin.site.register(Cultivar)
admin.site.register(Rose)

from __future__ import unicode_literals

from django.apps import AppConfig


class ExampleAppConfig(AppConfig):
    name = "example"

    def ready(self):
        from django_shopify_synchronizer.indexing.indexing import product_index

        Cultivar = self.get_model("Cultivar")
        Rose = self.get_model("Rose")

        product_index.register_tags(
            [
                {
                    "handler_key": "cultivar",
                    "model": Cultivar,
                    "fields": [
                        ["name", "get_name"],
                        ["flowerColor", "get_flower_color"],
                    ],
                },
                {
                    "handler_key": "Rosa",
                    "model": Rose,
                    "fields": [
                        ["name", "get_name"],
                        ["flowerShape", "get_flower_shape"],
                    ],
                },
            ]
        )

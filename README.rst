=============================
Django Shopify to Shopify synchronizer
=============================

.. image:: https://badge.fury.io/py/django-shopify-synchronizer.svg
    :target: https://badge.fury.io/py/django-shopify-synchronizer

.. image:: https://travis-ci.org/pa0lin082/django-shopify-synchronizer.svg?branch=master
    :target: https://travis-ci.org/pa0lin082/django-shopify-synchronizer

.. image:: https://codecov.io/gh/pa0lin082/django-shopify-synchronizer/branch/master/graph/badge.svg
    :target: https://codecov.io/gh/pa0lin082/django-shopify-synchronizer

Sync shopify to Shopify by webhook

Documentation
-------------

The full documentation is at https://django-shopify-synchronizer.readthedocs.io.

Quickstart
----------

Install Django Shopify to Shopify synchronizer::

    pip install django-shopify-synchronizer

Add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'django_shopify_synchronizer.apps.DjangoShopifySynchronizerConfig',
        ...
    )

Add Django Shopify to Shopify synchronizer's URL patterns:

.. code-block:: python

    from django_shopify_synchronizer import urls as django_shopify_synchronizer_urls


    urlpatterns = [
        ...
        url(r'^', include(django_shopify_synchronizer_urls)),
        ...
    ]

Features
--------

* TODO

Running Tests
-------------

Does the code actually work?

::

    source <YOURVIRTUALENV>/bin/activate
    (myenv) $ pip install tox
    (myenv) $ tox

Credits
-------

Tools used in rendering this package:

*  Cookiecutter_
*  `cookiecutter-djangopackage`_

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-djangopackage`: https://github.com/pydanny/cookiecutter-djangopackage

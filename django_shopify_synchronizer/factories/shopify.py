import factory
from .common import UserFactory
from .shop import ShopFactory
from django_shopify_synchronizer.models.shopify_models import (
    AbstractShopifyModel, Product, Customer
)


class ShopifyModelsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AbstractShopifyModel

    shopify_id = factory.Faker('pyint')
    shop = factory.SubFactory(ShopFactory)


class ProductFactory(ShopifyModelsFactory):
    class Meta:
        model = Product


class CustomerFactory(ShopifyModelsFactory):
    class Meta:
        model = Customer

    user = factory.SubFactory(UserFactory)
    data = {
        'fake': 'data'
    }

    #
    # username = factory.Faker('user_name')
    # first_name = factory.Faker('first_name')
    # last_name = factory.Faker('last_name')
    # email = factory.Faker('safe_email')

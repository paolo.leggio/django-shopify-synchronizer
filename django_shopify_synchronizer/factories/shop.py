import factory
from django_shopify_synchronizer.models.shop import Shop



class ShopFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Shop
        exclude = ['name']

    name = factory.Faker('hostname', levels=0)

    domain = factory.LazyAttribute(lambda a: '{}.myshopify.com'.format(a.name).lower())
    api_key = factory.Faker('pystr', min_chars=33, max_chars=33)
    password = factory.Faker('pystr', min_chars=33, max_chars=33)
    shared_secret = factory.Faker('pystr', min_chars=33, max_chars=33)
    signature = factory.Faker('pystr', min_chars=33, max_chars=33)
    storefront_access_token = factory.Faker('pystr', min_chars=33, max_chars=33)
    primary = True

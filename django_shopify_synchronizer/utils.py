import collections

def only(source, *keys):
    return {key: source[key] for key in keys if key in source}


def omit(source, *keys):
    return {key: source[key] for key in source.keys() if key not in keys}


def dict_update(d, u):
    for k, v in u.items():
        if isinstance(v, collections.Mapping):
            d[k] = dict_update(d.get(k, {}), v)
        else:
            d[k] = v
    return d

"""URLs for the review app."""
from django.conf.urls import url

from . import views


app_name = 'django_shopify_synchronizer'
urlpatterns = [
    # Todo: use only one of these urls
    url(r'^webhook/$', views.WebhookView.as_view(), name='webhook'),
    url(r'^shop/(?P<domain>[\w.@+-]+)/template.zip$', views.shop_template_zip, name='shop_template_zip'),

]

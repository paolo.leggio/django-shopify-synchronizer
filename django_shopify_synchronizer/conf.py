# Creare una app privata per ogni shop
# ottenere API_KEY, PASSWORD e SHARED_SECRET
#
# per generare dinamicamente i webhook aggiungere
# write_marketing_events, read_marketing_events
#
# per gestire i prodotti
# read_products, write_products



# conf = {
#     '__default__': {
#         'API_KEY': '86f4c513344756b4a57228f986ef6103',
#         'API_SECRET': 'c96e50b7da047d1c60acc552615f76b4',
#     },
#     'test-sync-s2s-dest.myshopify.com':{
#         'API_KEY': 'a4f66378ca81584d75ea189da0c3b825', # ottenute dalla private app dello shop
#         'PASSWORD': '74bf7078426eab655bcf9a9b433840a3',
#         'SHARED_SECRET': 'a6c0e077d128b0e2375287607f68591c',
#     },
#     'test-sync-s2s-source.myshopify.com': {
#         'API_KEY': '451a6bb8807ac28369cf26fd6483830b',  # ottenute dalla private app dello shop
#         'PASSWORD': 'f09d7eb4f4acb46398f38e6fdd1feea4',
#         'SHARED_SECRET': 'a46b674a8c91afd69c8e2b01a7ffb2bd',
#         'signature': 'de706f6a6055031155273b2bc1a5ae2addf152ea27079de95dcef779c21ab1a4',
#         'directive': {
#             'products/create': {
#                 'target': 'test-sync-s2s-dest.myshopify.com'
#             },
#             'products/update': {
#                 'target': 'test-sync-s2s-dest.myshopify.com'
#             }
#         }
#     }
# }

conf = {
    'test-sync-s2s-dest.myshopify.com': {
        'directive': {

        },
    },
    'test-sync-s2s-source.myshopify.com': {
        'directive': {
            'products/create': {
                'target': 'test-sync-s2s-dest.myshopify.com'
            },
            'products/update': {
                'target': 'test-sync-s2s-dest.myshopify.com'
            }
        }
    }
}

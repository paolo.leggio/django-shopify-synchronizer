from django.core.management.base import BaseCommand, CommandError
from django_shopify_synchronizer.models import Shop
from django_shopify_synchronizer.sqs import SqsWorker
import logging
import signal
import sys

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        # parser.add_argument('src', type=str)
        # parser.add_argument('dst', type=str)
        parser.add_argument(
            '-s', '--shop',
            dest='shop_domain',
            type=str,
            help='Define shop',
        )

        parser.add_argument(
            '-q', '--queue',
            dest='queue',
            type=str,
            help='Define shop',
            required=True
        )

    def handle_sigint(self, *args, **kwargs):
        self.worker.stop()

    def get_shop(self, **options):
        if options['shop_domain']:
            try:
                return Shop.objects.get(domain=options['shop_domain'])
            except Shop.DoesNotExist:
                raise CommandError('No Shop found with domain {} try -s [{}]'.format(
                    options['shop_domain'],
                    ' | '.join(list(Shop.objects.all().values_list('domain', flat=True))))
                )
        else:
            if Shop.objects.count() == 1:
                logger.info('Auto select default shop')
                return Shop.objects.last()
            else:
                raise CommandError('Multiple shop found in DB, define one with -s [{}]'.format(
                    ' | '.join(list(Shop.objects.all().values_list('domain', flat=True))))
                )

    def handle(self, *args, **options):
        shop = self.get_shop(**options)
        queue_name = options['queue']  # example 'scraped_queue'

        self.worker = SqsWorker(queue_name, shop)

        signal.signal(signal.SIGINT, self.handle_sigint)
        self.worker.run()

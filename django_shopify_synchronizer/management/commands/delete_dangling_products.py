# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand, CommandError
from django_shopify_synchronizer.models import Shop
import logging

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '-s', '--shop',
            dest='shop_domain',
            type=str,
            help='Define shop',
        )
        parser.add_argument(
            '-d', '--dry',
            dest='dry',
            action='store_true',
            help='Dry run',
        )

    def get_shop(self, **options):
        if options['shop_domain']:
            try:
                return Shop.objects.get(domain=options['shop_domain'])
            except Shop.DoesNotExist:
                raise CommandError('No Shop found with domain {} try -s [{}]'.format(
                    options['shop_domain'],
                    ' | '.join(list(Shop.objects.all().values_list('domain', flat=True))))
                )
        else:
            if Shop.objects.count() == 1:
                logger.info('Auto select default shop')
                return Shop.objects.last()
            else:
                raise CommandError('Multiple shop found in DB, define one with -s [{}]'.format(
                    ' | '.join(list(Shop.objects.all().values_list('domain', flat=True))))
                )

    def handle(self, *args, dry, **options):
        shop = self.get_shop(**options)
        shop.delete_dangling_products(dry)



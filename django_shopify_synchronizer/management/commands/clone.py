from django.core.management.base import BaseCommand, CommandError
import logging
from django_shopify_synchronizer.models import Shop

logger = logging.getLogger('shopify')


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('src', type=str)
        parser.add_argument('dst', type=str)

    def handle(self, *args, **options):
        print('shopify clone')
        try:
            src = Shop.objects.get(domain=options['src'])
        except Shop.DoesNotExist:
            raise CommandError('No Shop found with domain {}'.format(options['src']))

        try:
            dst = Shop.objects.get(domain=options['dst'])
        except Shop.DoesNotExist:
            raise CommandError('No Shop found with domain {}'.format(options['dst']))

        src.sync_in_shop(dst)

from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ImproperlyConfigured
import logging
import warnings
from ...conf import conf
from ...helpers import get_api_for_domain
# import shopify
import pprint
from django_shopify_synchronizer.models import Shop
from django_shopify_synchronizer.indexing.indexing import product_index
logger = logging.getLogger('shopify')



class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            '-s', '--shop',
            dest='shop_domain',
            type=str,
            help='Define shop',
        )

    def get_shop(self, **options):
        if options['shop_domain']:
            try:
                return Shop.objects.get(domain=options['shop_domain'])
            except Shop.DoesNotExist:
                raise CommandError('No Shop found with domain {} try -s [{}]'.format(
                    options['shop_domain'],
                    ' | '.join(list(Shop.objects.all().values_list('domain', flat=True))))
                )
        else:
            if Shop.objects.primary():
                logger.info('Auto select default shop')
                return Shop.objects.primary()
            else:
                raise CommandError('Multiple shop found in DB, define one with -s [{}]'.format(
                    ' | '.join(list(Shop.objects.all().values_list('domain', flat=True))))
                )



    def handle(self, *args, **options):
        print('Reindex products')
        shop = self.get_shop(**options)

        product_index.reindex_all_of_shop(shop)











from __future__ import unicode_literals
from django.core.management.base import BaseCommand, CommandError
import logging
from django_shopify_synchronizer.models import Shop
import re
import pprint
import datetime
from difflib import get_close_matches
import os
import time
import hashlib
from django.core.cache import cache
from django.utils.functional import wraps
from tabulate import tabulate

logger = logging.getLogger('shopify')


def cache_it(expires=300, key_func=None):
    def wrap(f):
        @wraps(f)
        def wrapper(*args, **kwds):
            if not cache:
                return f(*args, **kwds)

            if 'cache' in kwds and kwds['cache'] == False:
                # print('Call (NO CACHE)')
                # print(args,kwds)
                return f(*args, **kwds)

            if key_func:
                key = 'func:{}'.format(key_func(*args, **kwds))
            else:
                key = 'func:' + f.__name__ + ':' + \
                      str(list(args) + list(sorted(kwds.items())))
            key = hashlib.sha1(key.encode('utf-8')).hexdigest()
            value = cache.get(key)
            if value is None:
                # print(args, kwds)
                # print('Call (NO CACHE)')
                value = f(*args, **kwds)
                cache.set(key, value, expires)
                value = cache.get(key)
                if value is None:
                    raise Exception('failed to fetch cached value, try again')
            return value

        return wrapper

    return wrap


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('src', type=str)
        parser.add_argument('dst', type=str)
        # Named (optional) arguments
        parser.add_argument(
            '-d', '--dry',
            dest='dry',
            action='store_true',
            help='Dry run',
        )

        parser.add_argument(
            '-p', '--precision',
            type=float,
            dest='precision',
            default=0.8,
            # action='store_const',
            help='Close match precision',
        )

        parser.add_argument(
            '-c', '--cache-dst',
            # type=float,
            dest='cache_dest',
            # default=0.8,
            action='store_true',
            help='Cache dest',
        )

        parser.add_argument(
            '-l', '--limit',
            type=int,
            dest='limit',
            # default=0.8,
            # action='store_true',
            help='Limit',
        )

    def match_tag(self, tag, prefix):
        return re.match("^{}:[A-Za-z0-9-]+".format(prefix), tag)

    def get_valid_tags(self, tags, prefix):
        tags = [t.strip() for t in tags.split(',')]
        return [t for t in tags if self.match_tag(t, prefix)]

    @cache_it(3000, key_func=lambda x, shop, page, cache: 'shop_{}-page_{}'.format(shop.id, page))
    def get_products_page(self, shop, page, cache=True):
        return [i.to_dict() for i in shop.api.Product.find(limit=250, page=page)]

    def get_all_products(self, shop, cache=True, limit=None):
        page = 1

        while True:
            result = self.get_products_page(shop, page, cache=cache)
            # shop.api.Product.find(limit=250, page=page)
            page = page + 1

            if not result:
                break
            else:
                for item in result:
                    if limit is not None and limit <= 0:
                        return
                    #
                    # if limit is not None:
                    #     limit = limit - 1
                    yield item

    def make_title_tags_map(self, shop):
        map = {}
        i = 1
        for p in self.get_all_products(shop):
            tags = self.get_valid_tags(p.get('tags'), 'cultivar')
            if tags:
                map[p.get('title')] = {'tags': tags, 'match': 0}
            # print(i, ' src product:', tags)
            i = i + 1
        return map

    def get_shop(self, val):
        try:
            shop = Shop.objects.get(domain__icontains=val)
        except Shop.DoesNotExist:
            raise CommandError('No Shop found with domain {}'.format(val))
        except Shop.MultipleObjectsReturned:
            raise CommandError('Multiple Shop  match value {}'.format(val))

        if not shop:
            raise Exception('No shop found')

        return shop

    def init_log(self):
        date = datetime.datetime.today().strftime('%Y-%m-%d_%H-%M')
        directory = './logs/import_cultivar_logs-{}'.format(date)
        try:
            os.stat(directory)
        except:
            os.makedirs(directory)

        product_updated_path = '{}/product_updated.log'.format(directory)
        product_not_matched_path = '{}/product_not_matched.log'.format(directory)
        match_report_path = '{}/match_report.txt'.format(directory)

        self.product_updated_log = open(product_updated_path, 'w')
        self.product_not_matched_log = open(product_not_matched_path, 'w')
        self.match_report_log = open(match_report_path, 'w')


    def handle(self, dry, precision, cache_dest, limit, *args, **options):
        """
        Import cultivar reference:

        check products with tags cultivar:***** and replace product of destination shop with tag yg_data:cultivar:****


        :param args:
        :param options:
        :return:
        """

        self.init_log()
        print(dry, precision, cache_dest, limit, options)

        if dry:
            print ('-------- SAFE MODE (no product update) ----------')

        src = self.get_shop(options['src'])
        dst = self.get_shop(options['dst'])

        # try:
        #     src = Shop.objects.get(domain=options['src'])
        # except Shop.DoesNotExist:
        #     raise CommandError('No Shop found with domain {}'.format(options['src']))
        #
        # try:
        #     dst = Shop.objects.get(domain=options['dst'])
        # except Shop.DoesNotExist:
        #     raise CommandError('No Shop found with domain {}'.format(options['dst']))

        by_title_maps = self.make_title_tags_map(src)

        #
        # Todo: chech anche tramite lo SKU (che corrisponde al VendorProductId)


        table_data = []

        product_to_be_update = 0
        product_matched = 0
        product_count = 0

        for p in self.get_all_products(dst, cache=cache_dest, limit=limit):
            product_count += 1
            title = p.get('title')
            logger.warning('Check product [{}]'.format(title))

            if title in by_title_maps:
                print('Match perfect title')
                key = title
            else:
                matchs = get_close_matches(p.get('title'), by_title_maps.keys(), cutoff=precision)
                if matchs:
                    print('Match get_close_matches')
                    key = matchs[0]
                else:
                    key = None

            # matchs = get_close_matches(p.get('title'), by_title_maps.keys(), cutoff=precision)
            if key:
                product_matched += 1
                by_title_maps[key]['match'] = by_title_maps[key]['match'] + 1
                # key = matchs[0]
                tags = ['yg_data:{}'.format(t) for t in by_title_maps[key]['tags']]

                # print('Product [{}]: Match key:"{}" with tags: {}.  '.format(title, key, tags))

                curr_tags = [t.strip() for t in p.get('tags').split(',')]
                new_tags = list(set(curr_tags + tags))

                if set(curr_tags) != set(new_tags):
                    required_update = True
                    # print('Tags to update: {}'.format(new_tags))
                    product_to_be_update += 1
                    if not dry:
                        product = dst.api.Product()
                        product.id = p.get('id')
                        product.tags = new_tags
                        product.save()
                        time.sleep(0.5)
                else:
                    required_update = False
                    # print('Tags OK')
            else:
                key = None
                tags = []
                required_update = None
                # print('Product [{}] : NO MATCH'.format(title))

            table_data.append([title, key, ','.join(tags), required_update])

        # self.product_updated_log = open(product_updated_path, 'w')
        # self.product_not_matched_log = open(product_not_matched_path, 'w')
        # self.match_report_log = open(match_report_path, 'w')


        title = '\n{}\nProduct Updated   (precision match: {})  \n{}\n'.format('#'*50, precision, '#'*50)
        headers = ['Product', 'Matched product', 'Expected Tags', 'Required Update']
        product_updated_data = [r for r in table_data if r[3] == True]

        print(title)
        print(tabulate(product_updated_data, headers, tablefmt="fancy_grid"))
        self.product_updated_log.write(title)
        self.product_updated_log.write(tabulate(product_updated_data, headers, tablefmt="fancy_grid").encode('utf-8'))


        title = '\n{}\nSummary  \n{}\n'.format('#' * 50, '#' * 50)
        headers = ['Product Count', 'Matched products', 'Products to be update']
        summary_data = [[product_count, product_matched, product_to_be_update]]
        print(title)
        print(tabulate(summary_data, headers, tablefmt="fancy_grid"))
        self.product_updated_log.write(title)
        self.product_updated_log.write(tabulate(summary_data, headers, tablefmt="fancy_grid").encode('utf-8'))


        # log su file dei prodotti non trovati
        title = '\n{}\nProduct Not matched(precision match: {})  \n{}\n'.format('#' * 50, precision, '#' * 50)
        headers = ['Product', 'Matched product', 'Expected Tags', 'Required Update']
        product_not_matched_data = [r for r in table_data if r[3] != True]
        self.product_not_matched_log.write(title)
        self.product_not_matched_log.write(tabulate(product_not_matched_data, headers, tablefmt="fancy_grid").encode('utf-8'))





        # title = '\n{}\nSummary  \n{}\n'.format('#' * 50, '#' * 50)
        # headers = ['tile', 'tag', 'Match count']
        # summary_data = [[k[0], k[1]['tags'], k[1]['match']] for k in by_title_maps.items() if k[1]['match'] > 0]
        # print(title)
        # print(tabulate(table_data, headers, tablefmt="fancy_grid"))
        # self.product_updated_log.write(title)
        # self.product_updated_log.write(tabulate(summary_data, headers, tablefmt="fancy_grid").encode('utf-8'))
        #



        # Log report match e match count
        headers = ['tile', 'tag', 'Match count']
        match_report_table_data = [[k[0], k[1]['tags'], k[1]['match']] for k in by_title_maps.items()]
        self.match_report_log.write(tabulate(match_report_table_data, headers, tablefmt="fancy_grid").encode('utf-8'))

        # headers = ['Product Count', 'Matched products', 'Products to be update']
        # print(tabulate([[product_count, product_matched, product_to_be_update]], headers, tablefmt="fancy_grid"))





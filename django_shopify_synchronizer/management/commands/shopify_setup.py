from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ImproperlyConfigured
import logging
import warnings
from ...conf import conf
from ...helpers import get_api_for_domain
# import shopify
import pprint
from django_shopify_synchronizer.models import Shop
logger = logging.getLogger('shopify')



class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('url', type=str)



    def handle(self, *args, **options):
        print('setup shopify sync')
        url = options['url']
        # print('url', url)

        for shop in Shop.objects.all():
            shop.setup_webhook(url)













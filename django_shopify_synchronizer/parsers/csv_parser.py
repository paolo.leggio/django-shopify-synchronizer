# -*- coding: utf-8 -*-

import csv
import io
from .base import BaseParser
from django.db.models.fields.files import FieldFile

try:
    from django.utils.encoding import smart_bytes as smart_str, force_text as force_unicode
except:
    from django.utils.encoding import smart_str, force_unicode



def convert_string(s):
    return force_unicode(smart_str(s)).strip()



# #DA BUTTARE
# from django_shopify_synchronizer.parsers.csv_parser import CSVParser
# import csv
# import io
# import_source = ImportSource.objects.last()
#
# source = import_source.file
# print('source',type(source))
#
# file = source.file.file
#
# # file = source.file._open(mode='rt')
# # file._mode = 'rt'
# # file.open()
# print('file',type(file),file.mode)
#
# if 'b' in file.mode and hasattr(file,'_file') and isinstance(file._file, io.BytesIO):
#     file = io.TextIOWrapper(file._file ,encoding='utf-8')
#
#
# # source.file._mode = 'rt'
# # source.open(mode='rt')
# reader = csv.reader(file)
# next(reader)
#
# ###



class CSVParser(BaseParser):
    """
    Import models from a local CSV file. Requires `csv` module.
    """

    def load(self, source):
        """
        Opens the source file.
        """

        if isinstance(source, FieldFile):
            file = source.file.file
            if 'b' in file.mode:
                if isinstance(file,io.BufferedReader):
                    file = io.TextIOWrapper(file, encoding='utf-8')

                elif hasattr(file, '_file') and isinstance(file._file, io.BytesIO):
                    file = io.TextIOWrapper(file._file, encoding='utf-8')

            self.file = file

        else:
            self.file = open(self.source, 'rt')
        self.loaded = True

    def unload(self):
        """
        Closes the input file to free resources.
        """
        self.file.seek(0)
        self.file.close()
        self.loaded = False

    def get_items(self):
        """
        Iterator to read the rows of the CSV file.
        """
        if not self.loaded:
            self.load(self.source)
        # Get the csv reader
        # print('self.source', self.source, type(self.source))
        reader = csv.reader(self.file)
        # Get the headers from the first line
        # print('reader:',reader, self.source.path)
        headers = next(reader)
        # headers = reader.next()
        # Read each line yielding a dictionary mapping
        # the column headers to the row values
        for row in reader:
            # Skip empty rows
            if not row:
                continue
            yield dict(zip(headers, row))
        self.unload()

    def get_value(self, item, source_name):
        """
        This method receives an item from the source and a source name,
        and returns the text content for the `source_name` node.
        """
        val = item.get(source_name.encode('utf-8'), None)
        if val is not None:
            val = convert_string(val)
        return val

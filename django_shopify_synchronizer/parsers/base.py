# -*- coding: utf-8 -*-
import logging
from traceback import format_exception
from cerberus import Validator
from collections import OrderedDict
import sys
import pprint

logger = logging.getLogger(__name__)


class CustomDict(dict):
    def update_if_not_set(self, data):
        for k, v in data.items():
            if k not in self:
                self[k] = v


class ParsingError(Exception):
    pass


class BaseParser(object):
    """
    Base class to create importers for different sources.
    """

    fields = ()
    field_map = {}
    unique_fields = ()
    model = None

    def __init__(self, source=None):
        self.source = source
        self.loaded = False
        self.errors = []

    def save_error(self, data, exception_info):
        """
        Saves an error in the error list.
        """
        # TODO: what to do with errors? Let it flow? Write to a log file?
        self.errors.append({'data': data,
                            'exception': ''.join(format_exception(*exception_info)),
                            })

    def parse(self):
        """
        Parses all data from the source, saving model instances.
        """
        # Checks if the source is loaded
        if not self.loaded:
            self.load(self.source)

        for item in self.get_items():
            # Parse the fields from the source into a dict
            data = self.parse_item(item)
            # Get the instance from the DB, or a new one
            instance = self.get_instance(data)
            # Feed instance with data
            self.feed_instance(data, instance)
            # Try to save the instance or keep the error
            try:
                self.save_item(item, data, instance)
            except Exception as e:
                self.save_error(data, sys.exc_info())

        # Unload the source
        self.unload()

    def parse_item(self, item):
        """
        Receives an item and returns a dictionary of field values.
        """
        # Create a dictionary from values for each field
        parsed_data = {}

        for field_name in self.fields:
            # A field-name may be mapped to another identifier on the source,
            # it could be a XML path or a CSV column name / position.
            # Defaults to the field-name itself.
            source_name = self.field_map.get(field_name, field_name)

            # Uses a custom method "parse_%(field_name)"
            # or get the value from the item
            parse = getattr(self, 'parse_%s' % field_name, None)
            if parse:
                value = parse(item, field_name, source_name)
            else:
                value = self.get_value(item, source_name)

            # Add the value to the parsed data
            parsed_data[field_name] = value
        return parsed_data

    def get_instance(self, data):
        """
        Get an item from the database or an empty one if not found.
        """
        # Get unique fields
        unique_fields = self.unique_fields

        # If there are no unique fields option, all items are new
        if not unique_fields:
            return self.model()

        # Build the filter
        filter = dict([(f, data[f]) for f in unique_fields])

        # Get the instance from the DB or use a new instance
        try:
            instance = self.model._default_manager.get(**filter)
        except self.model.DoesNotExist:
            return self.model()

        return instance

    def feed_instance(self, data, instance):
        """
        Feeds a model instance using parsed data (usually from `parse_item`).
        """
        for prop, val in data.items():
            setattr(instance, prop, val)
        return instance

    def save_item(self, item, data, instance, commit=True):
        """
        Saves a model instance to the database.
        """
        if commit:
            instance.save()
        return instance

    def unload(self):
        """
        Unloads the source file.
        Useful to close open files and free resources.
        Not a required method by subclasses.
        Rembember to unset the ``loaded`` when extending this method.
        """
        self.loaded = False

    ### ABSTRACT METHODS #############

    def load(self, source):
        """
        Load data from the source.

        Must be implemented on subclasses or will raise `NotImplementedError`.
        """
        raise NotImplementedError

    def get_items(self):
        """
        Get the list of items for the current source.
        Must return an iterable.

        Must be implemented on subclasses or will raise `NotImplementedError`.
        """
        raise NotImplementedError

    def get_schema(self):
        to_bool = lambda v: v.lower() in ('true', '1')

        def to_int(v):
            if v:
                return int(v)
            else:
                return -1

        to_str_list = lambda v: [t.strip() for t in v.split(',')]

        schema = {
            # Remap fields name
            'Title': {'rename': 'title'},
            'Tags': {'rename': 'tags'},
            'Published': {'rename': 'published'},
            'Body (HTML)': {'rename': 'body_html'},
            'Type': {'rename': 'product_type'},

            'VendorProductId': {'type': 'string', 'coerce': str, 'required': True},
            'Variant Grams': {'rename': 'variant_grams'},
            'Variant Price': {'rename': 'variant_price'},
            'Variant SKU': {'rename': 'variant_sku'},
            'Variant Requires Shipping': {'rename': 'variant_requires_shipping'},
            'Variant Compare At Price': {'rename': 'variant_compare_at_price'},
            'Variant Taxable': {'rename': 'variant_taxable'},
            'Variant Inventory Qty': {'rename': 'variant_inventory_quantity'},

            'Option1 Name': {'rename': 'option1_name'},
            'Option2 Name': {'rename': 'option2_name'},
            'Option3 Name': {'rename': 'option3_name'},
            'Option1 Value': {'rename': 'option1_value'},
            'Option2 Value': {'rename': 'option2_value'},
            'Option3 Value': {'rename': 'option3_value'},

            'Image Alt Text': {'rename': 'image_alt_text'},
            'Image Position': {'rename': 'image_position'},
            'Image Src': {'rename': 'image_src'},

            # Check value type
            'title': {'type': 'string', 'coerce': str},
            'body_html': {'type': 'string', 'coerce': str},
            'product_type': {'type': 'string', 'coerce': str},
            'published': {'type': 'boolean', 'coerce': (str, to_bool)},
            'tags': {'type': 'list', 'coerce': (str, to_str_list)},

            'variant_grams': {'type': 'float', 'coerce': float},
            'variant_price': {'type': 'float', 'coerce': float},
            'variant_inventory_quantity': {'type': 'integer', 'coerce': (str, to_int), 'nullable': True},
            'variant_sku': {'type': 'string', 'coerce': str},
            'variant_requires_shipping': {'type': 'boolean', 'coerce': (str, to_bool)},
            'variant_compare_at_price': {'type': 'float', 'coerce': float},
            'variant_taxable': {'type': 'boolean', 'coerce': (str, to_bool)},
            'option1_name': {'type': 'string', 'coerce': str},
            'option2_name': {'type': 'string', 'coerce': str},
            'option3_name': {'type': 'string', 'coerce': str},
            'option1_value': {'type': 'string', 'coerce': str},
            'option2_value': {'type': 'string', 'coerce': str},
            'option3_value': {'type': 'string', 'coerce': str},
            'image_alt_text': {'type': 'string', 'coerce': str},
            'image_position': {'type': 'string', 'coerce': str},
            'image_src': {'type': 'string', 'coerce': str},

        }
        return schema

    def get_validator(self):
        schema = self.get_schema()
        validator = Validator(schema, purge_unknown=True)
        validator.allow_unknown = False
        return validator

    def normalize_item(self, item, validator):
        # remove null values
        normalized_item = {k: v for k, v in item.items() if v}
        # rename key
        normalized_item = validator.normalized(normalized_item)
        if normalized_item is None:
            raise Exception('Normalize error: {}'.format(str(validator.errors)))

        return normalized_item

    def get_validated_items(self):
        validator = self.get_validator()

        valid_items = []
        invalid_items = []

        for idx, item in enumerate(self.get_items()):
            item = self.normalize_item(item, validator)
            if validator.validate(item):
                valid_items.append(item)
            else:
                invalid_items.append({
                    'idx': idx,
                    'errors': validator.errors})

        if len(invalid_items):
            raise ParsingError(invalid_items)

        return valid_items

    def group_by_key(self, items, key):
        # raggruppa lista per chiave

        od = OrderedDict()

        for each in items:
            if each[key] in od:
                od[each[key]].append(each)
            else:
                od[each[key]] = [each]

        return od

    def extract_variant_and_options(self, item):
        variant = {
        }
        options = []

        # extract variant attr from row and insert in variant object
        for key in ['grams', 'price', 'sku', 'requires_shipping', 'compare_at_price', 'taxable', 'inventory_quantity']:
            ext_name = 'variant_{}'.format(key)
            if ext_name in item:
                variant[key] = item.pop(ext_name)

        for optIdx in range(1, 4):
            value_key = 'option{}_value'.format(optIdx)
            name_key = 'option{}_name'.format(optIdx)
            variant_option_key = 'option{}'.format(optIdx)

            opt_value = item.pop(value_key, None)
            opt_name = item.pop(name_key, None)

            if opt_value and opt_name:
                options.append({
                    'name': opt_name
                })
                variant[variant_option_key] = opt_value

        if variant.get('inventory_quantity') and variant.get('inventory_quantity') > 0:
            variant['inventory_management'] = 'shopify'

        if variant.keys():
            if 'title' in item:
                variant['title'] = item.get('title')
        else:
            variant = None

        return variant, options

    def extract_image(self, item):
        image = {}
        if 'image_src' in item:
            image['src'] = item.pop('image_src')
            image['position'] = 1

        if 'image_position' in item:
            image['position'] = item.pop('image_position')

        if 'image_alt_text' in item:
            alt_text = item.pop('image_alt_text')
            # image['alt_text'] = alt_text

        return image

    def normalize_product(self, product):
        # logger.warning('normalize_product is not implemented!! always return same object')
        # print('product:',product)

        if 'options' in product and len(product['options']) == 0:
            del product['options']
        return product

    def validate_product(self, product):
        # logger.warning('validate_product is not implemented!! always return True')
        return product

    def merge_items_in_product(self, items, VendorProductId):
        product = CustomDict({
            'VendorProductId': VendorProductId,
            'images': [],
            'options': [],
            'variants': []
        })

        max_score = 0
        for itemIdx, item in enumerate(items):

            variant, options = self.extract_variant_and_options(item)

            if variant:
                variant['position'] = len(product['variants']) + 1
                product['variants'].append(variant)

            if options:
                for o in options:
                    try:
                        product['options'].index(o)
                    except ValueError:
                        product['options'].append(o)


            image = self.extract_image(item)
            if image:
                image['position'] = len(product['images']) + 1
                product['images'].append(image)

            # Per definire la riga principale assegno uno scor ad ogni item.
            # Score definito dal numero delle chiavi presenti
            # Piu 1 se é la prima riga del gruppo
            # L'item con score piú alto aggiorna le info base di prodotto'
            score = len(item.keys()) + (1 if itemIdx == 0 else 0)

            if score >= max_score:
                product.update(item)
                max_score = score
            else:
                product.update_if_not_set(item)
        return product

    def get_products(self):
        """
        Normalize items and extract product info.
        Use cerberos
        :return:
        """

        # Normalizza e convalida le righe ottenute dal parese
        valid_items = self.get_validated_items()

        # Raggruppa gli item con lo stesso VendorProductId
        grouped_items = self.group_by_key(valid_items, 'VendorProductId')

        products = []
        for VendorProductId, items in grouped_items.items():
            product = self.merge_items_in_product(items, VendorProductId)
            product = self.normalize_product(product)
            if self.validate_product(product):
                products.append(product)

        return products

    def get_value(self, item, source_name):
        """
        Receives an item from the source and a source name,
        and must return the value for the field.

        Must be implemented on subclasses or will raise `NotImplementedError`.
        """
        raise NotImplementedError

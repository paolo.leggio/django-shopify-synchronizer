# -*- coding: utf-8
from django.apps import AppConfig



class DjangoShopifySynchronizerConfig(AppConfig):
    name = 'django_shopify_synchronizer'

    def ready(self):
        print('######## DjangoShopifySynchronizerConfig READY')
        from django_shopify_synchronizer.indexing.indexing import setup_indexing
        from django_shopify_synchronizer.syncronizer import setup_syncronizer
        setup_indexing()
        setup_syncronizer()

import json
import pprint
import logging
from functools import wraps

from django.http import HttpResponseNotAllowed, HttpResponseBadRequest

from django_shopify_synchronizer.conf import conf
from django_shopify_synchronizer.helpers import domain_is_valid, hmac_is_valid
from .models import Shop
logger = logging.getLogger(__name__)


# todo: insert conf in settings or in db models


def get_signature(domain):
    domain_settings = conf[domain]
    signature = domain_settings['signature']
    return signature


def shopify_webhook(f):
    """
    A view decorator that checks and validates a Shopify Webhook request.
    """

    @wraps(f)
    def wrapper(request, *args, **kwargs):
        # Ensure the request is a POST request.
        if request.method != 'POST':
            return HttpResponseNotAllowed(permitted_methods=['POST'])

        shopify = {}
        try:
            shopify['topic'] = request.META['HTTP_X_SHOPIFY_TOPIC']
            shopify['shop_domain'] = request.META['HTTP_X_SHOPIFY_SHOP_DOMAIN']
            shopify['hmac'] = request.META[
                'HTTP_X_SHOPIFY_HMAC_SHA256'] if 'HTTP_X_SHOPIFY_HMAC_SHA256' in request.META else None
            shopify['data'] = json.loads(request.body.decode('utf-8'))

            shopify['shop'], shopify['shop_is_created'] = Shop.objects.get_or_create(domain=shopify['shop_domain'])

        except (KeyError, ValueError) as e:
            logger.exception('Shopify webhook incomplete data')
            return HttpResponseBadRequest()

        if shopify:
            request.shopify = shopify
        else:
            logger.error('Invalid shopify webhook')
            return HttpResponseBadRequest('Invalid shopify webhook')

        # Verify the domain.
        if not domain_is_valid(shopify['shop_domain']):
            logger.error('Invalid shopify domain [{}] '.format(shopify['shop_domain']))
            return HttpResponseBadRequest('Invalid domain')

        # Verify the HMAC.
        if not hmac_is_valid(request.body, shopify['shop'].signature, shopify['hmac']) and \
            not hmac_is_valid(request.body, shopify['shop'].shared_secret, shopify['hmac']):
            logger.error('Domain hmac_is_invalid for shop {}'.format(shopify['shop_domain']))
            return HttpResponseBadRequest('Hmac integrity error')

        return f(request, *args, **kwargs)

    return wrapper

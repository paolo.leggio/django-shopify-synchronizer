from django import forms
from .models import Shop



class SyncShopToShopForm(forms.Form):

    src_shops = forms.ModelMultipleChoiceField(Shop.objects, widget=forms.MultipleHiddenInput())
    dst_shops = forms.ModelChoiceField(Shop.objects)

    def __init__(self, data=None, initial=None, *args, **kwargs):
        if data:
            src_shops = data.get('src_shops', None)
        elif initial:
            src_shops = initial.get('src_shops', None)
        else:
            src_shops = None


        super(SyncShopToShopForm, self).__init__(data=data  , initial=initial, *args, **kwargs)

        if src_shops:
            self.fields['dst_shops'].queryset = Shop.objects.exclude(pk__in=[s.id if isinstance(s, Shop) else s for s in src_shops])

    def save(self):
        print('form.save')

        print('self.cleaned_data', self.cleaned_data)

        for src_shop in self.cleaned_data.get('src_shops'):

            src_shop.sync_in_shop(self.cleaned_data.get('dst_shops'))

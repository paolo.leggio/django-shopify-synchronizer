# -*- coding: utf-8 -*-
from django_shopify_synchronizer.utils import only, omit

import pyactiveresource.connection
from shopify.base import ShopifyConnection
from shopify.resources.product import Product as OrigProduct
from shopify.resources.customer import Customer
from shopify.resources.variant import Variant as OrigVariant
from shopify.resources.image import Image as OrigImage
from shopify.resources.custom_collection import CustomCollection as OrigCustomCollection
from shopify.resources.smart_collection import SmartCollection as OrigSmartCollection
from shopify.resources.customer import Customer as OrigCustomer
from shopify.resources import ShopifyResource
import shopify
import logging
import os
import time

try:
    from urllib.parse import urlparse
except:
    from urlparse import urlparse

# Get an instance of a logger
logger = logging.getLogger(__name__)
import pprint

SLEEP = 0.2


def patch_shopify_with_limits():
    func = ShopifyConnection._open

    def patched_open(self, *args, **kwargs):
        time.sleep(SLEEP)
        while True:
            try:
                return func(self, *args, **kwargs)

            except pyactiveresource.connection.ClientError as e:
                if e.response.code == 429:
                    retry_after = float(e.response.headers.get('Retry-After', 4))
                    print('Service exceeds Shopify API call limit, '
                          'will retry to send request in %s seconds' % retry_after)
                    time.sleep(retry_after)
                else:
                    raise e

    ShopifyConnection._open = patched_open


patch_shopify_with_limits()


class ImageNotFound(Exception):
    pass


class MultipleImageFound(Exception):
    pass


class VariantNotFound(Exception):
    pass


class MultipleVariantFound(Exception):
    pass


class CustomerAlreadyExist(Exception):
    pass


class MultipleCustomerFound(Exception):
    pass


class CustomerNotExist(Exception):
    pass


class InvalidEmail(Exception):
    pass


class BaseShopifyResource():
    @classmethod
    def find_all(cls, limit=250, from_=None, **kwargs):
        if limit > 250:
            raise ValueError('Max shopify chunk size: 250')

        page = 1
        while True:
            result = cls._find_every(from_=from_, limit=limit, page=page, **kwargs)
            page = page + 1
            if not result:
                break

            break_after_stop_iteration = (len(result) < limit)
            for item in result:
                yield item

            if break_after_stop_iteration:
                break


class Image(BaseShopifyResource, OrigImage):
    def destroy(self):
        if 'product_id' not in self._prefix_options:
            self._prefix_options['product_id'] = self.product_id
        return super(Image, self).destroy()


class Variant(BaseShopifyResource, OrigVariant):
    def save(self):
        if 'inventory_quantity' in self.attributes:
            del self.attributes['inventory_quantity']
        return super(Variant, self).save()


class Product(BaseShopifyResource, OrigProduct):
    data_map = True

    def save(self):
        if 'variants' in self.attributes:
            for variant in self.attributes['variants']:
                try:
                    del variant.attributes['inventory_quantity']
                except:
                    pass
        if getattr(self, 'variants', None):
            for variant in self.variants:
                try:
                    del variant.attributes['inventory_quantity']
                except:
                    pass
        return super(Product, self).save()

    def find_variant_by_options(self, options):
        res = [v for v in self.variants if
               all([getattr(v, opt_key, None) == opt_val for (opt_key, opt_val) in options.items()])]
        if len(res) == 1:
            return res[0]
        elif len(res) == 0:
            raise Exception('Variant not found for options:{}'.format(options))
        else:
            raise Exception('Multiple Variant  found for options:{}'.format(options))

    def _get_image_filename(self, image):
        if isinstance(image, ShopifyResource):
            url = image.src
        elif isinstance(image, dict):
            url = image.get('src')
        path = urlparse(url).path
        filename = os.path.basename(urlparse(path).path)
        return filename

    def _get_image_coordinates(self, image):
        fname = self._get_image_filename(image)
        if isinstance(image, ShopifyResource):
            w = image.width
            h = image.height
        elif isinstance(image, dict):
            w = image.get('width')
            h = image.get('height')

        return {
            'filename': fname,
            'width': w,
            'height': h
        }

    def _search_image(self, image_data, list=None):
        src_image_coordinate = self._get_image_coordinates(image_data)
        # print('_search_image with coordinates:',src_image_coordinate)

        img_list = list or self.images

        matched_images = []
        for i in img_list:
            if self._get_image_coordinates(i) == src_image_coordinate:
                matched_images.append(i)

        if len(matched_images) == 1:
            return matched_images[0]
        elif len(matched_images) == 0:
            raise ImageNotFound('Image not found')
        else:
            raise MultipleImageFound(
                'Multiple Image  found for filename:{}, width:{}, height:{}'.format(src_image_filename, src_img_width,
                                                                                    src_img_height))

    def _remap_variant_ids(self, id_list):
        res = []
        for orig_id in id_list:
            orig_variant = next((x for x in self._get_clone_variant_map() if x.get('id') == orig_id), None)
            options = only(orig_variant, *['option1', 'option2', 'option3'])
            variant = self.find_variant_by_options(options)
            res.append(variant.id)
        return res

    def _assert_clone_data_map(self):
        assert self.data_map, 'No data map available for sync'
        assert isinstance(self.data_map, dict), 'Invalid data map'

    def _get_clone_variant_map(self):
        self._assert_clone_data_map()
        return self.data_map.get('variants')

    def _get_clone_images_map(self):
        self._assert_clone_data_map()
        return self.data_map.get('images')

    def _get_product_data_for_sync(self, from_dict=False):

        if from_dict:
            self.data_map = self.to_dict()

        self._assert_clone_data_map()

        variants = self.data_map.get('variants', [])
        options = self.data_map.get('options', [])

        product_data = only(self.data_map, *[
            'body_html',
            'handle',
            'product_type',
            'tags',
            'title',
            'vendor',
            'options'
        ])

        cleaned_options = []
        for op in options:
            op = only(op, *[
                'name',
                'values',
                'position'
            ])
            cleaned_options.append(op)

        cleaned_variants = []
        for v in variants:
            v = omit(v, *['id',
                          'product_id',
                          'inventory_quantity',
                          'inventory_item_id',
                          'image_id'
                          ])
            cleaned_variants.append(v)

        product_data['options'] = cleaned_options
        product_data['variants'] = cleaned_variants
        return product_data

    def sync_product(self):
        product_data = self._get_product_data_for_sync()
        for k, v in product_data.items():
            setattr(self, k, v)

        if not self.save():
            print(self.errors.full_messages())
            raise Exception('Error updating product')

    def sync_variants_quanitity(self, location=None):
        default_location = location or shopify.Location.find()[0]
        # Iterate variants
        for orig_variant in self._get_clone_variant_map():
            inventory_management = orig_variant.get('inventory_management', 0)

            # Se shopify gestisce il magazzino aggiorno le quantita
            if inventory_management == 'shopify':
                # print('try to update orig_variant:')
                # pprint.pprint(orig_variant)

                quantity = orig_variant.get('inventory_quantity', 0)
                options = only(orig_variant, *['option1', 'option2', 'option3'])

                variant = self.find_variant_by_options(options)
                try:
                    inventory_item_id = variant.inventory_item_id
                except:
                    raise Exception('variant withouth inventory_item_id')

                shopify.InventoryLevel.set(**{
                    'inventory_item_id': inventory_item_id,
                    'location_id': default_location.id,
                    'available': quantity,
                    'disconnect_if_necessary': True
                })

    def sync_connected_images(self):

        # clean images
        # Per ogni immagine del prodotto, controlla se é presente nella nuova images_map o la rimuove
        images_map = self._get_clone_images_map()
        # print('images_map to sync:')
        # pprint.pprint(images_map)

        for i in self.images:
            try:
                self._search_image(i, images_map)
            except ImageNotFound:
                i.destroy()

        for i in images_map:
            position = i.get('position', None)
            orig_variant_ids = i.get('variant_ids', None)
            variant_ids = self._remap_variant_ids(orig_variant_ids)

            try:
                image = self._search_image(i)
            except ImageNotFound:
                print('Upload new image')
                img_url = i.get('src', None)
                if img_url:
                    if img_url.startswith('//'):
                        img_url = 'http:' + img_url

                    img_data = {
                        'product_id': self.id,
                        'src': img_url,
                        'variant_ids': variant_ids,
                        'position': position
                    }
                    image = shopify.Image(img_data)

                    if image.save() == False:
                        print(image.errors.full_messages())
                        print('Error upload image')
            except Exception as e:
                raise e

            # Check connection with variants
            if set(variant_ids) != set(image.variant_ids):
                print('Save image variant connection')
                image.variant_ids = variant_ids
                image.save()

    def sync_from_clone_data_map(self, data_map):
        self.data_map = data_map
        self.sync_product()
        self.sync_variants_quanitity()
        self.sync_connected_images()

    @classmethod
    def create_from_clone_data_map(cls, data):
        resource = shopify.Product()
        resource.sync_from_clone_data_map(data)
        return resource

    def update_from_clone_data_map(self, data):
        self.sync_from_clone_data_map(data)
        return self


class Customer(BaseShopifyResource, OrigCustomer):
    @classmethod
    def get_by_email(cls, email):
        results = cls.search(query='email:{}'.format(email))

        if len(results) == 0:
            raise CustomerNotExist()
        elif len(results) == 1:
            return results[0]
        else:
            raise MultipleCustomerFound()

    def save(self):
        success = super(Customer, self).save()

        if not success:
            print('self.errors.errors', self.errors.errors)
            if 'email' in self.errors.errors:
                if u'has already been taken' in self.errors.errors['email']:
                    raise CustomerAlreadyExist()
                if u'is invalid' in self.errors.errors['email']:
                    raise InvalidEmail()

        # if not success and self.errors.errors.has_key('email') and u'has already been taken' in self.errors.errors['email']:
        #     raise CustomerAlreadyExist()
        #
        # if not success and self.errors.errors.has_key('email') and u'email is invalid' in self.errors.errors[
        #     'email']:
        #     raise InvalidEmail()

        return success


class BaseCollection:
    def all_products(self, chunk_size=250):
        return shopify.Product.find_all(limit=chunk_size, collection_id=self.id)


class SmartCollection(BaseShopifyResource, BaseCollection, OrigSmartCollection):
    pass


class CustomCollection(BaseShopifyResource, BaseCollection, OrigCustomCollection):
    pass


setattr(shopify, 'Product', Product)
setattr(shopify, 'Variant', Variant)
setattr(shopify, 'Image', Image)
setattr(shopify, 'Customer', Customer)
setattr(shopify, 'SmartCollection', SmartCollection)
setattr(shopify, 'CustomCollection', CustomCollection)

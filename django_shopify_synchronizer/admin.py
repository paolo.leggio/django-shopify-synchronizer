# -*- coding: utf-8 -*-
#
import logging
from django.contrib import admin
from django.contrib import messages
from django import forms
from django_shopify_synchronizer.models import Shop, Product, Vendor, ImportSource, \
    ImportTask, ExternalProductToShopifyProductMap, AlgoliaApplication
import pprint
from django.http import HttpResponse, HttpResponseRedirect
from django.template.response import TemplateResponse
from .admin_forms import SyncShopToShopForm
try:
    from django.urls import reverse
except:
    from django.core.urlresolvers import reverse
from django.conf.urls import url
from django.utils.html import format_html, linebreaks
logger = logging.getLogger(__name__)


class ShopAdmin(admin.ModelAdmin):

    actions = ['setup_webhook']

    def setup_webhook(self, request, queryset):
        logger.info('setup_webhook to domain {}'.format(request.META['HTTP_HOST']))
        if '127.0.0.1' in request.META['HTTP_HOST'] or 'localhost' in request.META['HTTP_HOST'] or 'local' in \
            request.META['HTTP_HOST']:
            self.message_user(request,
                              "Webhooks configured to non-public addresses do not work. For testing, try to activate an http tunnel. (ngrok  http {})".format(
                                  request.META['HTTP_ORIGIN']),
                              level=messages.WARNING)
        for shop in queryset:
            try:
                webhooks = shop.setup_webhook(request=request)
                if isinstance(webhooks, (list, tuple)):
                    self.message_user(request, '{} - {} webhook activated'.format(shop, len(webhooks)))
            except Exception as e:
                self.message_user(request, e.message, level=messages.ERROR)

                # print('setup_webhook',base_url, request, queryset)

    setup_webhook.short_description = "Configure webhook listener on shopify"

    def get_form(self, request, obj=None, **kwargs):
        # just save obj reference for future processing in Inline
        request._obj_ = obj
        return super(ShopAdmin, self).get_form(request, obj, **kwargs)




class ProductAdmin(admin.ModelAdmin):
    # raw_id_fields = ("clones",)
    pass




class ImportTaskAdmin(admin.ModelAdmin):
    list_display = ['pk', 'task_id', 'import_source', 'target_shop', 'started', 'stopped', 'success', 'processed_count',
                    'imported_count', 'updated_count', 'failed_count', 'task_result_preview_html']
    actions = ['run', 'force_re_run']

    change_form_template = "django_shopify_synchronizer/admin/import_task/change_form.html"


    # def change_view(self, request, object_id, form_url='', extra_context=None):
    #     result = super(ImportTaskAdmin, self).change_view()

    def run(self, request, queryset):
        for task in queryset:
            task.run()
    run.short_description = "Execute task"

    def force_re_run(self, request, queryset):
        for task in queryset:
            task.reset()
            task.run()
    force_re_run.short_description = "Force re running task"

    def task_result_preview_html(self, obj):

        return obj.get_preview_html()
    task_result_preview_html.allow_tags = True

admin.site.register(Shop, ShopAdmin)
admin.site.register(Product, ProductAdmin)
admin.site.register(Vendor)
admin.site.register(AlgoliaApplication  )
admin.site.register(ImportSource)
admin.site.register(ImportTask, ImportTaskAdmin)
admin.site.register(ExternalProductToShopifyProductMap)

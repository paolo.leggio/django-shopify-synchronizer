import sys
import importlib.util
from django.template.backends.base import BaseEngine
from django.template.engine import Engine
from django.template.base import Node
from django.template.loaders.filesystem import Loader
from django.template.loaders.base import TemplateDoesNotExist
from django.utils.encoding import force_text
from django.utils.safestring import mark_safe

# Load python file
spec = importlib.util.find_spec('django.template.base')
# Extract source of python modules
source = spec.loader.get_source('django.template.base')

# Modify templatetags
new_source = source\
    .replace('{{','<<')\
    .replace('}}','>>')\
    .replace('{%','<%')\
    .replace('%}','%>')\
    .replace('{#','<#')\
    .replace('#}','#>')


# Recompile module with modified source
module = importlib.util.module_from_spec(spec)
codeobj = compile(new_source, module.__spec__.origin, 'exec')
exec(codeobj, module.__dict__)


class ShopThemeTemplate(module.Template):
    pass


def render(self, context):
    bits = []
    for node in self:
        # print('############',node, type(node))
        if isinstance(node, (module.Node, Node)):
            bit = node.render_annotated(context)
        else:
            bit = node
        bits.append(force_text(bit))
    return mark_safe(''.join(bits))

setattr(module.NodeList, 'render', render)




class ShopifyTemplateFsLoader(Loader):
    def get_template(self, template_name, template_dirs=None, skip=None):
        """
        Calls self.get_template_sources() and returns a Template object for
        the first template matching template_name. If skip is provided,
        template origins in skip are ignored. This is used to avoid recursion
        during template extending.
        """
        tried = []

        args = [template_name]
        # RemovedInDjango20Warning: Add template_dirs for compatibility with
        # old loaders
        # if func_supports_parameter(self.get_template_sources, 'template_dirs'):
        #     args.append(template_dirs)

        for origin in self.get_template_sources(*args):
            if skip is not None and origin in skip:
                tried.append((origin, 'Skipped'))
                continue

            try:
                contents = self.get_contents(origin)
            except TemplateDoesNotExist:
                tried.append((origin, 'Source does not exist'))
                continue
            else:
                return ShopThemeTemplate(
                    contents, origin, origin.template_name, self.engine,
                )

        raise TemplateDoesNotExist(template_name, tried=tried)

    def load_template(self, template_name, template_dirs=None):
        warnings.warn(
            'The load_template() method is deprecated. Use get_template() '
            'instead.', RemovedInDjango20Warning,
        )
        source, display_name = self.load_template_source(
            template_name, template_dirs,
        )
        origin = Origin(
            name=display_name,
            template_name=template_name,
            loader=self,
        )
        try:
            template = ShopThemeTemplate(source, origin, template_name, self.engine)
        except TemplateDoesNotExist:
            # If compiling the template we found raises TemplateDoesNotExist,
            # back off to returning the source and display name for the
            # template we were asked to load. This allows for correct
            # identification of the actual template that does not exist.
            return source, display_name
        else:
            return template, None


class ShopifyEngine(Engine):

    def __init__(self, *args, **kwargs):
        super(ShopifyEngine, self).__init__(*args, **kwargs)
        self.loaders = ['django_shopify_synchronizer.templates.ShopifyTemplateFsLoader']

    def from_string(self, template_code):
        """
        Returns a compiled Template object for the given template code,
        handling template inheritance recursively.
        """
        return ShopThemeTemplate(template_code, engine=self)

    def get_template(self, template_name):
        """
        Returns a compiled Template object for the given template name,
        handling template inheritance recursively.
        """
        template, origin = self.find_template(template_name)
        if not hasattr(template, 'render'):
            # template needs to be compiled
            template = ShopThemeTemplate(template, origin, template_name, engine=self)
        return template

import boto3
import botocore
import pprint
from django.conf import settings
from django_shopify_synchronizer.importer import SqsMessageImporter, ACTION_CREATE, ACTION_UPDATE, ACTION_SKIP
from django_shopify_synchronizer.importer.exceptions import MissingMessageAttributes
from django_shopify_synchronizer.models import Vendor
import logging
import json

logger = logging.getLogger(__name__)

class SqsWorker(object):
    active = False
    processed = 0
    created = 0
    updated = 0
    skipped = 0
    error = 0

    def __init__(self, queue_name, shop):
        aws_access_key_id = getattr(settings,'AWS_ACCESS_KEY_ID', None)
        aws_secret_access_key = getattr(settings,'AWS_SECRET_ACCESS_KEY', None)
        aws_region = getattr(settings,'AWS_SQS_REGION', None)

        assert aws_access_key_id, 'Configure AWS_ACCESS_KEY_ID in django.settings'
        assert aws_secret_access_key, 'Configure AWS_SECRET_ACCESS_KEY in django.settings'
        assert aws_region, 'Configure AWS_SQS_REGION in django.settings'


        self.queue_name = queue_name
        self.shop = shop

        self.sqs = boto3.resource('sqs',
                                  aws_access_key_id=aws_access_key_id,
                                  aws_secret_access_key=aws_secret_access_key,
                                  region_name=aws_region
                                  )
        try:
            self.queue = self.sqs.get_queue_by_name(QueueName=self.queue_name)
        except self.sqs.meta.client.exceptions.QueueDoesNotExist as e:
            raise Exception('SqsQueue {} not found'.format(self.queue_name))


        self.importer = SqsMessageImporter(self.shop)

    def stop(self):
        self.active = False
        logger.warning('Interrupt long polling. Wait for the end of the last call')

    def run(self):

        self.active = True
        logger.info('###### Start Long polling to "{}" queue ######'.format(self.queue_name))
        while self.active:
            messages = self.queue.receive_messages(
                MessageAttributeNames=['*'],
                AttributeNames=['All'],
                MaxNumberOfMessages=10,
                WaitTimeSeconds=10,
                VisibilityTimeout=35
            )
            logger.info('Received {} messsages from "{}" queue'.format(len(messages),self.queue_name))

            for message in messages:
                if not self.active:
                    logger.debug('Interrup cicle')
                    break

                self.processed +=1
                logger.debug('Receive message: {}', message.__dict__)

                try:
                    action, save_success, shopify_resource = self.importer.import_from_message(message)
                except MissingMessageAttributes:
                    logger.warning('Recived message without attribute. Skyp and delete')
                    message.delete()
                    continue

                if not save_success:
                    # logger.error('Failed to process {} for shop: {} , vendor:{} '.format(product_data,self.shop,vendor))
                    self.error += 1
                    message.delete()
                else:
                    if action == ACTION_UPDATE:
                        self.updated += 1
                    elif action == ACTION_CREATE:
                        self.created += 1
                    elif action == ACTION_SKIP:
                        self.skipped += 1
                    else:
                        raise Exception('Invalid action {}'.format(action))

                    logger.info('Success {} '.format(action))
                    message.delete()

        logger.info('###### Stop Long polling to "{}" queue ######'.format(self.queue_name))
        logger.info('# processed: {}'.format(self.processed))
        logger.info('# created: {}'.format(self.created))
        logger.info('# updated: {}'.format(self.updated))
        logger.info('# skipped: {}'.format(self.skipped))
        logger.info('# error: {}'.format(self.error))
        logger.info('#############################################')

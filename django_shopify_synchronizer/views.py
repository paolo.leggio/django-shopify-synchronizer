import pprint
import os
import logging
import posixpath
from django.http.response import HttpResponse, HttpResponseNotAllowed, HttpResponseBadRequest, Http404, HttpResponseNotFound
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django_shopify_synchronizer.decorators import shopify_webhook
from django_shopify_synchronizer.helpers import (
    get_signal_name_for_topic
)
from django_shopify_synchronizer.utils import only, omit
from .models import Product, Shop
from django_shopify_synchronizer import signals

from django.views import static
from django.contrib.staticfiles import finders
from django.utils.six.moves.urllib.parse import unquote
import zipfile
from io import BytesIO

logger = logging.getLogger(__name__)


class WebhookView(View):
    @method_decorator(csrf_exempt)
    @method_decorator(shopify_webhook)
    def dispatch(self, request, *args, **kwargs):
        """
        The dispatch() method simply calls the parent dispatch method, but is required as method decorators need to be
        applied to the dispatch() method rather than to individual HTTP verb methods (eg post()).
        """
        signal_name = get_signal_name_for_topic(request.shopify['topic'])

        try:

            signals.webhook_received.send_robust(self, domain=request.shopify['shop_domain'],
                                                 topic=request.shopify['topic'],
                                                 data=request.shopify['data'], shop=request.shopify['shop'])
            getattr(signals, signal_name).send_robust(self, domain=request.shopify['shop_domain'],
                                                      topic=request.shopify['topic'],
                                                      data=request.shopify['data'], shop=request.shopify['shop'])
            logger.debug('Emitted signal for webhook', extra={
                'signal': {
                    'domain': request.shopify['shop_domain'],
                    'topic': request.shopify['topic'],
                    'data': request.shopify['data'],
                    'shop': str(request.shopify['shop']),
                }
            })
        except AttributeError:
            return HttpResponseBadRequest()

        # All good, return a 200.
        return HttpResponse('OK')



from django.template.loaders.filesystem import Loader as FsLoader
from django.template.engine import Engine
from django.template import Context
from django_shopify_synchronizer.templates import ShopThemeTemplate, ShopifyEngine
import io

def shop_template_zip(request, *args, **kwargs):
    print('shop_template_zip', request, args, kwargs)
    domain = kwargs.get('domain')

    try:
        shop = Shop.objects.get(domain=domain)
    except Shop.DoesNotExist:
        raise Http404


    try:
        template_folder = shop.get_theme_folder()
    except shop.MissingTemplateConfiguration as e:
        return HttpResponseNotFound(str(e))

    theme_context = Context({
        'request': request
    })

    # print('request:', )
    # pprint.pprint(request.META  )

    tplEngine = ShopifyEngine(dirs=[template_folder])


    # print('template_folder',template_folder)
    # print('tplEngine',tplEngine)


    if request.method != 'GET':
        return HttpResponse()
    # Open StringIO to grab in-memory ZIP contents
    zip_memory_data = BytesIO()
    zip_filename = 'template.zip'

    # The zip compressor
    ziph = zipfile.ZipFile(zip_memory_data, "w", zipfile.ZIP_DEFLATED, False)
    ziph.debug = 3
    for root, dirs, files in os.walk(template_folder):
        for file in files:
            file_path = os.path.join(root, file)
            zip_path = os.path.relpath(file_path, template_folder)

            try:
                rendered_content = tplEngine.render_to_string(zip_path, theme_context)
                ziph.writestr(zip_path, rendered_content)
            except:
                ziph.write(file_path, zip_path)
            #
            # try:
            #     tpl = tplEngine.get_template(zip_path)
            # except:
            #     tpl = None
            #
            # if tpl:
            #     print('Load from tpl')
            #     rendered_content = tpl.render(theme_context)
            #     ziph.writestr(zip_path, rendered_content)
            #     # print('rendered_content',rendered_content)
            # else:
            #     ziph.write(file_path, zip_path)


    # Must close zip for all contents to be written
    ziph.close()
    #
    #
    # return HttpResponse(str(template_folder))
    resp = HttpResponse(zip_memory_data.getvalue()
                        # , mimetype = "application/x-zip-compressed"
                        )
    # ..and correct content-disposition
    resp['Content-Disposition'] = 'attachment; filename=%s' % zip_filename

    return resp


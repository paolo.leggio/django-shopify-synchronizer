import logging
import pprint
from django.utils.timezone import now

from django_shopify_synchronizer.models import Product, Customer
from django_shopify_synchronizer.signals import (
    products_create, products_update,
    customers_create, customers_update,
)

logger = logging.getLogger(__name__)


class ModelClassByTopicNotfound(Exception):
    pass


TOPIC_CALSS_MAP = {
    'products': Product,
    'customers': Customer
}


def get_model_class_by_topic(topic):
    name = topic.replace('/', '_')
    name = name.split('_')[0]

    try:
        return TOPIC_CALSS_MAP[name]
    except KeyError:
        raise ModelClassByTopicNotfound(topic)


def get_callback_for_topic(instance, topic):
    """es: get update from customers/update"""
    name = topic.replace('/', '_')
    action = name.split('_')[1]

    method_name = 'on_shopify_resource_{}'.format(action)
    method = getattr(instance, method_name, None)

    if method and callable(method):
        return method

    return None


def update_shopify_model(sender, **kwargs):
    topic = kwargs.get('topic', None)
    data = kwargs.get('data', {})
    shopify_id = data.get('id', None)
    shop = kwargs.get('shop', None)

    model_class = get_model_class_by_topic(topic)
    model_class_name = model_class.__name__

    if not shopify_id:
        raise Exception('Shopify ID not found')

    try:
        obj = model_class.objects.get(shop=shop, shopify_id=shopify_id)
    except model_class_name.DoesNotExist:
        logger.warning(
            'Update {} by webhook - NOT FOUND IN DB - shop:{} - shopify_id:{}'.format(model_class_name, shop, product))
        return

    if obj.data != data:
        logger.info('Update {} local data - {}'.format(model_class_name, obj))
        obj.data = data
        obj.last_import_date = now()
        obj.save()


    # Call Class callback method for topic
    callback = get_callback_for_topic(obj, topic)
    if callback:
        callback()




def setup_syncronizer():
    logger.info('Setup Webhook syncronizer for local models')

    for signal in [products_create,
                   products_update,
                   customers_create,
                   customers_update
                   ]:
        signal.connect(update_shopify_model)

from gql import gql, Client
from gql.transport.requests import RequestsHTTPTransport


class StoreFrontApi(object):
    def __init__(self, domain, token):
        self.transport = RequestsHTTPTransport(
            url='https://{}/api/graphql'.format(domain),
            use_json=True,
            headers={
                'X-Shopify-Storefront-Access-Token': token
            }
        )

        self.client = client = Client(
            transport=self.transport,
            # fetch_schema_from_transport=True,
        )

    def execute(self, query, *args, **kwargs):
        return self.client.execute(query, *args, **kwargs)


    def customerAccessTokenCreate(self, email, password):
        query = gql("""
                            mutation customerAccessTokenCreate($input: CustomerAccessTokenCreateInput!) {
                              customerAccessTokenCreate(input: $input) {
                                customerAccessToken {
                                  accessToken
                                  expiresAt
                                }
                                customerUserErrors {
                                  code
                                  field
                                  message
                                }
                              }
                            }
                        """)

        res = self.client.execute(query, {
            'input': {
                "email": email,
                "password": password
            }
        })

        return res['customerAccessTokenCreate']['customerAccessToken']

import base64
import hashlib
import hmac
import logging
import os
import pprint
import traceback
# Get an instance of a logger
from .shopify_rest_api import shopify
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured

# from .conf import conf
from .models import Shop,  ImportTask, ExternalProductToShopifyProductMap, Product
from .importer import ImportTaskImporter

logger = logging.getLogger(__name__)


class ApiCredentialNotFound(Exception):
    pass


def get_api_for_domain(domain, verify=False):
    try:
        shop = Shop.objects.get(domain=domain)
    except Shop.DoesNotExist:
        raise ApiCredentialNotFound()

    return shop.api


#
#
# def get_shop_credential(domain):
#     # Return API_KEY and PASSWORD for domain
#     # get data from conf or DB
#
#     domain_conf = conf.get(domain, {})
#     API_KEY = domain_conf.get('API_KEY')
#     PASSWORD = domain_conf.get('PASSWORD')
#
#     if not API_KEY or not PASSWORD:
#         raise ImproperlyConfigured(
#             'Unable to find API_KEY and PASSWORD for domain {}\n'.format(
#                 domain
#             )
#         )
#
#     return (API_KEY, PASSWORD)




def get_signal_name_for_topic(webhook_topic):
    """
    Convert a Shopify Webhook topic (eg "orders/create") to the equivalent Pythonic method name (eg "orders_create").
    """
    return webhook_topic.replace('/', '_')


def domain_is_valid(domain):
    """
    Check whether the given domain is a valid source for webhook request.
    """
    if domain is None:
        return False
    return len(domain) > 0


def get_hmac(body, secret):
    """
    Calculate the HMAC value of the given request body and secret as per Shopify's documentation for Webhook requests.
    See: http://docs.shopify.com/api/tutorials/using-webhooks#verify-webhook
    """
    hash = hmac.new(secret.encode('utf-8'), body, hashlib.sha256)
    return base64.b64encode(hash.digest()).decode()


def hmac_is_valid(body, secret, hmac_to_verify):
    """
    Return True if the given hmac_to_verify matches that calculated from the given body and secret.
    """
    if not secret:
        # print('Signature is not setted')
        return False

    return get_hmac(body, secret) == hmac_to_verify


def get_proxy_signature(query_dict, secret):
    """
    Calculate the signature of the given query dict as per Shopify's documentation for proxy requests.
    See: http://docs.shopify.com/api/tutorials/application-proxies#security
    """

    # Sort and combine query parameters into a single string.
    sorted_params = ''
    for key in sorted(query_dict.keys()):
        sorted_params += "{0}={1}".format(key, ",".join(query_dict.getlist(key)))

    signature = hmac.new(secret.encode('utf-8'), sorted_params.encode('utf-8'), hashlib.sha256)
    return signature.hexdigest()


def proxy_signature_is_valid(request, secret):
    """
    Return true if the calculated signature matches that present in the query string of the given request.
    """

    # Allow skipping of validation with an explicit setting.
    # If setting not present, skip if in debug mode by default.
    skip_validation = getattr(settings, 'SKIP_APP_PROXY_VALIDATION', settings.DEBUG)
    if skip_validation:
        return True

    # Create a mutable version of the GET parameters.
    query_dict = request.GET.copy()

    # Extract the signature we're going to verify. If no signature's present, the request is invalid.
    try:
        signature_to_verify = query_dict.pop('signature')[0]
    except KeyError:
        return False

    calculated_signature = get_proxy_signature(query_dict, secret)

    # Try to use compare_digest() to reduce vulnerability to timing attacks.
    # If it's not available, just fall back to regular string comparison.
    try:
        return hmac.compare_digest(calculated_signature.encode('utf-8'), signature_to_verify.encode('utf-8'))
    except AttributeError:
        return calculated_signature == signature_to_verify



import sys
try:
    from cStringIO import StringIO
except ImportError:
    from io import StringIO
import contextlib


class Logger(object):
    def __init__(self, std):
        self.terminal = std
        self.log = StringIO()

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        #this flush method is needed for python 3 compatibility.
        #this handles the flush command by doing nothing.
        #you might want to specify some extra behavior here.
        pass

    def getvalue(self):
        return self.log.getvalue()


@contextlib.contextmanager
def capture():
    oldout, olderr = sys.stdout, sys.stderr
    try:
        out = [Logger(sys.stdout), Logger(sys.stderr)]
        sys.stdout, sys.stderr = out

        all_logger = logging.getLogger()
        all_logger.setLevel(level=logging.INFO)
        logging.getLogger('pyactiveresource').setLevel(logging.WARNING)
        # pyactiveresource.connection

        ch = logging.StreamHandler(sys.stdout.log)
        ch.setLevel(logging.DEBUG)
        all_logger.addHandler(ch)

        # ### Optionally add a formatter
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                                  datefmt='%H:%M:%S')
        ch.setFormatter(formatter)

        yield out
    finally:
        sys.stdout, sys.stderr = oldout, olderr
        out[0] = out[0].getvalue()
        out[1] = out[1].getvalue()


def run_import_task(taskId):
    import_task = ImportTask.objects.get(pk=taskId)
    assert isinstance(import_task, ImportTask)
    assert not import_task.started, '{} is already running!'.format(import_task)

    result = {}
    with capture() as out:
        try:
            importer = ImportTaskImporter(import_task)
            result['return'] = importer.run()
        except Exception as e:
            # exc_type, exc_value, exc_traceback = sys.exc_info()
            # lines = traceback.format_exception(exc_type, exc_value, exc_traceback)
            # for line in lines:
            #     out[1].write(line)

            out[1].write(pprint.pformat(e.args, indent=4))

    result['stdout'] = out[0]
    result['stderr'] = out[1]

    return result

# def hook_import_task(obj, *args, **kwargs):
#     print('hook_import_task')
#
#     print (obj,args,kwargs)
#     print(obj.get_result(obj.id))
#     print(obj.__dict__)

from algoliasearch import algoliasearch
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel


@python_2_unicode_compatible
class AlgoliaApplication(TimeStampedModel):
    WRITE_KEY_HELP_TEXT = """This is a private API key. Please keep it secret and use it ONLY from your backend: this
    key is used to create, update and DELETE your indices. You CANNOT use this key manage your API keys."""
    ADMIN_KEY_HELP_TEXT = """This is the ADMIN API key. Please keep it secret and use it ONLY from your backend: this
    key is used to create, update and DELETE your indices. You can also use it to manage your API keys."""
    SEARCH_KEY_HELP_TEXT = """This is the public API key which can be safely used in your frontend code.This key is
    usable for search queries and it's also able to list the indices you've got access to."""
    MONITORING_KEY_HELP_TEXT = "This key is used to access the Monitoring API."

    label = models.CharField(_('Application Label'), max_length=255)
    application_id = models.CharField(_('Application Id'), max_length=10)
    write_api_key = models.CharField(_('Write Api Key'),
                                     help_text=_(WRITE_KEY_HELP_TEXT), max_length=32, null=True, blank=True)
    search_api_key = models.CharField(
        _('Search Api Key'), help_text=_(SEARCH_KEY_HELP_TEXT), max_length=32, null=True, blank=True)
    admin_api_key = models.CharField(
        _('Admin Api Key'), help_text=_(ADMIN_KEY_HELP_TEXT), max_length=32)
    monitoring_api_key = models.CharField(
        _('Monitoring Api Key'), help_text=_(MONITORING_KEY_HELP_TEXT), max_length=32, null=True, blank=True)

    def get_client(self):
        return algoliasearch.Client(self.application_id, self.admin_api_key)

    def __str__(self):
        return self.label

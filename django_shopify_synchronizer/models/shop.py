import os
import shopify
import six

if six.PY2:
    from urlparse import urlparse, urljoin
elif six.PY3:
    from urllib.parse import urlparse, urljoin

from django_shopify_synchronizer.shopify_storefront_api import StoreFrontApi
from django_shopify_synchronizer.models.managers.shop import ShopManager
from django.core.exceptions import ImproperlyConfigured
from django.db import models
from django.urls import reverse
from django.utils.encoding import python_2_unicode_compatible
from django.utils.functional import cached_property
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel
from django.conf import settings


@python_2_unicode_compatible
class Shop(TimeStampedModel):
    """
    Creare modello per lo shop, definire flag per attivare o disattivare la sincronizzazione, inserire i parametri di accesso alle api
    """
    domain = models.CharField(_('Shop Domain'), max_length=256)
    api_key = models.CharField(_('Api Key'), max_length=33)
    password = models.CharField(_('Password'), max_length=33)
    shared_secret = models.CharField(_('Shared secret'), max_length=33)
    storefront_access_token = models.CharField(_('Storefront access token'), max_length=33, null=True, blank=True)
    signature = models.CharField(_('Signature'), max_length=65, null=True, blank=True)

    algolia_application = models.OneToOneField('AlgoliaApplication', null=True, blank=True, on_delete=models.CASCADE)
    primary = models.NullBooleanField(_('Primary shop'), null=True, blank=True)

    objects = ShopManager()

    class MissingTemplateConfiguration(Exception):
        pass

    def __str__(self):
        return self.name

    def _get_configured_api(self, verify=False):
        if not (self.api_key and self.password):
            raise ImproperlyConfigured('Set api_key and password for shop {}'.format(self))

        shop_url = "https://%s:%s@%s/admin" % (self.api_key, self.password, self.domain)

        shopify.ShopifyResource.clear_session()
        shopify.ShopifyResource.set_site(shop_url)

        if verify:
            shop = shopify.Shop.current()
            assert shop.domain == self.domain, 'Warning shopify Session problem'

        return shopify

    def save(self, *args, **kwargs):
        super(Shop, self).save(*args, **kwargs)
        if self.primary:
            Shop.objects.filter(primary=True).exclude(pk=self.pk).update(primary=False)

    @property
    def name(self):
        return self.domain.split('.')[0]

    @property
    def api(self):
        return self._get_configured_api()

    @property
    def storefront(self):
        if not hasattr(self, '_storefront_api'):
            _storefront_api = StoreFrontApi(self.domain, self.storefront_access_token)
            setattr(self, '_storefront_api', _storefront_api)
        return getattr(self, '_storefront_api')

    @cached_property
    def default_location(self):
        return self.api.Location.find()[0]

    def setup_script_tags(self, base_url=None, request=None):
        assert (base_url or request), 'Specify base_url or pass request for create webhooks'

        if request is not None:
            base_url = request.META['HTTP_ORIGIN']

        script_tags = getattr(settings, 'SHOPIFY', {}).get(self.domain, {}).get('SCRIPT_TAGS', []) or getattr(settings,
                                                                                                              'SHOPIFY',
                                                                                                              {}).get(
            '*', {}).get('SCRIPT_TAGS', [])

        if not base_url.startswith('https://'):
            raise Exception('After July 1st 2018, apps will be required to use HTTPS webhook addresses.')

        # Remove all precedent scripttags
        for st in self.api.ScriptTag.find():
            print('Remove script {}'.format(st.to_dict()))
            st.destroy()

        installed_scripts = []
        for st in script_tags:
            script_src = str(st['local_src'])
            url = urljoin(base_url, script_src)
            script = self.api.ScriptTag.create({
                "event": st['event'],
                "display_scope": st['display_scope'],
                "src": url
            })
            if script.id:
                print('Installed script {}'.format(script.to_dict()))
            else:
                print('FAIL Installed script {}'.format(script.to_dict()))
            installed_scripts.append(script)

        return installed_scripts

    def setup_webhook(self, base_url=None, request=None):
        assert (base_url or request), 'Specify base_url or pass request for create webhooks'
        view_url = reverse('django_shopify_synchronizer:webhook')

        if request is not None:
            base_url = request.META['HTTP_ORIGIN']

        url = '{}{}'.format(base_url, view_url)

        if not url.startswith('https://'):
            raise Exception('After July 1st 2018, apps will be required to use HTTPS webhook addresses.')

        active_topics = [
            'products/update', 'products/create', 'products/delete',
            'customers/update', 'customers/create', 'customers/delete',
            'collections/update', 'collections/create', 'collections/delete'
        ]
        print(self, 'Setup webhook with url:{}'.format(url), active_topics)

        for wh in self.api.Webhook.find():
            if not wh.topic in active_topics:
                wh.destroy()
            else:
                if not wh.address == url:
                    wh.address = url
                    wh.save()

                active_topics.remove(wh.topic)

        for topic in active_topics:
            self.api.Webhook.create({
                "topic": topic,
                "address": url,
                "format": "json"
            })

        webhooks = self.api.Webhook.find()
        print('{} Webhook active'.format(len(webhooks)))
        for wh in webhooks:
            # print(wh.__dict__)
            print ('Webhook: topic:{} address:{}'.format(wh.topic, wh.address))

        return webhooks

    def get_shopify_products_ids(self, limit=None):
        return [p.to_dict()['id'] for p in self.api.Product.find_all()]

    # def get_products_page(self, page, limit=250):
    #     return [i for i in self.api.Product.find(limit=limit, page=page)]
    #
    # def get_all_shopify_products(self, chunk_size=250):
    #     page = 1
    #
    #     while True:
    #         result = self.get_products_page(page, chunk_size)
    #         page = page + 1
    #         if not result:
    #             break
    #         else:
    #             for item in result:
    #                 yield item

    def delete_dangling_products(self, dry):
        shopify_products_ids = self.get_shopify_products_ids()
        qs = self.product_set.exclude(shopify_id__in=shopify_products_ids)
        if dry:
            for p in qs:
                print('Dangling product {}'.format(p.shopify_id))

        else:
            print('Deleting {} product'.format(qs.count()))
            qs.delete()

    def upload_theme(self, base_url=None, request=None):
        theme_folder = self.get_theme_folder()

        view_url = reverse('django_shopify_synchronizer:shop_template_zip', kwargs={'domain': self.domain})

        if request is not None:
            base_url = request.META['HTTP_ORIGIN']

        url = urljoin(base_url, view_url)

        if not url.startswith('https://'):
            raise Exception('Use https link for theme url')

        theme_name = 'UploadedTheme'

        # remove old themes
        for theme in self.api.Theme.find():
            if theme.name == theme_name and not theme.role == 'main':
                try:
                    theme.destroy()
                except:
                    pass

        theme_data = {
            "name": theme_name,
            "src": url,
            "role": "main"
        }

        theme = self.api.Theme.create(theme_data)
        if theme.errors:
            raise Exception(str(theme.errors.errors))

    def get_theme_folder(self):
        # Return template folder path of current shop
        # Read conf from settings

        theme_path = getattr(settings, 'SHOPIFY', {}).get(self.domain, {}).get('THEME_PATH', None) or getattr(settings,
                                                                                                              'SHOPIFY',
                                                                                                              {}).get(
            '*', {}).get('THEME_PATH', None)

        print('template_path:', theme_path)
        if not theme_path:
            raise self.MissingTemplateConfiguration('Not configured')

        if not os.path.exists(theme_path):
            raise self.MissingTemplateConfiguration('Path {} not exist'.format(theme_path))

        if not os.path.isdir(theme_path):
            raise self.MissingTemplateConfiguration('Path {} not is dir'.format(theme_path))

        return theme_path

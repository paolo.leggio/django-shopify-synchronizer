import logging
from django.db import models
from django.db.models.base import ModelBase
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.utils.crypto import get_random_string
from django.conf import settings
from django_shopify_synchronizer.shopify_rest_api import CustomerAlreadyExist
from jsonfield import JSONField
from model_utils.models import TimeStampedModel
from django.utils.dateparse import parse_datetime

from django_shopify_synchronizer.models.managers.shopify import ProductManager, CustomerManager
from django.forms.models import model_to_dict

logger = logging.getLogger(__name__)

class ModelDiffMixin(object):
    """
    A model mixin that tracks model fields' values and provide some useful api
    to know what fields have been changed.
    """

    def __init__(self, *args, **kwargs):
        super(ModelDiffMixin, self).__init__(*args, **kwargs)
        self.__initial = self._dict

    @property
    def diff(self):
        d1 = self.__initial
        d2 = self._dict
        diffs = [(k, (v, d2[k])) for k, v in d1.items() if v != d2[k]]
        return dict(diffs)

    @property
    def has_changed(self):
        return bool(self.diff)

    def has_changed_fields(self, field):
        return field in self.diff.keys()

    @property
    def changed_fields(self):
        return self.diff.keys()

    def get_field_diff(self, field_name):
        """
        Returns a diff for field if it's changed and None otherwise.
        """
        return self.diff.get(field_name, None)

    def refresh_from_db(self, *args, **kwargs):
        """
        Saves model and set initial state.
        """
        super(ModelDiffMixin, self).refresh_from_db(*args, **kwargs)
        self.__initial = self._dict

    def save(self, *args, **kwargs):
        """
        Saves model and set initial state.
        """
        super(ModelDiffMixin, self).save(*args, **kwargs)
        self.__initial = self._dict

    @property
    def _dict(self):
        return model_to_dict(self, fields=[field.name for field in
                             self._meta.fields])


@python_2_unicode_compatible
class AbstractShopifyModel(ModelDiffMixin, TimeStampedModel):
    shipify_api_resource_class = None

    shop = models.ForeignKey('django_shopify_synchronizer.Shop', on_delete=models.CASCADE)
    shopify_id = models.BigIntegerField()
    data = JSONField(null=True, blank=True)
    last_import_date = models.DateTimeField(_('Last import date'), null=True, blank=True)

    class Meta:
        abstract = True
        unique_together = ('shop', 'shopify_id')
        ordering = ['-created']

    # @classmethod
    # def __new__(cls, *arg, **kwargs):
    #     assert getattr(cls, 'shipify_api_resource_class'), 'Set shipify_api_resource_class in class {}'.format(cls)
    #     return super(AbstractShopifyModel, cls).__new__( *arg, **kwargs)

    def __str__(self):
        return '{} {}:{}'.format(self.shop, self.__class__.__name__, self.shopify_id)

    @property
    def shopify_resource(self):
        assert getattr(self, 'shipify_api_resource_class'), 'Set shipify_api_resource_class in class {}'.format(self)
        try:
            return getattr(self.shop.api, self.shipify_api_resource_class).find(self.shopify_id)
        except:
            return None


    def on_shopify_resource_update(self):
        """this method is called when an new update webhook is received"""
        pass

    def on_shopify_resource_create(self):
        """this method is called when an new create webhook is received"""
        pass



class Product(AbstractShopifyModel):
    """
    Mappa dei prodotti associazione
    """
    shipify_api_resource_class = 'Product'
    objects = ProductManager()


class Customer(AbstractShopifyModel):
    """
    Mappa dei Customer
    """

    shipify_api_resource_class = 'Customer'
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL, related_name='shopify_customers')

    email = models.EmailField(_('email address'), blank=True)
    password = models.CharField(_('password'), null=True, blank=True, max_length=128)
    customer_access_token = models.CharField(_('Customer accesss token'), null=True, blank=True, max_length=128)
    customer_access_token_expire_date = models.DateTimeField(_('Customer accesss token Expire date'), null=True,
                                                             blank=True)

    objects = CustomerManager()

    def _create_customer(self):
        password = self.password or get_random_string()
        email = self.user.email

        customer_data = {
            'email': email,
            'password': password,
            'password_confirmation': password,
            "verified_email": True,
            "send_email_welcome": False
        }

        for key in ['first_name', 'last_name']:
            val = getattr(self.user, key)
            if val:
                customer_data[key] = val

        customer = self.shop.api.Customer(customer_data)
        save_success = customer.save()

        if not save_success:
            raise Exception('Fail to create customer: {}'.format(customer.errors.full_messages()))
        return customer

    def _search_customer_by_email(self):
        return self.shop.api.Customer.get_by_email(self.user.email)

    def make_customer_password(self, commit=True):
        password = get_random_string()

        customer = self.shop.api.Customer({
            'id': self.shopify_id,
            "password": password,
            "password_confirmation": password
        })

        # print('customer.save()', customer.save())
        if customer.save():
            self.password = password
        else:
            raise Exception('ERROR SAVE PASSWORD {}'.format(customer.errors.full_messages()))
        if commit:
            self.save()

    def save_shopify_customer(self, commit=True):
        if not self.shop:
            raise Exception('Invalid shop')

        if not self.user or not self.user.email:
            raise Exception('Invalid user data')

        try:
            customer = self._create_customer()
            self.shopify_id = customer.id
            self.password = customer.password
            self.email = customer.email
            self.data = customer.to_dict()
        except CustomerAlreadyExist:
            customer = self._search_customer_by_email()
            self.shopify_id = customer.id
            self.email = customer.email
            self.data = customer.to_dict()

            if not self.password:
                self.make_customer_password(commit=False)

        if commit:
            self.save()

    def update_customer_access_token(self, commit=True):
        if not self.shop:
            raise Exception('Invalid shop')
        if not self.email:
            raise Exception('Invalid email')
        if not self.password:
            raise Exception('Invalid passsword')

        customerAccessToken = self.shop.storefront.customerAccessTokenCreate(self.email, self.password)
        self.customer_access_token = customerAccessToken['accessToken']
        self.customer_access_token_expire_date = parse_datetime(customerAccessToken['expiresAt'])

        if commit:
            self.save()

    def save(self, *args, **kwargs):
        if self.has_changed_fields('data'):
            s_first_name = self.data.get('first_name')
            s_last_name = self.data.get('last_name')

            save_user = False
            if s_first_name and self.user.first_name != s_first_name:
                self.user.first_name = s_first_name
                save_user = True
            if s_last_name and self.user.last_name != s_last_name:
                self.user.last_name = s_last_name
                save_user = True

            if save_user:
                self.user.save()

        return super(AbstractShopifyModel, self).save(*args, **kwargs)


class ShopifyImagesUrlMap(TimeStampedModel):
    """
    Mappa l'url di un'immagine su shopify con un url esterno
    """
    shopify_url = models.URLField()
    external_url = models.URLField()

import os
from jsonfield import JSONField
from django.conf import settings
from django.db import models
from django.template import Context, Template
from django.utils.functional import cached_property
from django.utils.safestring import mark_safe
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _

from django_shopify_synchronizer.models.shop import Shop
from django_shopify_synchronizer.parsers.csv_parser import CSVParser
from model_utils.models import TimeStampedModel

try:
    from django_q.tasks import async_task, result
except Exception as e:
    # raise e
    from django_q.tasks import async as async_task, result



class Vendor(TimeStampedModel):
    name = models.CharField(max_length=256)
    fulfillment_name = models.CharField(max_length=256, null=True, blank=True)
    fulfillment_email = models.EmailField(_('Fulfillment Email'), null=True, blank=True)

    def __str__(self):
        return self.name

    def __getattribute__(self, item):
        attr = super(Vendor, self).__getattribute__(item)
        if item == 'fulfillment_name':
            return attr or self.name

        if item == 'fulfillment_email':
            return attr or getattr(settings, 'SHIPIFY_DEAFULT_FULFILLMENT_EMAIL', None)

        return attr


class ImportSource(TimeStampedModel):
    vendor = models.ForeignKey('Vendor', on_delete=models.CASCADE)
    file = models.FileField()
    items_count = models.IntegerField(null=True, blank=True)

    PARSER_MAP = {
        'csv': CSVParser
    }

    class UnsupportedFileType(Exception):
        pass

    def file_extension(self):
        # print self.file.name
        name, extension = os.path.splitext(self.file.name)
        return extension[1:]

    def get_parser_class(self):
        # todo: select parser by file type
        try:
            return self.PARSER_MAP[self.file_extension()]
        except KeyError:
            raise self.UnsupportedFileType('No parser found for file extension "{}"'.format(self.file_extension()))

    @cached_property
    def products(self):
        """
        list of items:
        {
        vendor,
        vendor_id,
        ....
        }
        :return:
        """
        # print('products', self.file)
        parser = self.get_parser_class()(source=self.file)
        return parser.get_products()

    def __str__(self):
        return '{}: {} [{}]'.format(self.vendor, self.file.name, self.created.strftime("%Y-%m-%d %H:%M:%S"))


class ExternalProductToShopifyProductMap(TimeStampedModel):
    """
    mappa dei prodotti di vendor esterni, con i prodotti shopify
    """
    vendor = models.ForeignKey('Vendor', on_delete=models.CASCADE)
    vendor_product_id = models.CharField(max_length=256)
    product = models.ForeignKey('Product', on_delete=models.CASCADE, null=True, blank=True)
    shop = models.ForeignKey('django_shopify_synchronizer.Shop', on_delete=models.CASCADE, null=True, blank=True)
    src_data = JSONField(null=True, blank=True)

    class Meta:
        unique_together = ('vendor', 'vendor_product_id', 'product')


    def is_imported(self):
        return self.product is not None

    def get_shopify_resource(self):
        if self.product:
            return self.product.shopify_resource
        else:
            if self.shop:
                return self.shop.api.Product()
            else:
                raise Exception('No shop defined')


class ImportTask(TimeStampedModel):
    started = models.DateTimeField(editable=False, null=True, blank=True)
    stopped = models.DateTimeField(editable=False, null=True, blank=True)
    success = models.NullBooleanField(null=True, blank=True, editable=False)

    import_source = models.ForeignKey(ImportSource, null=True, blank=True, on_delete=models.CASCADE)
    target_shop = models.ForeignKey(Shop, verbose_name=_('Sync to shop'), on_delete=models.CASCADE)

    processed_count = models.IntegerField(null=True, blank=True, editable=False)
    imported_count = models.IntegerField(null=True, blank=True, editable=False)
    updated_count = models.IntegerField(null=True, blank=True, editable=False)
    failed_count = models.IntegerField(null=True, blank=True, editable=False)
    task_id = models.CharField(max_length=128, null=True, blank=True)

    def __str__(self):
        return '{} >>> {}'.format(self.import_source, self.target_shop)

    @property
    def in_progress(self):
        return self.started and not self.stopped

    @property
    def is_finished(self):
        return self.started is not None and self.stopped is not None

    @property
    def was_succesful(self):
        return self.stopped and self.success

    @property
    def has_failed(self):
        return self.started and not self.success

    @property
    def status(self):
        if self.is_finished:
            if self.was_succesful:
                return 'Completed on {}'.format(self.stopped)
            else:
                return 'Failed on {}'.format(self.stopped)
        elif self.in_progress:
            return 'Running'
        else:
            return 'Pending'

    def set_started(self):
        if self.started:
            raise Exception('Task is already started')

        self.started = now()

        for key in ['processed_count', 'imported_count', 'updated_count', 'failed_count']:
            if getattr(self, key) is None:
                setattr(self, key, 0)

        self.save()

    def set_stopped(self, success):
        if self.stopped:
            raise Exception('Task is already stopped')

        if not self.started:
            raise Exception('Task is not started')

        self.success = success
        self.stopped = now()
        self.save()

    def reset(self):
        for k in ['started', 'stopped', 'success', 'processed_count', 'imported_count', 'updated_count', 'failed_count',
                  'task_id']:
            setattr(self, k, None)
        self.save()

    def run(self):

        self.task_id = async_task('django_shopify_synchronizer.helpers.run_import_task', self.id, timeout=24 * 60 * 60,
                             )
        self.save()

    def get_preview_html(self):
        task_result = self.task_result()
        try:

            context = Context({
                'stdout': mark_safe('\n'.join(reversed(
                    [l for idx, l in enumerate(reversed([l for l in task_result['stdout'].split('\n')])) if idx <= 30])).replace(' ','&nbsp;')),
                # 'stderr': mark_safe('\n'.join(reversed(
                #     [l for idx, l in enumerate(reversed([l for l in task_result['stderr'].split('\n')])) if idx <= 30])).replace(' ','&nbsp;')),
                'stderr': '\n'.join(reversed(
                    [l for idx, l in enumerate(reversed([l for l in task_result['stderr'].split('\n')])) if
                     idx <= 30]))
            })
            template = Template('''
            <h1> stdout (last 30 row)</h1>
            <p>{{stdout|linebreaksbr}}<p>
            <h1> stderr (last 30 row)</h1>
            <p>{{stderr|linebreaksbr}}<p>
            
            ''')
            return template.render(context)
        except:
            return None

    def task_result(self):
        # print ('task_id',self.task_id)
        if self.task_id:
            task_result = result(self.task_id, 30)
            # print('task_result:',task_result)
            return task_result
        else:
            return None

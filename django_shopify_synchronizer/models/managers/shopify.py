from django.db import models, transaction
from django.db.models.manager import BaseManager
from django_shopify_synchronizer.models.shop import Shop
from django.db.utils import IntegrityError


class ShopifyModelQuerySet(  models.QuerySet):
    def in_domain(self, domain):
        return self.filter(shop__domain=domain)

    def in_shop(self, shop):
        return self.filter(shop=shop)




class CustomerQuerySet(ShopifyModelQuerySet):
    def get_or_create_from_user(self, user, shop=None):
        if shop is None:
            shop = Shop.objects.primary()

        created = False
        lookup = {
            'shop': shop,
            'user': user
        }

        try:
            return self.get(**lookup), False
        except self.model.DoesNotExist:
            pass

        # with transaction.atomic(using=self.db):
        obj = self.model(**lookup)
        obj.save_shopify_customer(commit=False)
        old_obj_qs = self.model.objects.filter(shop=obj.shop, shopify_id=obj.shopify_id)
        if old_obj_qs:
            obj.id = old_obj_qs[0].id
            obj.save()
            created = False
        else:
            obj.save()
            created = True


        return obj, created



class ShopifyModelManager(BaseManager.from_queryset(ShopifyModelQuerySet)):
    pass

class ProductManager(ShopifyModelManager):
    pass

class CustomerManager(BaseManager.from_queryset(CustomerQuerySet)):
    pass

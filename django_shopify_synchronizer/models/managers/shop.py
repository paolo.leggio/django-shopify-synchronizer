from django.db import models



class ShopManager(models.Manager):


    def primary(self):
        return self.get_queryset().get(primary=True)

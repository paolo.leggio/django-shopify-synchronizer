import logging
import json
import pprint
from six import __init__
import six
import collections
from contextlib import contextmanager
from django.utils.timezone import now
from django_shopify_synchronizer.models import Shop, ExternalProductToShopifyProductMap, Vendor, Product
from deepdiff import DeepDiff
from .exceptions import ShopNotDefined, DuplicatedProduct, VendorNotDefined, ProductDataNotDefined
from .settings import import_settings
from django_shopify_synchronizer.utils import omit, dict_update

logger = logging.getLogger(__name__)

ACTION_CREATE = 'CREATE'
ACTION_UPDATE = 'UPDATE'
ACTION_SKIP  = 'SKIP'

EXCLUDED_FIELDS_FROM_PRODUCT_UPDATE = ['title','body_html','product_type']

STATUS_DESCRIPTION = {
    201: "Created",
    204: "Updated",
    304: "Not Modified",
    400: "Create failed",
    401: "Update failed",
    409: "Conflict",
    500: "Internal Server Error",
    501: "Not Implemented",
    503: "Service Unavailable",
}



class ImporterLogger(logging.Logger):
    importer = None

    def set_importer(self, importer):
        self.importer = importer

    def _log(self, level, msg, args, exc_info=None, extra=None):
        extra = extra or {}
        if self.importer:
            importer_extra = {'importer': self.importer.get_log_extra()}
            extra = dict_update(extra, importer_extra)
            # extra.dict_update({'importer': self.importer.get_log_extra()})
            # msg =  '{} - {}'.format( self.importer.get_log_prefix(), msg)

        return super(ImporterLogger, self)._log(level, msg, args, exc_info, extra)


class BaseImporter(object):
    _logger = None
    _vendor = None
    _product_data = None
    _product_map = None

    def __init__(self, shop=None):
        if not isinstance(shop, Shop):
            raise ShopNotDefined()
        self.shop = shop
        self._logger = self.setup_logger()
        self.normalizer_classes = import_settings.NORMALIZER_CLASSES

    @property
    def vendor(self):
        if not self.vendor:
            raise VendorNotDefined()
        return self._vendor

    @property
    def vendor_str(self):
        try:
            return str(self.vendor)
        except:
            return 'Unknow'

    @property
    def product_data(self):
        if not self._product_data:
            raise ProductDataNotDefined()
        return self._product_data

    @property
    def vendor_product_id(self):
        vendor_product_id = self.product_data.get('VendorProductId')
        if not vendor_product_id:
            raise Exception('VendorProductId not found')
        return vendor_product_id

    @property
    def vendor_product_id_str(self):
        try:
            return self.vendor_product_id
        except:
            return 'Unknow'

    @property
    def product_map(self):
        if self._product_map is None:
            lookup_params = {
                'product__shop': self.shop,
                # 'shop': self.shop,
                'vendor': self.vendor,
                'vendor_product_id': self.vendor_product_id
            }

            try:
                self._product_map = ExternalProductToShopifyProductMap.objects.get(**lookup_params)
            except ExternalProductToShopifyProductMap.DoesNotExist:
                self._product_map = None
            except ExternalProductToShopifyProductMap.MultipleObjectsReturned:
                logging.exception('ExternalProductToShopifyProductMap.MultipleObjectsReturned {}'.format(lookup_params))
                raise DuplicatedProduct()

            if not self._product_map:
                self._product_map = ExternalProductToShopifyProductMap.objects.create(
                    vendor=self.vendor,
                    vendor_product_id=self.vendor_product_id,
                    shop=self.shop
                )
            else:
                if not self._product_map.shop:
                    self._product_map.shop = self.shop
                    self._product_map.save()

        return self._product_map

    @property
    def shopify_resource(self):
        return self.product_map.get_shopify_resource()

    @property
    def logger(self):
        return self._logger or logger

    def get_log_extra(self):
        log_extra = {
            'vendor': self.vendor_str,
            'vendor_product_id': self.vendor_product_id_str,
            'shop': str(self.shop)
        }
        try:
            log_extra['shopify_id'] = self.product_map.product.shopify_id
        except:
            pass
        return log_extra

    def get_log_prefix(self):

        shop = str(self.shop)
        # shop = str(self.shop).split('.')[0]

        return 'shop:[{}] vendor:[{}][{}]'.format(shop, self.vendor_str, self.vendor_product_id_str)

    def setup_logger(self):
        logging.setLoggerClass(ImporterLogger)
        logger_name = '{}.{}'.format(__package__, self.__class__.__name__)
        # print('logger_name:', logger_name)
        # logger = logging.getLogger('shop:[{}] vendor:[{}][{}]'.format( self.shop, self.vendor, self.vendor_product_id))
        logger = logging.getLogger('{}'.format(self.__class__.__name__))
        logger.set_importer(self)
        # if not logger.handlers:
        #     formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s', '%Y-%m-%d %H:%M:%S')
        #     ch = logging.StreamHandler()
        #     ch.setFormatter(formatter)
        #     logger.addHandler(ch)
        # logger.propagate = False
        return logger

    @contextmanager
    def import_item_context(self, product_data, vendor):
        try:
            self._product_data = product_data
            self._vendor = vendor

            yield
        finally:
            self._product_data = None
            self._vendor = None
            self._product_map = None

    def get_vendor_product_id(self, product_data):
        vendor_product_id = product_data.get('VendorProductId')
        if not vendor_product_id:
            raise Exception('VendorProductId not found')
        return vendor_product_id

    def get_normalized_product_data(self, merge_tags=True):
        product_data = self._product_data.copy()
        product_data.pop('VendorProductId')
        product_data['vendor'] = self.product_map.vendor.name

        if 'tags' in product_data:
            tags = product_data.get('tags', [])
            if isinstance(tags, six.string_types):
                tags = [t.strip() for t in tags.split(',')]
            product_data['tags'] = tags

        if merge_tags and self.shopify_resource:
            # merge tags and new tags
            try:
                old_tags = [t.strip() for t in self.shopify_resource.tags.split(',')]
                merge_tags = product_data.get('tags', [])
                product_data['tags'] = list(set(old_tags + merge_tags))
            except Exception as e:
                # print('Exception:', e)
                pass

        for normalizer in self.normalizer_classes:
            try:
                product_data = normalizer(product_data, self.product_map)
            except:
                self.logger.exception(
                    'Problem with normalizer {}! check NORMALIZER_CLASSES settings'.format(normalizer))

        return product_data

    def populate_shopify_resource(self, shopify_resource, data, action):

        for k, v in data.items():

            if action == ACTION_UPDATE and k in EXCLUDED_FIELDS_FROM_PRODUCT_UPDATE:
                continue

            setattr(shopify_resource, k, v)

        return shopify_resource

    def process_item(self, product_data, vendor):
        with self.import_item_context(product_data, vendor):
            # self.logger.info('Process item')
            try:
                return self.save_product(merge_tags=True)
            except DuplicatedProduct:
                return ACTION_UPDATE, False, None

    def run(self, product_data, vendor):
        return self.process_item(product_data, vendor)

    def get_product_difference(self):

        initial_data = omit(self.product_data, *EXCLUDED_FIELDS_FROM_PRODUCT_UPDATE)
        if self.product_map.src_data:
            current_data = omit(self.product_map.src_data, *EXCLUDED_FIELDS_FROM_PRODUCT_UPDATE)
        else:
            current_data = None


        pdiff = DeepDiff(current_data, initial_data, ignore_order=True)

        return json.loads(pdiff.json)






    def save_product(self, merge_tags=True):
        initial_data = self.product_data.copy()

        # pdiff = DeepDiff(self.product_map.src_data, initial_data, ignore_order=True)
        product_diff = self.get_product_difference()

        # if not pdiff and self.product_map.product:
        if not product_diff:
            self.logger.info('Product Already up-to-date.', extra={'importer': {'action':ACTION_SKIP, 'success':True}})
            product = self.product_map.product
            product.last_import_date = now()
            product.save(update_fields=["last_import_date"])
            self.logger.debug('Update last_import_date')
            return ACTION_SKIP, True, None

        shopify_resource = self.shopify_resource

        if self.product_map.is_imported():
            log_extra = {'importer': {
                'product_diff': product_diff
            }}
            action = ACTION_UPDATE
        else:
            log_extra = {'importer': {'initial_data':pprint.pformat(initial_data, indent=4)}}
            action = ACTION_CREATE

        normalized_product_data = self.get_normalized_product_data(merge_tags=merge_tags)
        shopify_resource = self.populate_shopify_resource(shopify_resource, normalized_product_data, action=action)
        save_success = shopify_resource.save()

        if action == ACTION_CREATE and save_success:
            shopify_product, shopify_product_created = Product.objects.get_or_create(shop=self.product_map.shop,
                                                                                     shopify_id=shopify_resource.id,
                                                                                     data=shopify_resource.to_dict()
                                                                                     )
            self.product_map.src_data = initial_data
            self.product_map.product = shopify_product
            self.product_map.save()

        elif action == ACTION_UPDATE and save_success:
            shopify_product = Product.objects.get(shop=self.product_map.shop, shopify_id=shopify_resource.id)
            shopify_product.data = shopify_resource.to_dict()
            self.product_map.src_data = initial_data
            self.product_map.save()

        if save_success:
            shopify_product.last_import_date = now()
            shopify_product.save()



        self.logger.info('Product {} Success: {}'.format(action, save_success), extra=dict_update(log_extra,{'importer': {'action':action, 'success':save_success}}))
        return action, save_success, shopify_resource

import logging
import json
import pprint
from django_shopify_synchronizer.importer.base import BaseImporter
from django_shopify_synchronizer.importer.exceptions import MissingMessageAttributes, VendorNotDefined
from django_shopify_synchronizer.models import Vendor
from contextlib import contextmanager

logger = logging.getLogger(__name__)


class SqsMessageImporter(BaseImporter):
    _message = None
    _product_data = None
    _vendor = None

    def _set_message(self, message):
        self._message = message

    def _unset_message(self):
        self._message = None
        self._product_data = None
        self._vendor = None

    def get_log_extra(self):
        extra = super(SqsMessageImporter, self).get_log_extra()
        extra.update(
            {
                'sqs': {
                    'message': {
                        'id': self._message.message_id
                    },
                    'queue':{
                        'name': self._message._queue_url.split('/')[-1]
                    }

                }
            })
        return extra

    @contextmanager
    def with_message(self, message):
        try:
            self._set_message(message)
            yield self._message
        finally:
            self._unset_message()

    def assert_message(self):
        if not self._message:
            raise Exception('Message not found! run with with_message context')

    @property
    def product_data(self):
        self.assert_message()
        if not self._product_data:
            self._product_data = json.loads(self._message.body)
        return self._product_data

    @property
    def vendor(self):
        self.assert_message()
        if not self._vendor:
            if self._message.message_attributes is None:
                raise MissingMessageAttributes()

            vendor_name = self._message.message_attributes.get('VendorName', {}).get('StringValue')
            vendor_code = self._message.message_attributes.get('VendorCode', {}).get('StringValue')

            if not (vendor_code and vendor_name):
                raise VendorNotDefined()

            vendor, created = Vendor.objects.get_or_create(name=vendor_name)
            if created:
                    self.logger.info('Create new vendor:{}'.format(vendor.name))

            self._vendor = vendor
        return self._vendor

    def import_from_message(self, message):
        with self.with_message(message):
            self.logger.info('Process SQSMessage id:{}'.format(message.message_id))
            return self.run(self.product_data, self.vendor)

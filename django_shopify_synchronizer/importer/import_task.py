import logging
from django.core.exceptions import ImproperlyConfigured
from django_shopify_synchronizer.importer.base import BaseImporter
from django_shopify_synchronizer.importer.exceptions import ProductNotImported
from django_shopify_synchronizer.models import ImportTask, ExternalProductToShopifyProductMap

logger = logging.getLogger(__name__)


class ImportTaskImporter(BaseImporter):
    processed_count = 0
    imported = []
    imported_failed = []
    updated = []
    updated_failed = []

    def __init__(self, import_task):
        if not isinstance(import_task, ImportTask):
            raise ImproperlyConfigured('ImportTaskImporter require import_task')

        self.import_task = import_task
        super(ImportTaskImporter, self).__init__(self.import_task.target_shop)
        # self.shop = self.import_task.target_shop

    @property
    def vendor(self):
        return self.import_task.import_source.vendor

    @property
    def source_products(self):
        return list(self.import_task.import_source.products)

    def get_imported_products_map(self):
        lookup_params = {
            'product__shop': self.shop,
            'vendor': self.vendor
        }
        return ExternalProductToShopifyProductMap.objects.filter(**lookup_params)

    def get_product_by_vendor_product_id(self, vendor_product_id, use_cache=False):
        # print('get_product_by_vendor_product_id', vendor_product_id)
        if use_cache:
            raise NotImplementedError()
        else:
            try:
                # 'check results for get_product_by_vendor_product_id'
                # for p in self.get_imported_products_map().filter(
                #     vendor_product_id=vendor_product_id):
                #     pprint.pprint(p.__dict__)
                product_import_map = self.get_imported_products_map().filter(
                    vendor_product_id=vendor_product_id).first()
                if product_import_map:
                    return product_import_map.product
                else:
                    raise ProductNotImported()
            except ExternalProductToShopifyProductMap.DoesNotExist:
                raise ProductNotImported()

    def already_imported_vendor_product_id(self):
        return list(
            self.get_imported_products_map().values_list('vendor_product_id', flat=True))

    def regroup_sorce_products(self):
        # processa la lista dei prodotti e li suddivide in prodotti da importare, da aggiornare e da eliminare

        products_to_be_imported = []
        products_to_be_updated = []

        already_imported_vendor_product_id = self.already_imported_vendor_product_id()

        # define which products are to be imported and which ones to be updated
        for source_product in self.source_products:
            # print('source_product', source_product)
            vendor_product_id = source_product.get('VendorProductId')

            try:
                already_imported_vendor_product_id.remove(vendor_product_id)
            except ValueError:
                pass

            try:
                product = self.get_product_by_vendor_product_id(vendor_product_id)
                products_to_be_updated.append((source_product, product))
            except ProductNotImported:
                products_to_be_imported.append(source_product)

        products_to_be_removed = ExternalProductToShopifyProductMap.objects \
            .filter(vendor_product_id__in=already_imported_vendor_product_id).values_list(
            'product', flat=True)

        return products_to_be_imported, products_to_be_updated, products_to_be_removed

    def _log_import_start(self):
        self.processed_count += 1
        self.import_task.processed_count = self.processed_count
        self.import_task.save()

    def _log_import_end(self, product, success=False):

        if success:
            self.imported.append(product)
            self.import_task.imported_count = (self.import_task.imported_count or 0) + 1
        else:
            self.imported_failed.append(product)
            self.import_task.failed_count = (self.import_task.failed_count or 0) + 1

        self.import_task.save()

    def _log_update_start(self):
        self.processed_count += 1
        self.import_task.processed_count = self.processed_count
        self.import_task.updated_count = len(self.updated)
        self.import_task.save()

    def _log_update_end(self, product, success=False):

        if success:
            self.updated.append(product)
            self.import_task.updated_count = (self.import_task.updated_count or 0) + 1
        else:
            self.updated_failed.append(product)
            self.import_task.failed_count = (self.import_task.failed_count or 0) + 1

        self.import_task.save()

    def _import_cycle(self, product_list):
        success_count = 0
        for product_data in product_list:
            self._log_import_start()
            action, success, product = self.process_item(product_data, self.vendor)
            # action, success, product = self.save_product(product_data, shop=self.shop, vendor=self.vendor)
            if success:
                success_count += 1
            self._log_import_end(product, success=success)
        return success_count

    def _update_cycle(self, product_list, merge_tags=True):
        # Update cycle
        success_count = 0
        for product_data, shopify_product in product_list:
            self._log_update_start()
            action, success, product = self.process_item(product_data, self.vendor)

            # product = shopify_product.shopify_resource
            # action, success, product = self.save_product(product_data, shopify_resource=product, shop=self.shop,
            #                                              vendor=self.vendor, merge_tags=merge_tags)
            if success:
                success_count += 1
            self._log_update_end(product, success=success)

        return success_count

    def _remove_cycle(self, product_list):
        raise NotImplementedError()

    def run(self):
        # msg = """##############################
        #     # Run import task id {}
        #     # Shop: {}
        #     # Vendor: {}
        #     # Source file: {}
        #     # ----------------------------
        #     """.format(
        #         self.import_task.id,
        #         self.import_task.target_shop,
        #         self.vendor.name,
        #         self.import_task.import_source.file.name
        #     )
        # self.logger.info(msg)
        self.logger.info('##############################')
        self.logger.info('# Run import task id {}'.format(self.import_task.id))
        self.logger.info('# Shop: {}'.format(self.import_task.target_shop))
        self.logger.info('# Vendor: {}'.format(self.vendor.name))
        self.logger.info('# Source file: {}'.format(self.import_task.import_source.file.name))
        self.logger.info('# ----------------------------')

        self.import_task.set_started()

        try:
            # todo: verificare esistenza del fulfillment
            products_to_be_imported, products_to_be_updated, products_to_be_removed = self.regroup_sorce_products()
            self.logger.info('# Products to be imported: {}'.format(len(products_to_be_imported)))
            self.logger.info('# Products to be updated: {}'.format(len(products_to_be_updated)))
            # self.logger.info('# Products to be removed: {}'.format(len(products_to_be_removed)))
            self.logger.info('##############################')
        except Exception as e:
            self.logger.info('# Fail')
            self.logger.info('##############################')
            task_success = False
            self.import_task.set_stopped(task_success)
            raise e

        try:
            self._import_cycle(products_to_be_imported)
            self._update_cycle(products_to_be_updated)
            task_success = True
        except:
            task_success = False
        finally:
            self.import_task.set_stopped(task_success)

            # self._remove_cycle(products_to_be_removed)

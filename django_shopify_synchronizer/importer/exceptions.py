class ProductNotImported(Exception):
    pass


class DuplicatedProduct(Exception):
    pass


class ShopNotDefined(Exception):
    pass


class VendorNotDefined(Exception):
    pass

class ProductDataNotDefined(Exception):
    pass

class MissingMessageAttributes(Exception):
    pass

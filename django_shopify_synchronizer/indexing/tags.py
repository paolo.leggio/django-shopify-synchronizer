import re
from functools import partial


class InstanceNotFound(Exception):
    pass


class TagHandlerNotFound(Exception):
    pass


class IndexingError(Exception):
    """Something went wrong with an Index."""


# def check_source_and_get_value(instance, name):
#     try:
#         attr = getattr(instance, name)
#         if callable(attr):
#             return attr()
#         else:
#             return attr
#     except AttributeError:
#         raise IndexingError(
#             '{} is not an attribute of {}'.format(name, instance))


def _getattr(obj, name):
    return getattr(obj, name)


def check_and_get_attr(model, name):
    try:
        attr = getattr(model, name)
        if callable(attr):
            return attr
        else:
            return get_model_attr(name)
    except AttributeError:
        raise IndexingError(
            '{} is not an attribute of {}'.format(name, model))


def get_model_attr(name):
    return partial(_getattr, name=name)


class TagHandler(object):
    """
    Process a tag like "yg:cultivar:abelia-x-snowdrift"
    """

    def __init__(self, model, fields):
        self.__named_fields = {}
        self.model = model
        for f in fields:
            # Ex: f[0] = "name", f[1] = "get_name"
            self.__named_fields[f[0]] = check_and_get_attr(model, f[1])

    def _get_instance(self, instance_id):
        try:
            return self.model.objects.get(slug=instance_id)
        except self.model.DoesNotExist:
            # Todo: this exception should just logged as error and execution continued
            raise InstanceNotFound(
                "No intance of {} could be found for slug/id {}".format(self.model.__name__, instance_id))

    def get_data(self, instance_id):
        data = {}
        instance = self._get_instance(instance_id)
        for name, method in self.__named_fields.items():
            data.update({name: method(instance)})
        return data


class TagManager(object):

    def __init__(self, prefix):
        self.__handlers = {}
        self.prefix = prefix

    @staticmethod
    def _parse_tags_string(tags):
        return [t for t in tags.split(", ")]

    def _validate_tag(self, tag):
        return re.match("^{}:[A-Za-z0-9]+:[a-z0-9]+".format(self.prefix), tag)

    def _get_relevant_tags(self, tags):
        """
        Must match the pattern <prefix>:<handler-key>:<instance-id>
        :param tags: Array ex: "
        :return: Array
        """
        return [t for t in tags if self._validate_tag(t)]

    @staticmethod
    def _parse_tags(tags):
        _ = [t.split(":") for t in tags]
        return [{"handler-key": t[1], "instance-id": t[2]} for t in _]

    def register_tag_handler(self, handler_key, model, fields):
        handler = TagHandler(model, fields)
        self.__handlers[handler_key] = handler

    def is_registered(self, tag_name):
        return tag_name in self.__handlers

    def _get_handler(self, key):
        try:
            return self.__handlers[key]
        except KeyError:
            raise TagHandlerNotFound("Tag handler with key {} couldn't be found.".format(key))

    def get_tags_data(self, tag_string):
        tags = self._parse_tags_string(tag_string)
        # print('get_tags_data tags:', tags)
        relevant_tags = self._get_relevant_tags(tags)
        # print('get_tags_data relevant_tags:', relevant_tags)
        parsed_tags = self._parse_tags(relevant_tags)
        # print('get_tags_data parsed_tags:', parsed_tags)

        added_data = {}
        for t in parsed_tags:
            handler = self._get_handler(t["handler-key"])
            added_data[t["handler-key"]] = handler.get_data(t["instance-id"])
        return added_data

# -*- coding: utf-8
import logging
import algoliasearch
import pprint
import traceback
from django_shopify_synchronizer.utils import only
from django.utils.encoding import python_2_unicode_compatible
from algoliasearch.helpers import AlgoliaException
from django.conf import settings
from django_shopify_synchronizer.signals import (
    webhook_received,
    # products_create,
    # products_update,
    # products_delete,
    # collections_create,
    # collections_update,
    # collections_delete
)

from ..models import Shop, Product

try:
    from django_q.tasks import async_task, result
except Exception as e:
    # raise e
    from django_q.tasks import async as async_task, result

"""
For reference and inspiration always check Algolia's algoliasearch-django repo
https://github.com/algolia/algoliasearch-django
"""

logger = logging.getLogger(__name__)

tag_prefix = getattr(settings, "DJANGO_SHOPIFY_SYNCHRONIZER_TAG_PREFIX", "extra-data")
index_name_template = "{}_{}"
index_name_prefix = getattr(settings, "DJANGO_SHOPIFY_SYNCHRONIZER_INDEX_NAME_PREFIX", "it")
product_index_name = getattr(settings, "DJANGO_SHOPIFY_SYNCHRONIZER_PRODUCT_INDEX_NAME", "shopify-products")
collection_index_name = getattr(settings, "DJANGO_SHOPIFY_SYNCHRONIZER_COLLECTION_INDEX_NAME", "shopify-collections")


class WithClient():
    def __init__(self, index, client):
        self.index = index
        self.client = client

    def __enter__(self):
        self.index.set_client(self.client)
        return self.index

    def __exit__(self, type, value, traceback):
        self.index.unset_client()


@python_2_unicode_compatible
class AlgoliaIndex(object):
    # Use to specify the settings of the index.
    settings = None

    def __init__(self):
        """Initializes the index."""
        self.__client = None
        self.__index = None
        self.__tmp_index = None
        self.index_name = self.__class__.index_name
        self.tmp_index_name = "{index_name}_tmp".format(index_name=self.index_name)

    def set_client(self, client):
        self.__client = client
        self.__index = client.init_index(self.index_name)
        self.__tmp_index = client.init_index(self.tmp_index_name)

    def unset_client(self):
        self.__client = None
        self.__index = None
        self.__tmp_index = None

    def get_client(self):
        return self.__client

    client = property(get_client)

    def get_index(self):
        return self.__index

    index = property(get_index)

    def with_client(self, client):
        return WithClient(self, client)

    def _get_index_settings(self, index):
        try:
            logger.info('GET SETTINGS FROM %s', index.index_name)
            return index.get_settings()
        except AlgoliaException as e:
            if DEBUG:
                raise e
            else:
                logger.warning('ERROR DURING GET_SETTINGS ON INDEX %s: %s',
                               index.index_name, e)

    def _set_index_settings(self, index, settings):
        if not settings:
            return
        try:
            index.set_settings(settings)
            logger.info('APPLY SETTINGS ON %s', index.index_name)
        except AlgoliaException as e:
            if DEBUG:
                raise e
            else:
                logger.warning('SETTINGS NOT APPLIED ON INDEX %s: %s',
                               index.index_name, e)

    def get_settings(self):
        """Returns the settings of the index."""
        return self._get_index_settings(self.__index)

    def set_settings(self):
        """Applies the settings to the index."""
        self._set_index_settings(self.__index, self.settings)

    def save_record(self, shopify_resource_data):
        assert self.__client, 'AlgoliaIndex, no client configurated. Use AlgoliaIndex().with_client(client)'

        try:
            obj = self.get_raw_record(shopify_resource_data)
            obj = self.normalize_data(obj)
            # pprint.pprint(obj)
            result = self.__index.save_object(obj)
            logger.info('SAVE {}: {}'.format(obj['objectID'], result))
        except AlgoliaException as e:
            if settings.DEBUG:
                raise e
            else:
                logger.warning('{} FROM {} NOT SAVED: {}', obj['objectID'], obj["handle"], e)
        except Exception as e:
            print('save_record error Exception', e)
            raise e

    def get_raw_record(self, data):
        """
        Gets the raw record.
        """
        # print('get_raw_record',data)
        shopify_resource_data = data["data"]
        object_id = shopify_resource_data["id"]

        # object to be sent to index
        tmp = {}
        tmp.update(shopify_resource_data)  # Shopify data
        tmp.update(self.get_extra_data(data))

        tmp.update({"objectID": object_id})  # identifier
        #
        # print('----get_raw_record----')
        # pprint.pprint(tmp)

        # logger.debug("get_raw_record BUILD {} FROM SHOPIFY {}".format(tmp["objectID"], shopify_resource_data["id"]))
        logger.debug('get_raw_record data:{}'.format(
            pprint.pformat(tmp, indent=4)
        ))
        return tmp

    def normalize_data(self, data):
        return data

    def delete_record(self, data):
        """Deletes the record."""
        # print('delete_record', data)
        shopify_resource_data = data["data"]
        object_id = shopify_resource_data["id"]

        try:
            self.__index.delete_object(object_id)
            logger.info('DELETE {} '.format(object_id))
        except AlgoliaException as e:
            if settings.DEBUG:
                raise e
            else:
                logger.warning('%s NOT DELETED: %s', object_id, e)

    def get_all_resources_for_shop(self, shop):
        raise NotImplementedError

    def reindex_all_of_shop(self, shop, batch_size=1000):
        # print('reindex_all_of_shop', shop)
        try:
            client = shop.algolia_application.get_client()
        except AttributeError:
            raise Exception("No AlgoliaApplication for Shop {}".format(shop))

        resources = self.get_all_resources_for_shop(shop)

        # logger.info('reindex_all_of_shop: {} resources'.format(len(resources)))
        # if len(resources):
        #     for i in range(5):
        #         print('#'*10)
        #         pprint.pprint(resources[i])
        #         print('-'*10)
        #         pprint.pprint(self.get_raw_record(resources[i]))
        #
        #     return

        with self.with_client(client) as index:
            index.reindex_all(resources)

    def save_records_to_index(self, index, resources, batch_size=1000):
        try:
            counts = 0
            batch = []

            for resource in resources:
                obj = self.get_raw_record(resource)
                batch.append(obj)
                if len(batch) >= batch_size:
                    index.save_objects(batch)
                    logger.info('SAVE %d OBJECTS TO %s', len(batch),
                                index.index_name)
                    batch = []
                counts += 1
            if len(batch) > 0:
                index.save_objects(batch)
                logger.info('SAVE %d OBJECTS TO %s', len(batch),
                            index.index_name)

            return counts
        except AlgoliaException as e:
            if settings.DEBUG:
                raise e
            else:
                logger.warning('ERROR DURING SAVE RECORDS TO %s: %s', index_name.index_name,
                               e)

    def save_records(self, resources, batch_size=1000):

        try:
            return self.save_records_to_index(self.__index, resources, batch_size)

        except AlgoliaException as e:
            if settings.DEBUG:
                raise e
            else:
                logger.warning('ERROR DURING SAVE BULK RECORDS %s: %s', self.index_name,
                               e)

    def reindex_all(self, resources, batch_size=1000):
        """
        Reindex all the records.
        """
        try:
            settings = self.get_settings()
        except:
            settings = self.settings
            logger.info('USE DEFAULT SETTINGS')

        if settings:
            logger.info('CONFIGURE SETTINGS TO INDEX %s', self.__tmp_index.index_name)
            self._set_index_settings(self.__tmp_index, settings)

        try:
            self.__tmp_index.clear_index()
            logger.debug('CLEAR INDEX %s_tmp', self.index_name)

            counts = self.save_records_to_index(self.__tmp_index, resources, batch_size)

            self.__client.move_index(self.__tmp_index.index_name,
                                     self.__index.index_name)
            logger.info('MOVE INDEX %s_tmp TO %s', self.index_name,
                        self.index_name)

            return counts
        except AlgoliaException as e:
            if settings.DEBUG:
                raise e
            else:
                logger.warning('ERROR DURING REINDEXING %s: %s', self.index_name,
                               e)

    def __str__(self):
        return self.index_name


@python_2_unicode_compatible
class ProductIndex(AlgoliaIndex):
    index_name = index_name_template.format(index_name_prefix, product_index_name)

    settings = {'alternativesAsExact': ['ignorePlurals', 'singleWordSynonym'],
                'attributeForDistinct': None,
                'attributesForFaceting': ['collections.handle',
                                          'searchable(collections.title)',
                                          'product_type',
                                          'vendor',
                                          'searchable(yg_data.cultivar.cultivar)',
                                          'searchable(yg_data.cultivar.plant)',
                                          'yg_data.cultivar.specialAttrs.roseFlowerShapeObject.name_IT',
                                          'yg_data.cultivar.specialAttrs.roseFloweringRepetitionObject.name_IT',
                                          'yg_data.cultivar.specialAttrs.roseFragranceObject.name_IT',
                                          'yg_data.cultivar.specialAttrs.roseHabitTypeObject.name_IT'],
                'attributesToHighlight': ['suggestionTitle'],
                'attributesToRetrieve': None,
                'attributesToSnippet': None,
                'customRanking': ['asc(title)'],
                'exactOnSingleWordQuery': 'attribute',
                'highlightPostTag': '</em>',
                'highlightPreTag': '<em>',
                'hitsPerPage': 20,
                'maxValuesPerFacet': 100,
                'minWordSizefor1Typo': 4,
                'minWordSizefor2Typos': 8,
                'numericAttributesToIndex': None,
                'optionalWords': None,
                'paginationLimitedTo': 1000,
                'queryType': 'prefixLast',
                'ranking': ['typo',
                            'geo',
                            'words',
                            'filters',
                            'proximity',
                            'attribute',
                            'exact',
                            'custom'],
                'removeWordsIfNoResults': 'none',
                'searchableAttributes': ['title'],
                'separatorsToIndex': '',
                'snippetEllipsisText': '',
                'unretrievableAttributes': None,
                'version': 2}

    @staticmethod
    def _get_collections(data):
        shop = data.get('shop', None)
        product_id = data.get("data", {}).get("id", None)
        if isinstance(shop, Shop) and product_id:
            smart_collections = shop.api.SmartCollection.find(product_id=product_id)
            custom_collections = shop.api.CustomCollection.find(product_id=product_id)

            smart_collections = [dict(only(c.attributes, *['title', 'handle', 'id']), collectionType='smart') for c in
                                 smart_collections]
            custom_collections = [dict(only(c.attributes, *['title', 'handle', 'id']), collectionType='custom') for c in
                                  custom_collections]

            return smart_collections + custom_collections

    def get_suggestions(self):
        pass

    def get_raw_record(self, data):

        def normalize_variant(variant):
            if 'price' in variant and not isinstance(variant['price'], float):
                try:
                    variant['price'] = float(variant['price'])
                except:
                    logger.error('Invalid variant price')

            return variant

        variants = data.get('data', {}).get('variants', [])
        if variants:
            data['data']['variants'] = list(map(normalize_variant, variants))

        return super(ProductIndex, self).get_raw_record(data)

    def normalize_data(self, data):
        if 'variants' in data and data['variants']:
            for v in data['variants']:
                if not v.get('inventory_quantity', None) and v.get('old_inventory_quantity', None):
                    v['inventory_quantity'] = v.get('old_inventory_quantity', None)

        if 'image' in data and not data.get('image'):
            del data['image']

        return data

    def get_extra_data(self, data):
        image = data.get("data", {}).get("image", {}) or {}

        extra_data = {
            "suggestionId": data.get("data", {}).get("handle"),
            "suggestionTitle": data.get("data", {}).get("title"),
            # very basic and error prone. See: https://www.shopify.com/partners/blog/img-url-filter
            "suggestionThumbnail": image.get("src", "").replace('.jpg', '_25x25_crop_center.jpg'),
            # "suggestionSubtitle": '',
            "suggestionType": "product",
        }

        if data.get("data", {}).get("collections", None) is None:
            extra_data['collections'] = self._get_collections(data)

        try:
            if data.get("data", {}).get('variants'):
                prices = [v['price'] for v in data.get("data", {}).get('variants')]
                extra_data['min_price'] = min(prices)
                extra_data['max_price'] = max(prices)
        except:
            logger.warning('Error during price range calculation')

        return extra_data

    def get_collection_map_by_product_id(self, shop):
        smart_collections = list(shop.api.SmartCollection.find_all())
        custom_collections = list(shop.api.CustomCollection.find_all())
        shopify_collections = smart_collections + custom_collections

        collection_map_by_product_id = {}
        for c in shopify_collections:
            for p in c.all_products():
                collectionType = 'custom' if isinstance(c, shop.api.CustomCollection) else 'smart'
                coll_data = dict(only(c.attributes, *['title', 'handle', 'id']), collectionType=collectionType)
                if p.id in collection_map_by_product_id:
                    collection_map_by_product_id[p.id].append(coll_data)
                else:
                    collection_map_by_product_id[p.id] = [coll_data]
        return collection_map_by_product_id

    def prepare_resources_from_shopify_instance_products_list(self, products, shop):
        collection_map_by_product_id = self.get_collection_map_by_product_id(shop)

        resources = []
        for r in products:
            obj_data = r.to_dict()
            obj_data.update(r._get_product_data_for_sync(from_dict=True))
            obj_data = self.normalize_data(obj_data)

            try:
                obj_data['collections'] = collection_map_by_product_id[r.id]
            except KeyError:
                obj_data['collections'] = []

            obj = {'id': r.id, 'data': obj_data, 'shop': shop}
            resources.append(obj)

        return resources

    def get_all_resources_for_shop(self, shop):
        return self.prepare_resources_from_shopify_instance_products_list(shop.api.Product.find_all(), shop)

    def __str__(self):
        return self.index_name


@python_2_unicode_compatible
class CollectionIndex(AlgoliaIndex):
    index_name = index_name_template.format(index_name_prefix, collection_index_name)

    def get_extra_data(self, data):
        extra_data = {
            "suggestionId": data.get("data", {}).get("handle"),
            "suggestionTitle": data.get("data", {}).get("title"),
            # very basic and error prone. See: https://www.shopify.com/partners/blog/img-url-filter
            "suggestionThumbnail": data.get("data", {}).get("image", {}).get("src", '').replace('.jpg',
                                                                                                '_25x25_crop_center.jpg'),
            # "suggestionSubtitle": '',
            "suggestionType": "collection",
        }
        return extra_data

    def __str__(self):
        return self.index_name

    def is_smart_collection(self, data):
        return True if data.get('rules') else False

    def is_custom_collection(self, data):
        return False if data.get('rules') else True

    def update_connected_products(self, data):
        collection_data = data.get('data')
        shop = data.get('shop')
        collection_id = collection_data.get('id')

        if self.is_custom_collection(collection_data):
            collection = shop.api.CustomCollection(collection_data)
        elif self.is_smart_collection(collection_data):
            collection = shop.api.SmartCollection(collection_data)

        with product_index.with_client(self.client) as index:
            products = list(collection.all_products())
            logger.info('Update {} products connected to collection id:{}'.format(len(products), collection_id))

            resources = index.prepare_resources_from_shopify_instance_products_list(products, shop)
            index.save_records(resources)

    def clean_connected_algolia_products(self, data):
        collection_data = data.get('data')
        collection_id = collection_data.get('id')
        with product_index.with_client(self.client) as index:
            query = ''
            params = {
                'query': query,
                'facetFilters': ["collections.id: {}".format(collection_id)],
                'attributesToRetrieve': 'collections',

            }
            hits = index.index.browse_all(params)

            update_list = []
            for p in hits:
                update_object = {
                    'objectID': p.get('objectID'),
                    'collections': [c for c in p.get('collections') if c.get('id') != collection_id]
                }
                update_list.append(update_object)

            logger.info('Clean {} products connected to collectio id:{}'.format(len(update_list), collection_id))
            index.index.save_objects(update_list)

    def save_record(self, data):
        super(CollectionIndex, self).save_record(data)
        self.clean_connected_algolia_products(data)
        self.update_connected_products(data)

    def delete_record(self, data):
        super(CollectionIndex, self).delete_record(data)
        self.clean_connected_algolia_products(data)


# Instantiate indexes
product_index = ProductIndex()
collection_index = CollectionIndex()


def get_client(**kwargs):
    shop = kwargs.get('shop')
    if not isinstance(shop, Shop):
        domain = kwargs["domain"]
        try:
            shop = Shop.objects.get(domain=domain)
        except Shop.DoesNotExist:
            raise Exception("You need to create a Shop for domain {}".format(domain))

    try:
        return shop.algolia_application.get_client()

    except AttributeError:
        raise Exception("No AlgoliaApplication for Shop {}".format(shop))


# signal handlers
def update_product(sender, **kwargs):
    try:
        client = get_client(**kwargs)
        with product_index.with_client(client) as index:
            index.save_record(kwargs)
    except Exception as err:
        logger.exception("Signal handler update_product got an error: \n{}".format(err))
        traceback.print_exc()


def delete_product(sender, **kwargs):
    try:
        client = get_client(**kwargs)
        with collection_index.with_client(client) as index:
            index.delete_record(kwargs)
    except Exception as err:
        logger.exception("Signal handler delete_product got an error: \n{} ".format(err))


def update_collection(sender, **kwargs):
    # print('Update collection')
    # pprint.pprint(kwargs)
    try:
        client = get_client(**kwargs)
        with collection_index.with_client(client) as index:
            index.save_record(kwargs)
    except Exception as err:
        logger.exception("Signal handler update_collection got an error: \n{} ".format(err))


def delete_collection(sender, **kwargs):
    # print('Delete collection')
    # pprint.pprint(kwargs)
    try:
        client = get_client(**kwargs)
        with collection_index.with_client(client) as index:
            index.delete_record(kwargs)
    except Exception as err:
        logger.exception("Signal handler delete_collection got an error: \n{} ".format(err))


def async_indexing_task(data, domain, topic):
    logger.info('Start indexing_task domain:{} topic:{} data.id:{}'.format(domain, topic, data.get('id')))

    try:
        shop = Shop.objects.get(domain=domain)
    except Shop.DoesNotExist:
        raise Exception("You need to create a Shop for domain {}".format(domain))

    kwargs = {
        'data': data,
        'domain': domain,
        'topic': topic,
        'shop': shop
    }

    if topic in ['products/update', 'products/create']:
        update_product('Async task', **kwargs)
    elif topic in ['products/delete']:
        delete_product('Async task', **kwargs)
    elif topic in ['collections/update', 'collections/create']:
        update_collection('Async task', **kwargs)
    elif topic in ['collections/delete']:
        delete_collection('Async task', **kwargs)
    else:
        logger.error('No valid action for topic:{}'.format(topic))


def async_task_emitter(sender, **kwargs):
    # Recive classc signal and generate async task

    data = kwargs.get('data', None)
    domain = kwargs.get('domain', None)
    topic = kwargs.get('topic', None)

    if not all([data, domain, topic]):
        logger.error('async_task_emitter invalid data')
        return

    task_kwargs = {
        'data': data,
        'domain': domain,
        'topic': topic,
    }

    task_id = async_task('django_shopify_synchronizer.indexing.indexing.async_indexing_task', timeout=24 * 60 * 60,
                         **task_kwargs)
    logger.info('Register new async_indexing_task {} for domain:{} topic:{} data.id:{}'.format(task_id, domain, topic,
                                                                                               data.get('id')))

    return task_id
    # print('async_task_emitter', sender, kwargs)


def setup_indexing():
    # connect recived signal (generated by webhook) to async_connection
    webhook_received.connect(async_task_emitter)

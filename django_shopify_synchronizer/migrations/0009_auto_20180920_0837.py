# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-09-20 08:37
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('django_shopify_synchronizer', '0008_auto_20180914_1317'),
    ]

    operations = [
        migrations.AlterField(
            model_name='syncdirectivemap',
            name='dst',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='directives_targets', to='django_shopify_synchronizer.Shop', verbose_name='Sync to shop'),
        ),
    ]

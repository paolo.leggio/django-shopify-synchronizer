# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-09-20 15:30
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        ('django_shopify_synchronizer', '0008_auto_20180914_1317'),
    ]

    operations = [
        migrations.CreateModel(
            name='AlgoliaApplication',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('label', models.CharField(max_length=255, verbose_name='Application Label')),
                ('application_id', models.CharField(max_length=10, verbose_name='Application Id')),
                ('write_api_key', models.CharField(help_text='This is a private API key. Please keep it secret and use it ONLY from your backend: this\n    key is used to create, update and DELETE your indices. You CANNOT use this key manage your API keys.', max_length=32, verbose_name='Write Api Key')),
                ('search_api_key', models.CharField(blank=True, help_text="This is the public API key which can be safely used in your frontend code.This key is\n    usable for search queries and it's also able to list the indices you've got access to.", max_length=32, null=True, verbose_name='Search Api Key')),
                ('admin_api_key', models.CharField(help_text='This is the ADMIN API key. Please keep it secret and use it ONLY from your backend: this\n    key is used to create, update and DELETE your indices. You can also use it to manage your API keys.', max_length=32, verbose_name='Admin Api Key')),
                ('monitoring_api_key', models.CharField(blank=True, help_text='This key is used to access the Monitoring API.', max_length=32, null=True, verbose_name='Monitoring Api Key')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='shop',
            name='algolia_application',
            field=models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to='django_shopify_synchronizer.AlgoliaApplication'),
        ),
    ]

============
Installation
============

At the command line::

    $ easy_install django-shopify-synchronizer

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv django-shopify-synchronizer
    $ pip install django-shopify-synchronizer

=====
Usage
=====

To use Django Shopify to Shopify synchronizer in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'django_shopify_synchronizer.apps.DjangoShopifySynchronizerConfig',
        ...
    )

Add Django Shopify to Shopify synchronizer's URL patterns:

.. code-block:: python

    from django_shopify_synchronizer import urls as django_shopify_synchronizer_urls


    urlpatterns = [
        ...
        url(r'^', include(django_shopify_synchronizer_urls)),
        ...
    ]

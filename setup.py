#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import re
import sys

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


def get_version(*file_paths):
    """Retrieves the version from django_shopify_synchronizer/__init__.py"""
    filename = os.path.join(os.path.dirname(__file__), *file_paths)
    version_file = open(filename).read()
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError('Unable to find version string.')


version = get_version("django_shopify_synchronizer", "__init__.py")

if sys.argv[-1] == 'publish':
    try:
        import wheel

        print("Wheel version: ", wheel.__version__)
    except ImportError:
        print('Wheel library missing. Please run "pip install wheel"')
        sys.exit()
    os.system('python setup.py sdist upload')
    os.system('python setup.py bdist_wheel upload')
    sys.exit()

if sys.argv[-1] == 'tag':
    print("Tagging the version on git:")
    os.system("git tag -a %s -m 'version %s'" % (version, version))
    os.system("git push --tags")
    sys.exit()

readme = open('README.rst').read()
history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='django-shopify-synchronizer',
    version=version,
    description="""Sync shopify to Shopify by webhook""",
    long_description=readme + '\n\n' + history,
    author='Paolo Leggio',
    author_email='paolo@digitalmonkeys.it',
    url='https://github.com/pa0lin082/django-shopify-synchronizer',
    packages=[
        'django_shopify_synchronizer',
    ],
    include_package_data=True,
    install_requires=[
        'ShopifyAPI==3.1.0',
        'django-q>=0.8.1',
        'jsonfield>=2.0.2',
        'Cerberus==1.2',
        'algoliasearch>=1.17.0',
        'tabulate==0.8.2',
        'boto3>=1.9.62',
        'botocore>=1.12.62',
        'deepdiff==3.3.0',
        'gql==0.1.0',
        'graphql-core==2.1'
    ],
    license="MIT",
    zip_safe=False,
    keywords='django-shopify-synchronizer',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Framework :: Django :: 1.11',
        'Framework :: Django :: 2.0',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
)

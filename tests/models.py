from django.db import models


class Cultivar(models.Model):
    slug = models.SlugField(max_length=255, unique=True)
    name = models.CharField(max_length=255)
    description = models.TextField()

    def get_name(self):
        return self.name

    # See: https://www.kurup.org/blog/2014/07/21/django-test-models
    class Meta:
        app_label = 'tests'

#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_django-shopify-synchronizer
------------

Tests for `django-shopify-synchronizer` models module.
"""
import pytest
from django.test import TestCase
import pprint
from django_shopify_synchronizer.helpers import get_api_for_domain
from django_shopify_synchronizer.models import Shop
from gql import gql, Client
from gql.transport.requests import RequestsHTTPTransport

@pytest.mark.slow
class TestShopifyStorefrontApi(TestCase):
    def setUp(self):
        self.shop1 = Shop.objects.create(
            domain='test-sync-s2s-dest.myshopify.com',
            api_key='a4f66378ca81584d75ea189da0c3b825',
            password='74bf7078426eab655bcf9a9b433840a3',
            shared_secret='a6c0e077d128b0e2375287607f68591c',
            storefront_access_token='8de4299db300d527a8a22a5c054999ea'
        )

        self.shop1_storefront_access_token = '8de4299db300d527a8a22a5c054999ea'

        self.shop2 = Shop.objects.create(
            domain='test-sync-s2s-source.myshopify.com',
            api_key='451a6bb8807ac28369cf26fd6483830b',
            password='f09d7eb4f4acb46398f38e6fdd1feea4',
            shared_secret='a46b674a8c91afd69c8e2b01a7ffb2bd'
        )

    def tearDown(self):
        pass


    def test_storefront_api(self):
        query = gql("""
                {
                  shop {
                    name
                  }
                }

                """)

        assert self.shop1.storefront.execute(query) == {
            "shop": {
                "name": self.shop1.name
            }
        }

    def test_dev(self):


        _transport = RequestsHTTPTransport(
            url='https://{}/api/graphql'.format(self.shop1.domain),
            use_json=True,
            headers={
                'X-Shopify-Storefront-Access-Token':self.shop1_storefront_access_token
            }
        )
        client = Client(
            transport=_transport,
            # fetch_schema_from_transport=True,
        )

        query = gql("""
        {
          shop {
            name
          }
        }

        """)
        print(client.execute(query))



        query = gql("""
            mutation customerAccessTokenCreate($input: CustomerAccessTokenCreateInput!) {
              customerAccessTokenCreate(input: $input) {
                customerAccessToken {
                  accessToken
                  expiresAt
                }
                customerUserErrors {
                  code
                  field
                  message
                }
              }
            }
        """)

        res = client.execute(query, {
            'input': {
                "email": "paolo@digitalmonkeys.it",
                "password": "peppe2"
            }
        })

        print(res, type(res))
        customerAccessToken = res['customerAccessTokenCreate']['customerAccessToken']['accessToken']
        assert customerAccessToken



        # # Test cambio password
        # query = gql("""
        # mutation customerUpdate($customerAccessToken: String!, $customer: CustomerUpdateInput!) {
        #   customerUpdate(customerAccessToken: $customerAccessToken, customer: $customer) {
        #     customer {
        #       id
        #     }
        #     customerAccessToken {
        #       accessToken
        #       expiresAt
        #     }
        #     customerUserErrors {
        #       code
        #       field
        #       message
        #     }
        #   }
        # }
        # """)
        #
        # print(client.execute(query,
        #     {
        #       "customerAccessToken": customerAccessToken,
        #       "customer": {
        #           "password": 'peppe2'
        #       }
        #     }
        # ))


    def test_base_api(self):
        search = self.shop1.api.Customer.search(query='email:paolo@digitalmonkeys.it')
        c = search[0]
        print(c)
        pprint.pprint(c.__dict__)
        print('-'*20    )

        customer = self.shop1.api.Customer.create({
            'email':'paolo2@digitalmonkeys.it',
            'password': 'foobarbaz123',
            'password_confirmation': 'foobarbaz123',
            "verified_email": True,
            "send_email_welcome": False
        })
        print(customer,customer.__dict__)
        # res = customer.save()

        print('error    :', customer.errors.__dict__)
        # print('res:',res, customer.errors.__dict__)

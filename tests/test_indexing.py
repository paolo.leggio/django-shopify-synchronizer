#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_django-shopify-algolia-synchronizer
------------

Tests for `django-shopify-algolia-synchronizer` tag manager module
"""

from django.test import TestCase
from mock import patch
from algoliasearch import algoliasearch

from django_shopify_synchronizer.indexing.indexing import ProductIndex
from django_shopify_synchronizer.models import Shop, AlgoliaApplication


class TestIndexing(TestCase):
    def setUp(self):
        self.algolia = AlgoliaApplication.objects.create()
        self.shop1 = Shop.objects.create(
            domain='test-sync-s2s-dest.myshopify.com',
            api_key='a4f66378ca81584d75ea189da0c3b825',
            password='74bf7078426eab655bcf9a9b433840a3',
            shared_secret='a6c0e077d128b0e2375287607f68591c',
            storefront_access_token='8de4299db300d527a8a22a5c054999ea',
            algolia_application=self.algolia
        )


    def test_product_index(self):
        index = ProductIndex()
        self.assertEqual(type(index), ProductIndex)
        import algoliasearch.index

    @patch('algoliasearch.index.Index.save_object')
    def test_product_update(self, mock_method):
        client = algoliasearch.Client('xxx', 'xxx')

        index = ProductIndex()

        with index.with_client(client) as index:
            mock_method.assert_not_called()
            index.save_record({
                'data': {'VendorProductId': '1',
                         'id':1,
                         'body_html': 'Descrizione della Abelia Grandiflora in vaso da 16 cm',
                         'images': [{'position': 1,
                                     'src': 'https://cdn.shopify.com/s/files/1/0014/5368/6854/products/uytuyt.png?v=1532078025'}],
                         'options': [{'name': 'Diametro vaso'}],
                         'product_type': 'Pianta in vaso',
                         'published': True,
                         'tags': ['Pianta in vaso', 'Pacco', 'yg:cultivar:abelia-grandiflora'],
                         'title': 'Abelia Grandiflora in vaso da 16cm',
                         'variants': [
                             {
                                 'grams': 2000.0,
                                 'price': 20.0,
                                 'sku': '123',
                                 'option1': '16 cm',
                                 'title': 'Abelia Grandiflora in vaso da 16cm',
                                 'position': 1
                             },
                             {
                                 'grams': 1000.0,
                                 'price': 50.0,
                                 'sku': '123',
                                 'option1': '18 cm',
                                 'title': 'Abelia Grandiflora in vaso da 16cm',
                                 'position': 2
                             }

                         ]}

            })
            mock_method.assert_called_with(

                {'suggestionType': 'product',
                 'product_type': 'Pianta in vaso',
                 'objectID': 1,
                 'title': 'Abelia Grandiflora in vaso da 16cm',
                 'suggestionTitle': 'Abelia Grandiflora in vaso da 16cm',
                 'published': True,
                 'body_html': 'Descrizione della Abelia Grandiflora in vaso da 16 cm',
                 'id': 1,
                 'suggestionId': None,
                 'min_price': 20.0,
                 'max_price': 50.0,
                 'collections': None,
                 'VendorProductId': '1',
                 'images': [
                    {'position': 1,
                     'src': 'https://cdn.shopify.com/s/files/1/0014/5368/6854/products/uytuyt.png?v=1532078025'}
                 ],
                 'variants': [
                     {'sku': '123',
                      'grams': 2000.0,
                      'title': 'Abelia Grandiflora in vaso da 16cm',
                      'price': 20.0,
                      'position': 1,
                      'option1': '16 cm'
                      },
                     {'sku': '123',
                      'grams': 1000.0,
                      'title': 'Abelia Grandiflora in vaso da 16cm',
                      'price': 50.0,
                      'position': 2,
                      'option1': '18 cm'}
                 ],
                 'options': [{'name': 'Diametro vaso'}],
                 'tags': ['Pianta in vaso', 'Pacco', 'yg:cultivar:abelia-grandiflora'], 'suggestionThumbnail': ''}

                )

    @patch('algoliasearch.index.Index.save_object')
    def test_product_update_check_price_type_is_float(self, mock_method):
        client = algoliasearch.Client('xxx', 'xxx')

        index = ProductIndex()

        with index.with_client(client) as index:
            mock_method.assert_not_called()
            index.save_record({
                'data': {'VendorProductId': '1',
                         'id': 1,
                         'body_html': 'Descrizione della Abelia Grandiflora in vaso da 16 cm',
                         'images': [{'position': 1,
                                     'src': 'https://cdn.shopify.com/s/files/1/0014/5368/6854/products/uytuyt.png?v=1532078025'}],
                         'options': [{'name': 'Diametro vaso'}],
                         'product_type': 'Pianta in vaso',
                         'published': True,
                         'tags': ['Pianta in vaso', 'Pacco', 'yg:cultivar:abelia-grandiflora'],
                         'title': 'Abelia Grandiflora in vaso da 16cm',
                         'variants': [
                             {
                                 'grams': 2000.0,
                                 'price': "20.0",
                                 'sku': '123',
                                 'option1': '16 cm',
                                 'title': 'Abelia Grandiflora in vaso da 16cm',
                                 'position': 1
                             },
                             {
                                 'grams': 1000.0,
                                 'price': "50.0",
                                 'sku': '123',
                                 'option1': '18 cm',
                                 'title': 'Abelia Grandiflora in vaso da 16cm',
                                 'position': 2
                             }

                         ]}

            })
            mock_method.assert_called_with(

                {'suggestionType': 'product',
                 'product_type': 'Pianta in vaso',
                 'objectID': 1,
                 'title': 'Abelia Grandiflora in vaso da 16cm',
                 'suggestionTitle': 'Abelia Grandiflora in vaso da 16cm',
                 'published': True,
                 'body_html': 'Descrizione della Abelia Grandiflora in vaso da 16 cm',
                 'id': 1,
                 'suggestionId': None,
                 'min_price': 20.0,
                 'max_price': 50.0,
                 'collections': None,
                 'VendorProductId': '1',
                 'images': [
                     {'position': 1,
                      'src': 'https://cdn.shopify.com/s/files/1/0014/5368/6854/products/uytuyt.png?v=1532078025'}
                 ],
                 'variants': [
                     {'sku': '123',
                      'grams': 2000.0,
                      'title': 'Abelia Grandiflora in vaso da 16cm',
                      'price': 20.0,
                      'position': 1,
                      'option1': '16 cm'
                      },
                     {'sku': '123',
                      'grams': 1000.0,
                      'title': 'Abelia Grandiflora in vaso da 16cm',
                      'price': 50.0,
                      'position': 2,
                      'option1': '18 cm'}
                 ],
                 'options': [{'name': 'Diametro vaso'}],
                 'tags': ['Pianta in vaso', 'Pacco', 'yg:cultivar:abelia-grandiflora'], 'suggestionThumbnail': ''}

            )


    def test_reindex_all_of_shop(self):
        index = ProductIndex()
        index.reindex_all_of_shop(self.shop1)

        # mock_get_client.assert_called()

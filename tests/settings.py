# -*- coding: utf-8
from __future__ import unicode_literals, absolute_import

import django

DEBUG = True
USE_TZ = True



# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "3%6_-&0&_n%&1(j60$lo9+r@32yf(gn2%$jh&^ytb@%j5$ld$i"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": ":memory:",
    }
}

ROOT_URLCONF = "tests.urls"

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sites",
    "django.contrib.admin",
    "django_shopify_synchronizer",
    "tests",
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
    },
]

SITE_ID = 1

if django.VERSION >= (1, 10):
    MIDDLEWARE = ()
else:
    MIDDLEWARE_CLASSES = ()

import logging
import pprint

class testFormatter(logging.Formatter):
    def format(self, record):
        # return pprint.pformat(record.__dict__, indent=4)
        return '{} - {}'.format(record.levelname,record.msg) % record.args

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'DEBUG',
        'handlers': ['console'],
    },
    'formatters': {
        'verbose': {
            '()': testFormatter
        },
    },
    'handlers': {
        'console': {
            'level': 'WARNING',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },

    },

}

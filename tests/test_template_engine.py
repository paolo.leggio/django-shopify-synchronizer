#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_django-shopify-synchronizer
------------

Tests for `django-shopify-synchronizer` models module.
"""

from django.test import TestCase
import pytest
from django.template import engines
from django.template import Template as DjangoTemplate, Context,  Node
from django.template.exceptions import TemplateDoesNotExist
from django_shopify_synchronizer.templates import ShopThemeTemplate, ShopifyEngine



class TestTemplateEngine(TestCase):

    def test_django_template_vars(self):
        t = DjangoTemplate("My name is {{ name }}.")
        c = Context({"name": "Paolo"})
        res = t.render(c)
        assert res == "My name is Paolo."


    def test_django_template_vars_ignore_custom_tag(self):
        t = DjangoTemplate("My name is {{ name }}. << foo >>")
        c = Context({"name": "Paolo"})
        res = t.render(c)
        assert res == "My name is Paolo. << foo >>"


    def test_custom_template_vars_ignore_orig_tag(self):
        t = ShopThemeTemplate("My name is {{ name }}.<< name >>")
        c = Context({"name": "Paolo"})
        res = t.render(c)
        assert res == "My name is {{ name }}.Paolo"


    def test_base_template_engine_ignore_custom_tag(self):
        t = ShopThemeTemplate("My name is {{ name }}. << foo >>")
        c = Context({"name": "Paolo"})
        res = t.render(c)
        assert res == "My name is {{ name }}. "

    def test_django_foreach(self):
        t = DjangoTemplate('{% for i in i|rjust:5 %}{{ forloop.counter }}{% endfor %}' )
        res = t.render(Context({}))
        assert res == "12345"

    def test_custom_foreach(self):
        t = ShopThemeTemplate('<% for i in i|rjust:5 %><< forloop.counter >><% endfor %>' )
        res = t.render(Context({}))
        assert res == "12345"

    def test_custom_foreach_and_vars(self):
        t = ShopThemeTemplate('<% for i in i|rjust:5 %>{{name}}_<< forloop.counter >>_<<name>> <% endfor %>' )
        res = t.render(Context({"name": "Paolo"}))
        assert res == "{{name}}_1_Paolo {{name}}_2_Paolo {{name}}_3_Paolo {{name}}_4_Paolo {{name}}_5_Paolo "

    def test_django_include_fail(self):
        t = ShopThemeTemplate("<< name >> xxx <% include 'pluto' %> xxx << name >>")

        c = Context({"name": "Paolo"})
        with pytest.raises(TemplateDoesNotExist):
            res = t.render(c)


    def test_custom_include_fail(self):
        t = DjangoTemplate("{{ name }} xxx {% include 'null.html' %} xxx {{ name }}")

        c = Context({"name": "Paolo"})
        with pytest.raises(TemplateDoesNotExist):
            res = t.render(c)



    def test_shopify_engine(self):
        e = ShopifyEngine



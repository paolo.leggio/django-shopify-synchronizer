#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_import
------------

Tests for `django-shopify-synchronizer` import .
"""

import os
import pprint
import pytest
from django.test import TestCase
from django_shopify_synchronizer.parsers.csv_parser import CSVParser
from django_shopify_synchronizer.parsers.base import ParsingError

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class TestCsvParser(TestCase):
    @pytest.mark.slow
    def test_csv_parse_rose_barni(self):
        file_path = '{}/tests/source_files/rose-barni.csv'.format(BASE_DIR)
        parser = CSVParser(file_path)
        parser.load(file_path)

        # pprint.pprint(list(parser.get_items())[0])
        assert list(parser.get_items())[0] == {
            'Body (HTML)': 'Fiori doppi, rosa vivo, nettamente striati di rosso scarlatto. Vigorosa.',
            'Image Alt Text': 'Rosa Ferdinand Pichard ',
            'Image Position': '',
            'Image Src': 'https://shopold.rosebarni.it/immagini/sbig_75090_0.jpg',
            'Option1 Name': '',
            'Option1 Value': '',
            'Option2 Name': '',
            'Option2 Value': '',
            'Option3 Name': '',
            'Option3 Value': '',
            'Published': 'true',
            'Tags': 'Piante, Arbusti, Rose',
            'Title': 'Rosa Ferdinand Pichard ',
            'Type': 'Pianta in vaso',
            'Variant Barcode': '',
            'Variant Compare At Price': '',
            'Variant Fulfillment Service': 'manual',
            'Variant Grams': '2000',
            'Variant Inventory Policy': 'deny',
            'Variant Inventory Qty': '1000',
            'Variant Inventory Tracker': '',
            'Variant Price': '16.00',
            'Variant Requires Shipping': 'true',
            'Variant SKU': '',
            'Variant Taxable': '',
            'VendorProductId': '75090'
        }

        # pprint.pprint(list(parser.get_products())[0])

        assert list(parser.get_products())[0] == {
            'VendorProductId': '75090',
            'body_html': 'Fiori doppi, rosa vivo, nettamente striati di rosso scarlatto. Vigorosa.',
            'images': [{'position': 1,
                        'src': 'https://shopold.rosebarni.it/immagini/sbig_75090_0.jpg'}],
            'product_type': 'Pianta in vaso',
            'published': True,
            'tags': ['Piante', 'Arbusti', 'Rose'],
            'title': 'Rosa Ferdinand Pichard ',
            'variants': [{
                # 'compare_at_price': False,
                'grams': 2000.0,
                'position': 1,
                'price': 16.0,
                'requires_shipping': True,
                # 'sku': '',
                # 'taxable': False,
                'inventory_quantity': 1000,
                'inventory_management': 'shopify',
                'title': 'Rosa Ferdinand Pichard '}]}

    def test_csv_parse_vivai_commande(self):
        file_path = '{}/tests/source_files/vivai-delle-commande.csv'.format(BASE_DIR)
        parser = CSVParser(file_path)
        parser.load(file_path)

        assert list(parser.get_items())[0] == {
            'Body (HTML)': "Fiore semidoppio giallo crema, profumato. Pianta dal portamento compatto e folto, di notevole vigore e dalla fioritura prolungata. Vincitrice della medaglia d'Oro American Peony Society.",
            'Image Alt Text': 'Peonia "Age of Gold"',
            'Image Position': '',
            'Image Src': 'http://www.peonie.it/IMMAGINI/ageofgold9.jpg',
            'Option1 Name': '',
            'Option1 Value': '',
            'Option2 Name': '',
            'Option2 Value': '',
            'Option3 Name': '',
            'Option3 Value': '',
            'Published': 'TRUE',
            'Tags': 'Piante, Peonie',
            'Title': 'Peonia "Age of Gold"',
            'Type': 'Pianta in vaso',
            'Variant Barcode': '',
            'Variant Compare At Price': '',
            'Variant Fulfillment Service': 'manual',
            'Variant Grams': '1000',
            'Variant Inventory Policy': 'deny',
            'Variant Inventory Qty': '',
            'Variant Inventory Tracker': '1000',
            'Variant Price': '0',
            'Variant Requires Shipping': 'TRUE',
            'Variant SKU': '',
            'Variant Taxable': '',
            'VendorProductId': 'peonia-age-of-gold'
        }

        # pprint.pprint(list(parser.get_products())[0])

        assert list(parser.get_products())[0] == {
            'VendorProductId': 'peonia-age-of-gold',
            'body_html': "Fiore semidoppio giallo crema, profumato. Pianta dal portamento compatto e folto, di notevole vigore e dalla fioritura prolungata. Vincitrice della medaglia d'Oro American Peony Society.",
            'images': [{'position': 1,
                        'src': 'http://www.peonie.it/IMMAGINI/ageofgold9.jpg'}],
            # 'options': [],
            'product_type': 'Pianta in vaso',
            'published': True,
            'tags': ['Piante', 'Peonie'],
            'title': 'Peonia "Age of Gold"',
            'variants': [{'grams': 1000.0,
                          # 'option1': '',
                          'position': 1,
                          'price': 0.0,
                          # 'sku': '',
                          'requires_shipping': True,
                          # 'compare_at_price': False,
                          # 'taxable': False,
                          # 'inventory_quantity': -1,
                          'title': 'Peonia "Age of Gold"'}]
        }

    def test_csv_parser_single_line(self):
        file_path = '{}/tests/source_files/import-1 - products_export.csv'.format(BASE_DIR)
        parser = CSVParser(file_path)
        parser.load(file_path)
        # pprint.pprint(list(parser.get_items()))
        assert list(parser.get_items()) == [
            {'Body (HTML)': 'Descrizione della Abelia Grandiflora in vaso da 16 cm',
             'Image Alt Text': 'Immagine Abelia in vaso',
             'Image Position': '1',
             'Image Src': 'https://cdn.shopify.com/s/files/1/0014/5368/6854/products/uytuyt.png?v=1532078025',
             'Option1 Name': 'Diametro vaso',
             'Option1 Value': '16 cm',
             'Published': 'TRUE',
             'Tags': 'Pianta in vaso, Regalo, yg:cultivar:abelia-grandiflora',
             'Title': 'Abelia Grandiflora in vaso da 16cm',
             'Type': 'Pianta in vaso',
             'Variant Grams': '2000',
             'Variant Price': '20',
             'Variant SKU': '123',
             'VendorProductId': '1'}
        ]

        assert list(parser.get_products()) == [
            {
                'body_html': 'Descrizione della Abelia Grandiflora in vaso da 16 cm',
                'tags': ['Pianta in vaso', 'Regalo', 'yg:cultivar:abelia-grandiflora'],
                'published': True,
                'title': 'Abelia Grandiflora in vaso da 16cm',
                'product_type': 'Pianta in vaso',
                'VendorProductId': '1',
                'options': [{'name': 'Diametro vaso'}],
                'variants': [{
                    'grams': 2000.0,
                    'price': 20.0,
                    'sku': '123',
                    'option1': '16 cm',
                    'title': 'Abelia Grandiflora in vaso da 16cm',
                    'position': 1
                }],
                'images': [{
                    'position': 1,
                    'src': 'https://cdn.shopify.com/s/files/1/0014/5368/6854/products/uytuyt.png?v=1532078025'
                }],
            }
        ]

    def test_csv_parser_multi_line(self):
        file_path = '{}/tests/source_files/import-2-added-new-product - products_export.csv'.format(BASE_DIR)
        parser = CSVParser(file_path)
        parser.load(file_path)
        # pprint.pprint(list(parser.get_items()))
        assert list(parser.get_items()) == [
            {'Body (HTML)': 'Descrizione della Abelia Grandiflora in vaso da 16 cm',
             'Image Alt Text': 'Immagine Abelia in vaso',
             'Image Position': '1',
             'Image Src': 'https://cdn.shopify.com/s/files/1/0014/5368/6854/products/uytuyt.png?v=1532078025',
             'Option1 Name': 'Diametro vaso',
             'Option1 Value': '16 cm',
             'Published': 'TRUE',
             'Tags': 'Pianta in vaso, Pacco, yg:cultivar:abelia-grandiflora',
             'Title': 'Abelia Grandiflora in vaso da 16cm',
             'Type': 'Pianta in vaso',
             'Variant Grams': '2000',
             'Variant Price': '20',
             'Variant SKU': '123',
             'VendorProductId': '1'},
            {'Body (HTML)': 'Descrizione dei semi della rosa canina',
             'Image Alt Text': 'Immagine Rosa canina - semi',
             'Image Position': '1',
             'Image Src': 'https://shop.unquadratodigiardino.it/1654-thickbox_default/rosa-canina-alba-.jpg',
             'Option1 Name': 'Numero semi',
             'Option1 Value': '100',
             'Published': 'FALSE',
             'Tags': 'Semi, yg:cultivar:rosa-canina',
             'Title': 'Rosa canina - 12 semi',
             'Type': 'Semi',
             'Variant Grams': '1000',
             'Variant Price': '9.9',
             'Variant SKU': '456',
             'VendorProductId': '2'}
        ]

        # pprint.pprint(list(parser.get_products()))

        assert list(parser.get_products()) == [
            {'VendorProductId': '1',
             'body_html': 'Descrizione della Abelia Grandiflora in vaso da 16 cm',
             'images': [{'position': 1,
                         'src': 'https://cdn.shopify.com/s/files/1/0014/5368/6854/products/uytuyt.png?v=1532078025'}],
             'options': [{'name': 'Diametro vaso'}],
             'product_type': 'Pianta in vaso',
             'published': True,
             'tags': ['Pianta in vaso', 'Pacco', 'yg:cultivar:abelia-grandiflora'],
             'title': 'Abelia Grandiflora in vaso da 16cm',
             'variants': [{'grams': 2000.0,
                           'option1': '16 cm',
                           'position': 1,
                           'price': 20.0,
                           'sku': '123',
                           'title': 'Abelia Grandiflora in vaso da 16cm'}]},
            {'VendorProductId': '2',
             'body_html': 'Descrizione dei semi della rosa canina',
             'images': [{'position': 1,
                         'src': 'https://shop.unquadratodigiardino.it/1654-thickbox_default/rosa-canina-alba-.jpg'}],
             'options': [{'name': 'Numero semi'}],
             'product_type': 'Semi',
             'published': False,
             'tags': ['Semi', 'yg:cultivar:rosa-canina'],
             'title': 'Rosa canina - 12 semi',
             'variants': [{'grams': 1000.0,
                           'option1': '100',
                           'position': 1,
                           'price': 9.9,
                           'sku': '456',
                           'title': 'Rosa canina - 12 semi'}]}
        ]

    def test_parse_vivai_delle_commande(self):
        file_path = '{}/tests/source_files/vivai-delle-commande.csv'.format(BASE_DIR)
        parser = CSVParser(file_path)
        parser.load(file_path)

        assert len(list(parser.get_items())) == 141
        assert len(list(parser.get_items())) == len(list(parser.get_products()))

    def test_parse_csv_without_vendor_product_id(self):
        file_path = '{}/tests/source_files/NO_VENDOR_PRODUCT_ID-demo.csv'.format(BASE_DIR)
        parser = CSVParser(file_path)
        parser.load(file_path)

        assert len(list(parser.get_items())) == 2
        with pytest.raises(ParsingError) as e:
            parser.get_products()
        assert e.value.args == ([{'errors': {'VendorProductId': ['required field']}, 'idx': 0},
                                 {'errors': {'VendorProductId': ['required field']}, 'idx': 1}],)

    def test_base_multiple_images(self):
        file_path = '{}/tests/source_files/test_multiple_images.csv'.format(BASE_DIR)
        parser = CSVParser(file_path)
        parser.load(file_path)
        # pprint.pprint(list(parser.get_items()))
        assert list(parser.get_items()) == [
            {'Body (HTML)': 'Descrizione della Abelia Grandiflora in vaso da 16 cm',
             'Image Alt Text': 'Immagine Abelia in vaso',
             'Image Position': '1',
             'Image Src': 'https://fakeimg.pl/350x200/ffffff/000/',
             'Option1 Name': 'Diametro vaso',
             'Option1 Value': '16 cm',
             'Published': 'TRUE',
             'Tags': 'Pianta in vaso, Regalo, yg:cultivar:abelia-grandiflora',
             'Title': 'Abelia Grandiflora in vaso da 16cm',
             'Type': 'Pianta in vaso',
             'Variant Grams': '2000',
             'Variant Price': '20',
             'Variant SKU': '123',
             'VendorProductId': '1'},
            {'Body (HTML)': '',
             'Image Alt Text': '',
             'Image Position': '',
             'Image Src': 'https://fakeimg.pl/350x200/ffff00/000/',
             'Option1 Name': '',
             'Option1 Value': '',
             'Published': '',
             'Tags': '',
             'Title': '',
             'Type': '',
             'Variant Grams': '',
             'Variant Inventory Qty': '',
             'Variant Price': '',
             'Variant SKU': '',
             'VendorProductId': '1'},
            {'Body (HTML)': '',
             'Image Alt Text': '',
             'Image Position': '',
             'Image Src': 'https://fakeimg.pl/350x200/ff0000/000/',
             'Option1 Name': '',
             'Option1 Value': '',
             'Published': '',
             'Tags': '',
             'Title': '',
             'Type': '',
             'Variant Grams': '',
             'Variant Inventory Qty': '',
             'Variant Price': '',
             'Variant SKU': '',
             'VendorProductId': '1'},
            {'Body (HTML)': '',
             'Image Alt Text': '',
             'Image Position': '',
             'Image Src': 'https://fakeimg.pl/350x200/ff00ff/000/',
             'Option1 Name': '',
             'Option1 Value': '',
             'Published': '',
             'Tags': '',
             'Title': '',
             'Type': '',
             'Variant Grams': '',
             'Variant Inventory Qty': '',
             'Variant Price': '',
             'Variant SKU': '',
             'VendorProductId': '1'}
        ]

        assert len(list(parser.get_products())) == 1

        assert list(parser.get_products()) == [
            {
                'body_html': 'Descrizione della Abelia Grandiflora in vaso da 16 cm',
                'tags': ['Pianta in vaso', 'Regalo', 'yg:cultivar:abelia-grandiflora'],
                'published': True,
                'title': 'Abelia Grandiflora in vaso da 16cm',
                'product_type': 'Pianta in vaso',
                'VendorProductId': '1',
                'options': [{'name': 'Diametro vaso'}],
                'variants': [{
                    'grams': 2000.0,
                    'price': 20.0,
                    'sku': '123',
                    'option1': '16 cm',
                    'title': 'Abelia Grandiflora in vaso da 16cm',
                    'position': 1
                }],
                'images': [
                    {'position': 1, 'src': 'https://fakeimg.pl/350x200/ffffff/000/'},
                    {'position': 2, 'src': 'https://fakeimg.pl/350x200/ffff00/000/'},
                    {'position': 3, 'src': 'https://fakeimg.pl/350x200/ff0000/000/'},
                    {'position': 4, 'src': 'https://fakeimg.pl/350x200/ff00ff/000/'}
                ],
            }
        ]

    def test_base_multiple_variants(self):
        file_path = '{}/tests/source_files/test_multiple_variants.csv'.format(BASE_DIR)
        parser = CSVParser(file_path)
        parser.load(file_path)
        # pprint.pprint(list(parser.get_items()))
        assert list(parser.get_items()) == [
            {'Body (HTML)': 'Descrizione della Abelia Grandiflora in vaso da 16 cm',
             'Image Alt Text': 'Immagine Abelia in vaso',
             'Image Position': '1',
             'Image Src': 'https://fakeimg.pl/350x200/ffffff/000/',
             'Option1 Name': 'Diametro vaso',
             'Option1 Value': '16 cm',
             'Option2 Name': 'Altezza',
             'Option2 Value': '100',
             'Published': 'TRUE',
             'Tags': 'Pianta in vaso, Regalo, yg:cultivar:abelia-grandiflora',
             'Title': 'Abelia Grandiflora in vaso da 16cm',
             'Type': 'Pianta in vaso',
             'Variant Grams': '2000',
             'Variant Inventory Qty': '',
             'Variant Price': '20',
             'Variant SKU': '123',
             'VendorProductId': '1'},
            {'Body (HTML)': '',
             'Image Alt Text': '',
             'Image Position': '',
             'Image Src': '',
             'Option1 Name': 'Diametro vaso',
             'Option1 Value': '20 cm',
             'Option2 Name': 'Altezza',
             'Option2 Value': '150',
             'Published': 'TRUE',
             'Tags': '',
             'Title': 'variant title 1',
             'Type': '',
             'Variant Grams': '2500',
             'Variant Inventory Qty': '',
             'Variant Price': '30',
             'Variant SKU': '124',
             'VendorProductId': '1'},
            {'Body (HTML)': '',
             'Image Alt Text': '',
             'Image Position': '',
             'Image Src': '',
             'Option1 Name': 'Diametro vaso',
             'Option1 Value': '24 cm',
             'Option2 Name': 'Altezza',
             'Option2 Value': '200',
             'Published': 'TRUE',
             'Tags': '',
             'Title': 'variant title 2',
             'Type': '',
             'Variant Grams': '3000',
             'Variant Inventory Qty': '',
             'Variant Price': '40',
             'Variant SKU': '125',
             'VendorProductId': '1'},
            {'Body (HTML)': '',
             'Image Alt Text': '',
             'Image Position': '',
             'Image Src': '',
             'Option1 Name': 'Diametro vaso',
             'Option1 Value': '30 cm',
             'Option2 Name': 'Altezza',
             'Option2 Value': '250',
             'Published': 'TRUE',
             'Tags': '',
             'Title': 'variant title 3',
             'Type': '',
             'Variant Grams': '3500',
             'Variant Inventory Qty': '',
             'Variant Price': '50',
             'Variant SKU': '126',
             'VendorProductId': '1'},
            {'Body (HTML)': '',
             'Image Alt Text': '',
             'Image Position': '',
             'Image Src': '',
             'Option1 Name': 'Diametro vaso',
             'Option1 Value': '80 cm',
             'Option2 Name': 'Altezza',
             'Option2 Value': '300',
             'Published': 'TRUE',
             'Tags': '',
             'Title': 'variant title 4',
             'Type': '',
             'Variant Grams': '4000',
             'Variant Inventory Qty': '',
             'Variant Price': '60',
             'Variant SKU': '127',
             'VendorProductId': '1'},
            {'Body (HTML)': '',
             'Image Alt Text': '',
             'Image Position': '',
             'Image Src': '',
             'Option1 Name': 'Diametro vaso',
             'Option1 Value': '90 cm',
             'Option2 Name': 'Altezza',
             'Option2 Value': '350',
             'Published': 'TRUE',
             'Tags': '',
             'Title': 'variant title 5',
             'Type': '',
             'Variant Grams': '4500',
             'Variant Inventory Qty': '',
             'Variant Price': '70',
             'Variant SKU': '128',
             'VendorProductId': '1'}
        ]

        assert len(list(parser.get_products())) == 1

        # pprint.pprint(parser.get_products())
        assert list(parser.get_products()) == [
            {'VendorProductId': '1',
             'body_html': 'Descrizione della Abelia Grandiflora in vaso da 16 cm',
             'images': [{'position': 1,
                         'src': 'https://fakeimg.pl/350x200/ffffff/000/'}],
             'options': [{'name': 'Diametro vaso'}, {'name': 'Altezza'}],
             'product_type': 'Pianta in vaso',
             'published': True,
             'tags': ['Pianta in vaso', 'Regalo', 'yg:cultivar:abelia-grandiflora'],
             'title': 'Abelia Grandiflora in vaso da 16cm',
             'variants': [{'grams': 2000.0,
                           'option1': '16 cm',
                           'option2': '100',
                           'position': 1,
                           'price': 20.0,
                           'sku': '123',
                           'title': 'Abelia Grandiflora in vaso da 16cm'},
                          {'grams': 2500.0,
                           'option1': '20 cm',
                           'option2': '150',
                           'position': 2,
                           'price': 30.0,
                           'sku': '124',
                           'title': 'variant title 1'},
                          {'grams': 3000.0,
                           'option1': '24 cm',
                           'option2': '200',
                           'position': 3,
                           'price': 40.0,
                           'sku': '125',
                           'title': 'variant title 2'},
                          {'grams': 3500.0,
                           'option1': '30 cm',
                           'option2': '250',
                           'position': 4,
                           'price': 50.0,
                           'sku': '126',
                           'title': 'variant title 3'},
                          {'grams': 4000.0,
                           'option1': '80 cm',
                           'option2': '300',
                           'position': 5,
                           'price': 60.0,
                           'sku': '127',
                           'title': 'variant title 4'},
                          {'grams': 4500.0,
                           'option1': '90 cm',
                           'option2': '350',
                           'position': 6,
                           'price': 70.0,
                           'sku': '128',
                           'title': 'variant title 5'}]}
        ]

#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_django-shopify-synchronizer
------------

Tests for `django-shopify-synchronizer` models module.
"""
import pytest
import datetime
from django.test import TestCase
import pprint
from django_shopify_synchronizer.helpers import get_api_for_domain
from django_shopify_synchronizer.models import Shop, Customer
from gql import gql, Client
from gql.transport.requests import RequestsHTTPTransport
from django.contrib.auth import get_user_model
from django_shopify_synchronizer.factories.common import UserFactory
from django_shopify_synchronizer.factories.shopify import CustomerFactory

from mock import mock


class TestCustomer(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.shop1 = Shop.objects.create(
            domain='test-sync-s2s-dest.myshopify.com',
            api_key='a4f66378ca81584d75ea189da0c3b825',
            password='74bf7078426eab655bcf9a9b433840a3',
            shared_secret='a6c0e077d128b0e2375287607f68591c',
            storefront_access_token='8de4299db300d527a8a22a5c054999ea'
        )

    def setUp(self):
        self.customer = CustomerFactory(shop=self.shop1)

    def test_customer(self):
        assert self.customer.id


    @mock.patch('django_shopify_synchronizer.models.Customer.save')
    @mock.patch('django_shopify_synchronizer.shopify_rest_api.shopify.Customer')
    def test_save_shopify_customer(self, mockCustomerClass, mockSave):

        assert not self.customer.email
        assert not self.customer.password
        self.customer.save_shopify_customer()
        mockCustomerClass.assert_called()
        mockSave.assert_called()

        assert self.customer.email
        assert self.customer.password



    @mock.patch('django_shopify_synchronizer.shopify_rest_api.shopify.Customer')
    def test_make_customer_password(self, mockCustomerClass):
        mockCustomerInstance = mock.MagicMock()
        mockCustomerClass.return_value = mockCustomerInstance
        assert not self.customer.password

        self.customer.make_customer_password()

        assert self.customer.password
        mockCustomerClass.assert_called_with({
            'id': self.customer.shopify_id,
            "password": self.customer.password,
            "password_confirmation": self.customer.password
        })
        mockCustomerInstance.save.assert_called_with()

    @mock.patch('django_shopify_synchronizer.models.shopify_models.get_random_string')
    @mock.patch('django_shopify_synchronizer.shopify_rest_api.shopify.Customer')
    def test_make_customer_password_fail(self, mockCustomerClass, mockRandomPassword):
        mockRandomPassword.return_value = 'xxx'
        mockCustomerClass.return_value.save.return_value = False

        assert not self.customer.password

        with pytest.raises(Exception) as e:
            self.customer.make_customer_password()

        assert not self.customer.password
        mockCustomerClass.assert_called_with({
            'id': self.customer.shopify_id,
            'password': 'xxx',
            'password_confirmation': 'xxx',
        })
        mockCustomerClass.return_value.save.assert_called_with()


        # def test_foo(self):
        #     for i in range(100):
        #         u = UserFactory.build()
        #         print(u.email)
        #         print( u.first_name, u.last_name)


@pytest.mark.slow
class TestCustomerInRealWorld(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.shop1 = Shop.objects.create(
            domain='test-sync-s2s-dest.myshopify.com',
            api_key='a4f66378ca81584d75ea189da0c3b825',
            password='74bf7078426eab655bcf9a9b433840a3',
            shared_secret='a6c0e077d128b0e2375287607f68591c',
            storefront_access_token='8de4299db300d527a8a22a5c054999ea'
        )

        cls.shop1_storefront_access_token = '8de4299db300d527a8a22a5c054999ea'

        cls.shop2 = Shop.objects.create(
            domain='test-sync-s2s-source.myshopify.com',
            api_key='451a6bb8807ac28369cf26fd6483830b',
            password='f09d7eb4f4acb46398f38e6fdd1feea4',
            shared_secret='a46b674a8c91afd69c8e2b01a7ffb2bd',
            primary=True
        )

    def test_test_customer_get_or_create_from_user_with_invalid_email(self):
        user = UserFactory(email='allenkenneth@flowers-barr.com')

        from django_shopify_synchronizer.shopify_rest_api import InvalidEmail

        with pytest.raises(InvalidEmail):
            customer, created = Customer.objects.get_or_create_from_user(user)


    def test_customer_get_or_create_from_user(self):
        user = UserFactory()
        customer, created = Customer.objects.get_or_create_from_user(user)

        assert customer
        assert customer.user == user
        assert customer.id
        assert customer.data
        assert customer.shopify_id == customer.data['id']
        assert customer.shop == self.shop2
        assert customer.email == user.email
        assert customer.password
        assert created

    def test_customer_get_or_create_from_user_custom_shop(self):
        user = UserFactory()
        customer, created = Customer.objects.get_or_create_from_user(user, shop=self.shop1)

        assert customer.shop == self.shop1
        # Default check
        assert customer
        assert customer.user == user
        assert customer.id
        assert customer.data
        assert customer.shopify_id == customer.data['id']
        assert customer.email == user.email
        assert customer.password
        assert created

    def test_customer_customer_access_token(self):
        user = UserFactory()
        customer, created = Customer.objects.get_or_create_from_user(user, shop=self.shop1)

        assert customer.customer_access_token is None

        customer.update_customer_access_token()
        assert customer.customer_access_token
        assert customer.customer_access_token_expire_date
        assert isinstance(customer.customer_access_token_expire_date, datetime.datetime)

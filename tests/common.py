import shutil
import tempfile

from django.conf import settings


class TempMediaMixin(object):
    "Mixin to create MEDIA_ROOT in temp and tear down when complete."

    def _pre_setup(self):
        # print('@@@@@@@@@@@@ setup_test_environment')
        "Create temp directory and update MEDIA_ROOT and default storage."
        super(TempMediaMixin, self)._pre_setup()
        settings._original_media_root = settings.MEDIA_ROOT
        settings._original_file_storage = settings.DEFAULT_FILE_STORAGE
        self._temp_media = tempfile.mkdtemp()
        settings.MEDIA_ROOT = self._temp_media
        settings.DEFAULT_FILE_STORAGE = 'django.core.files.storage.FileSystemStorage'

    def _post_teardown(self):
        "Delete temp storage."
        super(TempMediaMixin, self)._post_teardown()
        shutil.rmtree(self._temp_media, ignore_errors=True)
        settings.MEDIA_ROOT = settings._original_media_root
        del settings._original_media_root
        settings.DEFAULT_FILE_STORAGE = settings._original_file_storage
        del settings._original_file_storage

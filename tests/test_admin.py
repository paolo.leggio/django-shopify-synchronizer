#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_django-shopify-synchronizer
------------

Tests for `django-shopify-synchronizer` models module.
"""

import os
from django.test import TestCase
from django.core.exceptions import ImproperlyConfigured
import pytest
from django_shopify_synchronizer.models import Shop, Product, Vendor, ImportSource, ImportTask, ExternalProductToShopifyProductMap
from django.core.files import File
from tests.common import TempMediaMixin
from django.test.utils import override_settings

from django_shopify_synchronizer.shopify_rest_api import shopify
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class TestAdmin(TestCase):

    def test_admin_init(self):
        from django_shopify_synchronizer.admin import ShopAdmin
        assert ShopAdmin

#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_django-shopify-synchronizer
------------

Tests for `django-shopify-synchronizer` models module.
"""
import pytest
import datetime
from django.test import TestCase
import pprint
from django_shopify_synchronizer.helpers import get_api_for_domain
from django_shopify_synchronizer.models import Shop, Customer
from gql import gql, Client
from gql.transport.requests import RequestsHTTPTransport
from django.contrib.auth import get_user_model
from django_shopify_synchronizer.factories.common import UserFactory
from django_shopify_synchronizer.factories.shopify import CustomerFactory, ProductFactory
from django_shopify_synchronizer.factories.shop import ShopFactory
from django_shopify_synchronizer import signals as shopify_signals
from django_shopify_synchronizer.syncronizer import get_model_class_by_topic, ModelClassByTopicNotfound
from django_shopify_synchronizer.models import Product, Customer
from django.core.exceptions import ImproperlyConfigured

from mock import mock


class TestSyncronizer(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.shop = ShopFactory()

    def test_get_model_class_by_topic(self):
        assert get_model_class_by_topic('products/create') == Product
        assert get_model_class_by_topic('products/update') == Product
        assert get_model_class_by_topic('products/delete') == Product
        assert get_model_class_by_topic('customers/create') == Customer
        assert get_model_class_by_topic('customers/update') == Customer
        assert get_model_class_by_topic('customers/delete') == Customer

        with pytest.raises(ModelClassByTopicNotfound) as excinfo:
            get_model_class_by_topic('foo/bar')

        assert 'foo/bar' in str(excinfo.value)

    def test_update_product(self):
        product = ProductFactory(shop=self.shop)
        assert not product.last_import_date

        new_p_data = {
            'name': 'foo',
            'tags': ['bar','baz']
        }

        wh_data = {
            'id': product.shopify_id,
            'data': new_p_data
        }

        res = shopify_signals.products_create.send_robust(
            sender=self,
            domain=self.shop.domain,
            topic='products/create',
            data=wh_data,
            shop=self.shop
        )

        product.refresh_from_db()
        assert product.data == wh_data
        assert product.last_import_date

    def test_update_customer(self):
        customer = CustomerFactory(shop=self.shop)
        assert not customer.last_import_date

        new_p_data = {
            'name': 'foo',
            'tags': ['bar','baz']
        }

        wh_data = {
            'id': customer.shopify_id,
            'data': new_p_data
        }

        res = shopify_signals.customers_create.send_robust(
            sender=self,
            domain=self.shop.domain,
            topic='customers/create',
            data=wh_data,
            shop=self.shop
        )
        customer.refresh_from_db()
        assert customer.data == wh_data
        assert customer.last_import_date



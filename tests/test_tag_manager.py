#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_django-shopify-synchronizer
------------

Tests for `django-shopify-synchronizer` models module.
"""

from django.test import TestCase
# from mock import patch

from django_shopify_synchronizer.indexing.tags import TagHandler, TagManager, InstanceNotFound, TagHandlerNotFound
from .models import Cultivar


class TestTagHandler(TestCase):

    def setUp(self):
        Cultivar.objects.create(slug="foo", name="Foo", description="This is Foo")

    def test_get_instance(self):
        handler = TagHandler(model=Cultivar, fields=[["name", "get_name"], ["desc", "description"]])
        self.assertIsInstance(handler._get_instance("foo"), Cultivar)

    # def test_get_data(self):
    #     handler = TagHandler(model=Cultivar, fields=[["name", "get_name"], ["desc", "description"]])
    #     self.assertEqual(handler.get_data('foo'), {
    #         "name": "Foo",
    #         "desc": "This is Foo",
    #     })

    def tearDown(self):
        pass


class TestTagManager(TestCase):

    def setUp(self):
        Cultivar.objects.create(slug="abelia-snowdrift", name="Abelia Snowdrift", description="This is an Abelia cultivar")

    # def test_register_a_tag(self):
    #     tag_manager = TagManager(prefix='foo')
    #     tag_manager.register_tag_handler(
    #         handler_key="cultivar",
    #         model=Cultivar,
    #         fields=[["name", "get_name"], ["desc", "description"]]
    #     )
    #     self.assertTrue(tag_manager.is_registered("cultivar"))
    #     self.assertFalse(tag_manager.is_registered("foo"))

    def test_get_tags_data(self):
        tag_manager = TagManager(prefix='bar')
        tag_manager.register_tag_handler(
            handler_key="myhandler",
            model=Cultivar,
            fields=[["name", "get_name"]]
        )
        self.assertEqual(tag_manager.get_tags_data("bar:myhandler:abelia-snowdrift"), {"myhandler": {"name": "Abelia Snowdrift"}})
        with self.assertRaises(TagHandlerNotFound):
            tag_manager.get_tags_data("bar:xxx:abelia-snowdrift")
        with self.assertRaises(InstanceNotFound):
            tag_manager.get_tags_data("bar:myhandler:xxx")

    # @patch("tests.models.Cultivar.objects.get")
    # def test_test_get_relevant_tags(self, mock_method):
    #     tag_manager = TagManager(prefix='bau')
    #     tag_manager.register(tag_name="cultivar", model=Cultivar, attrs=[
    #         {
    #             "label": "name",
    #             "field": "get_name"
    #         },
    #     ])
    #     mock_method.assert_not_called()
    #     tag_manager.get_tags_data('bau:cultivar:abelia')
    #     mock_method.assert_called_with(slug='abelia')

    def tearDown(self):
        pass

#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_import
------------

Tests for `django-shopify-synchronizer` import .
"""

from django.test import TestCase, override_settings
import pytest
from django_shopify_synchronizer.models import Shop, Product,  Vendor, ImportSource, ImportTask, ExternalProductToShopifyProductMap
from django.core.files import File
from django.core.exceptions import ImproperlyConfigured
from django_shopify_synchronizer.helpers import run_import_task
from mock.mock import patch, Mock, MagicMock, PropertyMock

import os
import pprint
import json

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

import shutil
import tempfile
from tests.common import TempMediaMixin
from django.conf import settings
from django_shopify_synchronizer.importer import ImportTaskImporter, \
    BaseImporter, SqsMessageImporter, ACTION_CREATE, ACTION_UPDATE, ACTION_SKIP
from django_shopify_synchronizer.importer.exceptions import ShopNotDefined
from django_shopify_synchronizer.shopify_rest_api import shopify
import random
import string




def rand_string(length=16):
    return ''.join(random.choice(string.digits) for _ in range(length))


class ProductMock(Mock):
    def __init__(self, *args, **kwargs):
        super(ProductMock, self).__init__()
        self._id = kwargs.pop('id', None)
        self._conf = kwargs.pop('conf', None)
        self.save = Mock(name='save', side_effect=self._save, parent=self)
        # self.save.side_effect = self._save

        # self.save/ = self._mock_children.get('save')

        # if 'name' in kwargs and kwargs['name'] == 'Product':
        # self.save = self
        # self.save.side_effect = self._save
        #     print('ProductMock.__init__', args, kwargs, self, self.save)
        #     print('%%%%')
        #     self.save = ProductMock(side_effect=self._save)
        #
        # self.save()
        # self.save.side_effect = self._save
        # print('ProductMock.__init__', args, kwargs, self, self.save)

    def __new__(cls, *args, **kwargs):
        # print('ProductMock.__new__',args,kwargs)
        res = super(ProductMock, cls).__new__(cls, *args, **kwargs)
        # res.save.side_effect = res._save
        # print('New.res ', type(res))
        return res

        # self.save = Mock()
        # self.save.side_effect = self._save

    #
    def __call__(self, *args, **kwargs):
        """ Reset fake data when Product() is called"""
        # print('ProductMock.__call__',args, kwargs, self, self._id)
        call_res = super(ProductMock, self).__call__(*args, **kwargs)
        call_res._id = None
        # print('call_res',args, kwargs, call_res, call_res.id, self, self.id)
        return call_res

    # def __getattr__(self, item):
    #     res = super(ProductMock, self).__getattr__(item)
    #     print ('__getattr__', item, res, type(res))
    #
    #     local_attr_name =  '_{}'.format(item)
    #     if hasattr(self, local_attr_name) and isinstance(res, Mock) :
    #         res.side_effect = getattr(self,local_attr_name)
    #         print(self.__class__, res, isinstance(res, Mock))
    #         print('Has attr')
    #     return res

    # def _mock_check_sig(self, *args, **kwargs):
    #     print('_mock_check_sig',self._mock_name,args,kwargs)
    #     # stub method that can be replaced with one with a specific signature
    #     pass

    @property
    def id(self):
        # print('ProductMock.id',self, self._id, self._conf)
        return self._id

    def to_dict(self):
        return {'id':self.id}

    def _save(self):
        # super(ProductMock, self).save()
        # super(ProductMock, self).save()
        # print('ProductMock.save', self, self._id, self._conf)
        if not self._id:
            self._id = rand_string()
        # print('ProductMock.save--',self._id)
        return True

    def find(self, id):
        # print('ProductMock.find', id, self._conf, self._mock_return_value._conf, self._id)
        self._id = id
        self._mock_return_value._id = id
        return self._mock_return_value


class TestMock(TestCase):
    @patch('django_shopify_synchronizer.shopify_rest_api.shopify.Product', new_callable=ProductMock, conf='abracadabra')
    def test_mock(self, p_mock):
        sp = shopify.Product()
        # print('sp',sp)
        assert sp.id == None
        sp.save()
        assert sp.id

        sp2 = shopify.Product()
        # print('sp2',sp2)
        assert sp2.id == None

        sp3 = shopify.Product.find(2222)
        # print('sp3',sp3)
        assert sp3.id == 2222

        # print('p_mock',p_mock, type(p_mock), p_mock.__dict__)

        # p_mock.save.assert_called()


def foo():
    pass

class TestBaseImporter(TempMediaMixin, TestCase):
    def setUp(self):
        self.shop = Shop.objects.create(
            domain='test-sync-s2s-dest.myshopify.com',
            api_key='a4f66378ca81584d75ea189da0c3b825',
            password='74bf7078426eab655bcf9a9b433840a3',
            shared_secret='a6c0e077d128b0e2375287607f68591c'
        )

    def test_init_base_importer_without_shop(self):
        with pytest.raises(ShopNotDefined):
            importer = BaseImporter()

    def test_init_base_importer_with_shop(self):
        importer = BaseImporter(self.shop)

    def test_init_base_importer_with_default_normalizer_classes(self):
        importer = BaseImporter(self.shop)
        assert isinstance(importer.normalizer_classes, list)
        assert len(importer.normalizer_classes) == 0

    @override_settings(DSS_IMPORTER={
        'NORMALIZER_CLASSES': ('tests.test_import.TestBaseImporter','tests.test_import.foo')
    })
    def test_init_base_importer_with_custom_normalizer_classes(self):
        importer = BaseImporter(self.shop)
        assert isinstance(importer.normalizer_classes, list)
        assert len(importer.normalizer_classes) == 2
        assert issubclass(importer.normalizer_classes[0],  TestBaseImporter)
        assert importer.normalizer_classes[1] == foo


class TestSqsMessageImporter(TempMediaMixin, TestCase):
    def setUp(self):
        self.shop = Shop.objects.create(
            domain='test-sync-s2s-dest.myshopify.com',
            api_key='a4f66378ca81584d75ea189da0c3b825',
            password='74bf7078426eab655bcf9a9b433840a3',
            shared_secret='a6c0e077d128b0e2375287607f68591c'
        )

        self.vendor, created = Vendor.objects.get_or_create(name='test_vendor')

        message_attributes = {
                'VendorName': {'StringValue':'Test vendor'},
                'VendorCode': {'StringValue':'test_vendor'},
            }


        self.src_data = {
            'VendorProductId': u'https://www.geelfloricultura.it/vendita-online/acetosa-araba/',
            'title': u'Acetosa araba',
            'body_html': u' <p>Specie appartenente al genere Rumex, R. versicarius è un’erbacea a ciclo annuale presente allo stato spontaneo in Europa, Africa e Asia.</p> <p>Il fogliame dal gusto asprigno è particolarmente gustoso consumato crudo in insalata o cotto come gli spinaci.</p> <div class="product_description" itemprop="description"><ul class="single-portfolio-meta single-product-meta"><li><div class="type"><i class="fa fa-folder-open"></i>Famiglia</div><div class="value">Polygonaceae</div></li><li><div class="type"><i class="fa fa-calendar-check-o"></i>Durata</div><div class="value">Annuale</div></li><li><div class="type"><i class="fa fa-sun-o"></i>Dimora</div><div class="value">Pieno sole</div></li><li><div class="type"><i class="fa fa-exclamation-triangle"></i>Temperatura</div><div class="value">Rustica (da -5°c a -20°c e oltre)</div></li><li><div class="type"><i class="fa fa-tint"></i>Annaffiatura</div><div class="value">Moderata</div></li><li><div class="type"><i class="fa fa-tree"></i>Terreno</div><div class="value">Ben drenato</div></li></ul></div>',
            'tags': u'Erbacee',
            'published': u'true',
            'variants': [
                {
                    'grams': 1000,
                    'inventory_quantity': 0,
                    'inventory_policy': u'deny',
                    'fulfillment_service': u'manual',
                    'inventory_management': u'shopify',
                    'price': 5.0,
                    'requires_shipping': u'true'
                }
            ],
            'images': [
                {
                    'src': u'https://www.geelfloricultura.it/media/2016/07/acetosella-araba-rumex-versicarius.jpg',
                    'position': 1,
                    'alt_text': u'Acetosa araba'
                }
            ],
            'likely_cultivar_name': u'Rumex versicarius'
        }

        self.src_data_modified = self.src_data.copy()
        self.src_data_modified['title'] = 'New title'
        self.src_data_modified['body_html'] = 'new body'
        self.src_data_modified['likely_cultivar_name'] = 'foo'

        self.message = type('empty', (object,), {
            '_queue_url':'https://eu-central-1.queue.amazonaws.com/0000/dev_queue',
            'message_id': 'xxxxx',
            'message_attributes': message_attributes,
            'body': json.dumps(self.src_data)
        })
        self.message_modified = type('empty', (object,), {
            '_queue_url': 'https://eu-central-1.queue.amazonaws.com/0000/dev_queue',
            'message_id': 'xxxxx',
            'message_attributes': message_attributes,
            'body': json.dumps(self.src_data_modified)
        })



    def test_with_message(self):
        importer = SqsMessageImporter(self.shop)

        assert not importer._message

        with importer.with_message(self.message) as m:
            assert m == self.message
            assert importer._message == self.message

        assert not importer._message


    @patch('django_shopify_synchronizer.shopify_rest_api.shopify.Product', create=True, new_callable=ProductMock, conf={})
    def test_import(self, MockProduct):
        importer = SqsMessageImporter(self.shop)
        assert ExternalProductToShopifyProductMap.objects.count() == 0

        # res = importer.import_from_message(self.message)
        action, save_success, shopify_resource = importer.import_from_message(self.message)

        # assert res.status_code

        # action, save_success, shopify_resource = importer.run(self.src_data, self.shop, self.vendor)

        assert action == ACTION_CREATE
        assert save_success
        assert ExternalProductToShopifyProductMap.objects.count() == 1
        assert shopify_resource
        # print('shopify_resource',shopify_resource)
        # MockProduct.save.assert_called()

        product_map = ExternalProductToShopifyProductMap.objects.last()

        assert product_map
        # print('product_map.src_data')
        # pprint.pprint(product_map.src_data)
        #
        # print('self.src_data')
        # pprint.pprint(self.src_data)
        assert product_map.src_data == self.src_data

    @patch('django_shopify_synchronizer.shopify_rest_api.shopify.Product', create=True, new_callable=ProductMock, conf={})
    def test_skyp_if_already_imported(self, MockProduct):
        importer = SqsMessageImporter(self.shop)
        assert ExternalProductToShopifyProductMap.objects.count() == 0
        action, save_success, shopify_resource = importer.import_from_message(self.message)
        assert ExternalProductToShopifyProductMap.objects.count() == 1
        assert action == ACTION_CREATE

        action, save_success, shopify_resource = importer.import_from_message(self.message)
        assert ExternalProductToShopifyProductMap.objects.count() == 1
        assert action == ACTION_SKIP
        #
        #
        # # assert res.status_code
        #
        # # action, save_success, shopify_resource = importer.run(self.src_data, self.shop, self.vendor)
        #
        # assert action == ACTION_CREATE
        # assert save_success
        # assert shopify_resource
        # print('shopify_resource', shopify_resource)
        #
        # product_map = ExternalProductToShopifyProductMap.objects.last()
        #
        # assert product_map
        # print('product_map.src_data')
        # pprint.pprint(product_map.src_data)
        #
        # print('self.src_data')
        # pprint.pprint(self.src_data)
        # assert product_map.src_data == self.src_data

    @patch('django_shopify_synchronizer.shopify_rest_api.shopify.Product', create=True, new_callable=ProductMock, conf={})
    def test_skyp_if_already_imported_and_update(self, MockProduct):
        importer = SqsMessageImporter(self.shop)
        assert ExternalProductToShopifyProductMap.objects.count() == 0
        action, save_success, shopify_resource = importer.import_from_message(self.message)
        assert ExternalProductToShopifyProductMap.objects.count() == 1
        assert action == ACTION_CREATE
        assert shopify_resource.title == self.src_data['title']
        assert shopify_resource.body_html == self.src_data['body_html']

        action, save_success, shopify_resource = importer.import_from_message(self.message)
        assert ExternalProductToShopifyProductMap.objects.count() == 1
        assert action == ACTION_SKIP
        # assert shopify_resource.title == self.src_data['title']
        # assert shopify_resource.body_html == self.src_data['body_html']

        action, save_success, shopify_resource = importer.import_from_message(self.message_modified)
        assert ExternalProductToShopifyProductMap.objects.count() == 1
        assert action == ACTION_UPDATE

        # Check not update title and body_data
        assert shopify_resource.title == self.src_data['title']
        assert shopify_resource.body_html == self.src_data['body_html']
        assert shopify_resource.likely_cultivar_name == self.src_data_modified['likely_cultivar_name']





class TestImporter(TempMediaMixin, TestCase):
    def setUp(self):
        self.mock_product_id = 0
        self.shop1 = Shop.objects.create(
            domain='test-sync-s2s-dest.myshopify.com',
            api_key='a4f66378ca81584d75ea189da0c3b825',
            password='74bf7078426eab655bcf9a9b433840a3',
            shared_secret='a6c0e077d128b0e2375287607f68591c'
        )
        self.vendor = Vendor.objects.create(name='Vendor fake')

        file_path = '{}/tests/source_files/import-1 - products_export.csv'.format(BASE_DIR)
        file_path2 = '{}/tests/source_files/vivai-delle-commande.csv'.format(BASE_DIR)
        self.import_source = ImportSource.objects.create(
            vendor=self.vendor,
            file=File(open(file_path), name='import_file.csv')
        )
        self.import_source2 = ImportSource.objects.create(
            vendor=self.vendor,
            file=File(open(file_path2), name='import_file2.csv')
        )

        self.import_task = ImportTask.objects.create(import_source=self.import_source, target_shop=self.shop1)
        self.import_task_long = ImportTask.objects.create(import_source=self.import_source2, target_shop=self.shop1)

    def test_importer_init_fail_if_no_argouments(self):
        with pytest.raises(TypeError):
            importer = ImportTaskImporter()

    def test_importer_init_fail_if_no_import_task(self):
        with pytest.raises(ImproperlyConfigured):
            importer = ImportTaskImporter({})

    def test_importer_init(self):
        importer = ImportTaskImporter(self.import_task)
        assert importer.import_task == self.import_task

    def test_already_imported_vendor_product_id_empty(self):
        importer = ImportTaskImporter(self.import_task)
        assert importer.already_imported_vendor_product_id() == []

    def test_regroup(self):
        importer = ImportTaskImporter(self.import_task)

        products_to_be_imported, products_to_be_updated, products_to_be_removed = importer.regroup_sorce_products()

        assert len(products_to_be_imported) == 1
        assert len(products_to_be_updated) == 0
        assert len(products_to_be_removed) == 0

        assert products_to_be_imported == importer.source_products

    @patch('django_shopify_synchronizer.shopify_rest_api.shopify.Product', create=True, new_callable=ProductMock, conf={})
    def test_run(self, MockProduct):
        print('test run... MockProduct:', MockProduct, 'MockProduct.save:', MockProduct.save)

        importer = ImportTaskImporter(self.import_task)
        importer.run()

        # MockProduct.save.assert_called()
        assert self.import_task.is_finished
        assert self.import_task.processed_count == 1
        assert self.import_task.imported_count == 1
        assert self.import_task.updated_count == 0

    # @patch('django_shopify_synchronizer.shopify_rest_api.shopify.resources.product.Product.id', new_callable=PropertyMock)
    # @patch('django_shopify_synchronizer.shopify_rest_api.shopify.resources.product.Product.save')
    # def test_run_long(self, MockProductSave, MockProductId):
    #     def product_save_side_effect():
    #         return True
    #
    #     def id_side_effect():
    #         self.mock_product_id = self.mock_product_id + 1
    #         return self.mock_product_id
    #
    #     MockProductSave.side_effect = product_save_side_effect
    #     MockProductId.side_effect = id_side_effect
    #
    #     importer = ImportTaskImporter(self.import_task_long)
    #     importer.run()
    #
    #     assert self.import_task_long.is_finished
    #     assert self.import_task_long.processed_count == 141
    #     assert self.import_task_long.imported_count == 141
    #     assert self.import_task_long.updated_count == 0

    @pytest.mark.slow
    @patch('django_shopify_synchronizer.shopify_rest_api.shopify.Product', create=True, new_callable=ProductMock, conf={})
    def test_run_long(self, ProductMock):
        # p = Product.objects.create(shop=self.shop1, shopify_id='12345')
        # print('product:', p, p.id)
        # print('shop res', p.shopify_resource, p.shopify_resource.id)
        #
        # p2 = Product.objects.create(shop=self.shop1, shopify_id='99999')
        # print('product2:', p2, p2.id)
        # print('shop2 res', p2.shopify_resource, p2.shopify_resource.id)
        #
        # if True:
        #     return

        importer = ImportTaskImporter(self.import_task_long)
        importer.run()

        assert self.import_task_long.is_finished
        assert self.import_task_long.processed_count == 141
        assert self.import_task_long.imported_count == 141
        assert self.import_task_long.updated_count == 0


class TestImportAsync(TempMediaMixin, TestCase):
    def setUp(self):
        self.shop1 = Shop.objects.create(
            domain='test-sync-s2s-dest.myshopify.com',
            api_key='a4f66378ca81584d75ea189da0c3b825',
            password='74bf7078426eab655bcf9a9b433840a3',
            shared_secret='a6c0e077d128b0e2375287607f68591c'
        )
        self.vendor = Vendor.objects.create(name='Vendor fake')

    # @patch('django_shopify_synchronizer.shopify_rest_api.shopify.Product', create=True, new_callable=ProductMock, conf={})
    @pytest.mark.slow
    def test_run_task_import(self, p_mock=None):
        file_path = '{}/tests/source_files/import-1 - products_export.csv'.format(BASE_DIR)
        source = ImportSource.objects.create(
            vendor=self.vendor,
            file=File(open(file_path, 'rt'), name='import_file.csv')
        )

        import_task = ImportTask.objects.create(import_source=source, target_shop=self.shop1)

        # pprint.pprint(source.products)

        result = run_import_task(import_task.id)
        import_task.refresh_from_db()

        assert import_task.imported_count == 1
        assert import_task.processed_count == 1
        assert import_task.updated_count == 0
        assert import_task.failed_count == 0
        # assert len(result['imported']) == 1
        # assert result['import   zed'][0].title == 'Abelia Grandiflora in vaso da 16cm'


        # print(Product.objects.all())
        p = Product.objects.first()
        r = p.shopify_resource
        # pprint.pprint(r.__dict__)
        assert [t.strip() for t in r.tags.split(',')] == ['Pianta in vaso', 'Regalo', 'yg:cultivar:abelia-grandiflora']
        # print('r.tags', r.tags)


        file_path2 = '{}/tests/source_files/import-2-added-new-product - products_export.csv'.format(BASE_DIR)
        source2 = ImportSource.objects.create(
            vendor=self.vendor,
            file=File(open(file_path2, 'rt'), name='import_file2.csv')
        )

        import_task2 = ImportTask.objects.create(import_source=source2, target_shop=self.shop1)
        result2 = run_import_task(import_task2.id)
        import_task2.refresh_from_db()

        assert import_task2.imported_count == 1
        assert import_task2.processed_count == 2
        assert import_task2.updated_count == 1
        assert import_task2.failed_count == 0
        # assert len(import_task2.imported) == 1
        # assert len(import_task2.updated) == 1
        # assert import_task2.imported[0].title == 'Rosa canina - 12 semi'
        # assert import_task2.updated[0].title == 'Abelia Grandiflora in vaso da 16cm'

        r = p.shopify_resource
        pprint.pprint(r.__dict__)
        assert [t.strip() for t in r.tags.split(',')] == ['Pacco', 'Pianta in vaso', 'Regalo',
                                                          'yg:cultivar:abelia-grandiflora']

    # @patch('django_shopify_synchronizer.shopify_rest_api.shopify.Product', create=True, new_callable=ProductMock, conf={})
    @pytest.mark.slow
    def test_run_task_import_inventory(self, p_mock=None):
        file_path = '{}/tests/source_files/import-1 - products_export_inventory.csv'.format(BASE_DIR)
        source = ImportSource.objects.create(
            vendor=self.vendor,
            file=File(open(file_path, 'rt'), name='import_file.csv')
        )

        import_task = ImportTask.objects.create(import_source=source, target_shop=self.shop1)

        pprint.pprint(source.products)

        result = run_import_task(import_task.id)
        import_task.refresh_from_db()

        assert import_task.imported_count == 1
        assert import_task.processed_count == 1
        assert import_task.updated_count == 0
        assert import_task.failed_count == 0
        # assert len(result['imported']) == 1
        # assert result['import   zed'][0].title == 'Abelia Grandiflora in vaso da 16cm'

        # file_path2 = '{}/tests/source_files/import-2-added-new-product - products_export.csv'.format(BASE_DIR)
        # source2 = ImportSource.objects.create(
        #     vendor=self.vendor,
        #     file=File(open(file_path2), name='import_file2.csv')
        # )
        #
        # import_task2 = ImportTask.objects.create(import_source=source2, target_shop=self.shop1)
        # result2 = run_import_task(import_task2.id)
        # import_task2.refresh_from_db()
        #
        # assert import_task2.imported_count == 1
        # assert import_task2.processed_count == 2
        # assert import_task2.updated_count == 1
        # assert import_task2.failed_count == 0
        # # assert len(import_task2.imported) == 1
        # # assert len(import_task2.updated) == 1
        # # assert import_task2.imported[0].title == 'Rosa canina - 12 semi'
        # # assert import_task2.updated[0].title == 'Abelia Grandiflora in vaso da 16cm'

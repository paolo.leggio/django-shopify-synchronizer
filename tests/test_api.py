#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_django-shopify-synchronizer
------------

Tests for `django-shopify-synchronizer` models module.
"""
import pytest
from django.test import TestCase
import pprint
from django_shopify_synchronizer.helpers import get_api_for_domain
from django_shopify_synchronizer.models import Shop


class TestShopifyResource(TestCase):
    def setUp(self):
        self.shop1 = Shop.objects.create(
            domain='test-sync-s2s-dest.myshopify.com',
            api_key='a4f66378ca81584d75ea189da0c3b825',
            password='74bf7078426eab655bcf9a9b433840a3',
            shared_secret='a6c0e077d128b0e2375287607f68591c'
        )

    def tearDown(self):
        pass

    def test_find_all(self):
        api = get_api_for_domain('test-sync-s2s-dest.myshopify.com')
        res = api.Product.find_all()
        # for p in res:
        #     print(p)
        # todo: mock url request



@pytest.mark.slow
class TestShopifyApi(TestCase):
    def setUp(self):
        self.shop1 = Shop.objects.create(
            domain='test-sync-s2s-dest.myshopify.com',
            api_key='a4f66378ca81584d75ea189da0c3b825',
            password='74bf7078426eab655bcf9a9b433840a3',
            shared_secret='a6c0e077d128b0e2375287607f68591c'
        )

        self.shop2 = Shop.objects.create(
            domain='test-sync-s2s-source.myshopify.com',
            api_key='451a6bb8807ac28369cf26fd6483830b',
            password='f09d7eb4f4acb46398f38e6fdd1feea4',
            shared_secret='a46b674a8c91afd69c8e2b01a7ffb2bd'
        )

    def tearDown(self):
        pass

    def test_product_get_variant_from_options(self):
        api = get_api_for_domain('test-sync-s2s-dest.myshopify.com')

        product = api.Product()
        product.variants = [
            api.Variant({
                "option1": "Pink",
                "option2": "Small",
                "price": 199.99,
            }),
            api.Variant({
                "option1": "Red",
                "option2": "Small",
                "price": 199.99,
            }),
            api.Variant({
                "option1": "Green",
                "option2": "Medium",
                "price": 199.99,
            }),
            api.Variant({
                "option1": "Red",
                "option2": "Medium",
                "price": 199.99,
            }),
        ]

        assert product.find_variant_by_options({
            "option1": "Red",
            "option2": "Medium",
        }).attributes == {
                   "option1": "Red",
                   "option2": "Medium",
                   "price": 199.99,
               }

    def test_create(self):
        api = get_api_for_domain('test-sync-s2s-dest.myshopify.com')

        product = api.Product()
        product.title = 'title'
        product.save()

        assert product.id

        # test create size options

        product.options = [
            {
                'name': 'Color',
            },
            {
                'name': 'Size',
            },
        ]

        product.variants = [
            {
                "option1": "Pink",
                "option2": "Small",
                "price": 199.99,
            },
            {
                "option1": "Red",
                "option2": "Small",
                "price": 199.99,
            },
            {
                "option1": "Green",
                "option2": "Medium",
                "price": 199.99,
            },
            {
                "option1": "Red",
                "option2": "Medium",
                "price": 199.99,
            },

        ]

        saved = product.save()
        if not saved:
            print(product.errors.full_messages())
        assert saved
        assert len(product.variants) == 4

        product.variants = [
            {
                "option1": "Pink",
                "option2": "Small",
                "price": 199.99,
            },
        ]
        saved = product.save()
        if not saved:
            print(product.errors.full_messages())
        assert saved
        assert len(product.variants) == 1

    # def test_sync_in_shop(self):
    #     self.shop2.sync_in_shop(self.shop1)

    # def test_collection(self):
    #     print(self.shop2.api.Collection.find())

    def test_metafields(self):

        api = get_api_for_domain('test-sync-s2s-dest.myshopify.com')

        product = api.Product()
        product.title = 'title'
        product.save()
        product.add_metafield(api.Metafield({
            "key": "long_description",
            "value": "test",
            "value_type": "string",
            "namespace": "prd_long_description"
        }))
        # pprint.pprint(product.__dict__)
        print('product.metafields( )', product.metafields())

        p2 = api.Product.find(product.id)
        # pprint.pprint(p2.__dict__)
        print('p2.metafields()', p2.metafields())
        pprint.pprint([m.__dict__ for m in p2.metafields()])

    # def test_api_limit(self):
    #
    #     api = get_api_for_domain('test-sync-s2s-dest.myshopify.com')
    #
    #     for p in api.Product.find():
    #         print('p',p)
    #         smart_collections = api.SmartCollection.find(product_id=p.id)

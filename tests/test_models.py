#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_django-shopify-synchronizer
------------

Tests for `django-shopify-synchronizer` models module.
"""

import os
from django.test import TestCase
from django.core.exceptions import ImproperlyConfigured
import pytest
from mock.mock import patch, call
from django_shopify_synchronizer.models import Shop, Product, Customer, Vendor, ImportSource, ImportTask, \
    ExternalProductToShopifyProductMap
from django.core.files import File
from tests.common import TempMediaMixin
from django.test.utils import override_settings
from django_shopify_synchronizer.factories.common import UserFactory
from django_shopify_synchronizer.shopify_rest_api import shopify

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class TestShopModel(TestCase):
    def test_base_shop_model(self):
        shop = Shop(domain='shop.domain')
        shop.save()

        assert shop.id
        assert shop.domain == 'shop.domain'

    def test_shop_model_unique_primary(self):
        shop = Shop(domain='shop.domain', primary=True)
        shop.save()

        assert shop.id
        assert shop.domain == 'shop.domain'
        assert shop.primary

        shop2 = Shop(domain='shop2.domain', primary=True)
        shop2.save()
        shop.refresh_from_db()
        assert shop2.primary
        assert shop.primary == False

        assert Shop.objects.primary() == shop2

    def test_shop_api_unconfigured(self):
        shop = Shop(domain='shop.domain')
        shop.save()

        with pytest.raises(ImproperlyConfigured):
            shop.api

    def test_shop_api_configured(self):
        API_KEY = 'apikey'
        PASSWORD = 'apikey'
        DOMAIN = 'shop.domain'

        shop = Shop(domain=DOMAIN)
        shop.api_key = API_KEY
        shop.password = PASSWORD
        shop.save()

        expected_admin_url = 'https://{}:{}@{}/admin'.format(API_KEY, PASSWORD, DOMAIN)
        assert shop.api.ShopifyResource._site == expected_admin_url
        assert shop.api.ShopifyResource.site == expected_admin_url
        assert shop.api.ShopifyResource.get_site() == expected_admin_url

        assert shop.api.ShopifyResource.user == API_KEY
        assert shop.api.ShopifyResource.password == PASSWORD

    @patch('django_shopify_synchronizer.shopify_rest_api.shopify.Webhook')
    def test_setup_webhook(self, mockWebhook):
        mockWebhook.find.return_value = []

        active_topics = ['products/update', 'products/create', 'products/delete', 'collections/update',
                         'collections/create', 'collections/delete']

        API_KEY = 'apikey'
        PASSWORD = 'apikey'
        DOMAIN = 'shop.domain'
        shop = Shop(domain=DOMAIN)
        shop.api_key = API_KEY
        shop.password = PASSWORD
        shop.save()

        shop.setup_webhook(base_url='https://127.0.0.1')
        assert mockWebhook.find.call_count == 2
        assert mockWebhook.create.call_count == len(active_topics)

        expected_calls = []
        for t in active_topics:
            expected_calls.append(call({u'topic': t, u'format': u'json', u'address': u'https://127.0.0.1/webhook/'}))

        mockWebhook.create.assert_has_calls(expected_calls, any_order=True)


class TestProductModel(TestCase):
    def setUp(self):
        self.shop1 = Shop.objects.create(domain='shop1.domain')
        self.shop2 = Shop.objects.create(domain='shop2.domain')
        self.shop3 = Shop.objects.create(domain='shop3.domain')
        pass

    def test_product_new(self):
        product = Product()

    def test_base_product_model(self):
        # todo: Non testo il field data perche funziona solo su postgresql. usando sqlite durante i test fallirebbe
        custom_data = {
            'foo': 'bar'
        }
        product_shop1 = Product.objects.create(shopify_id='1234', shop=self.shop1, data=custom_data)

        assert product_shop1.id
        assert product_shop1.shop == self.shop1
        assert product_shop1.data == custom_data

        assert Product.objects.in_shop(self.shop1).count() == 1
        assert Product.objects.in_shop(self.shop1).last() == product_shop1


class TestCustomerModel(TestCase):
    def setUp(self):
        self.shop1 = Shop.objects.create(domain='shop1.domain')
        pass

    def test_product_new(self):
        customer = Customer()

    def test_base_customer_model(self):
        # todo: Non testo il field data perche funziona solo su postgresql. usando sqlite durante i test fallirebbe
        custom_data = {
            'foo': 'bar'
        }
        user = UserFactory()
        customer = Customer.objects.create(shopify_id='1234', shop=self.shop1, data=custom_data, email='foo@bar.it',
                                           password='password', user=user)

        assert customer.id
        assert customer.shop == self.shop1
        assert customer.data == custom_data
        assert customer.user == user
        assert customer.email == 'foo@bar.it'
        assert customer.password == 'password'

        assert Customer.objects.in_shop(self.shop1).count() == 1
        assert Customer.objects.in_shop(self.shop1).last() == customer

    def test_update_user_data_on_model_save(self):
        custom_data = {
            'bar': 'foo',

        }
        user = UserFactory()
        orig_first_name = user.first_name
        orig_last_name = user.last_name
        customer = Customer.objects.create(shopify_id='1234', shop=self.shop1, data=custom_data, email='foo@bar.it',
                                           password='password', user=user)


        user.refresh_from_db()
        assert user.first_name == orig_first_name
        assert user.last_name == orig_last_name

        customer.data =  {
            'first_name': 'Peppa',
            'last_name': 'Pig',
        }
        customer.save()
        assert user.first_name == 'Peppa'
        assert user.last_name == 'Pig'
    


class TestVendorModel(TestCase):
    def test_base_model(self):
        vendor = Vendor.objects.create(name='vendor name')
        assert vendor.id
        assert vendor.name == 'vendor name'

    def test_def_fulfilment_name_not_setted(self):
        vendor = Vendor.objects.create(name='vendor name')
        assert vendor.fulfillment_name == 'vendor name'

    def test_def_fulfilment_name(self):
        vendor = Vendor.objects.create(name='vendor name', fulfillment_name='ff_name')
        assert vendor.fulfillment_name == 'ff_name'

    def test_def_fulfilment_email_(self):
        vendor = Vendor.objects.create(name='vendor name', fulfillment_email='ff_name@me.com')
        assert vendor.fulfillment_email == 'ff_name@me.com'

    @override_settings(SHIPIFY_DEAFULT_FULFILLMENT_EMAIL='default_email@shop.com')
    def test_def_fulfilment_email_not_setted(self):
        vendor = Vendor.objects.create(name='vendor name')
        assert vendor.fulfillment_email == 'default_email@shop.com'


class TestImportSourceModel(TempMediaMixin, TestCase):
    def setUp(self):
        self.vendor = Vendor.objects.create(name='Vendor fake')

    def test_base_model(self):
        file_path = '{}/tests/source_files/import-1 - products_export.csv'.format(BASE_DIR)
        source = ImportSource.objects.create(
            vendor=self.vendor,
            file=File(open(file_path, 'rt'), name='import_file.csv')
        )

        assert source.vendor == self.vendor

    def test_file_extension(self):
        file_path = '{}/tests/source_files/import-1 - products_export.csv'.format(BASE_DIR)
        source = ImportSource.objects.create(
            vendor=self.vendor,
            file=File(open(file_path, 'rt'), name='import_file.csv')
        )

        assert source.file_extension() == 'csv'

    def test_products(self):
        file_path = '{}/tests/source_files/import-1 - products_export.csv'.format(BASE_DIR)
        source = ImportSource.objects.create(
            vendor=self.vendor,
            file=File(open(file_path), name='import_file.csv')
        )
        assert isinstance(source.products, (list))
        assert len(source.products) == 1


class TestImportTaskModel(TempMediaMixin, TestCase):
    def setUp(self):
        self.shop1 = Shop.objects.create(
            domain='test-sync-s2s-dest.myshopify.com',
            api_key='a4f66378ca81584d75ea189da0c3b825',
            password='74bf7078426eab655bcf9a9b433840a3',
            shared_secret='a6c0e077d128b0e2375287607f68591c'
        )
        self.vendor = Vendor.objects.create(name='Vendor fake')

        file_path = '{}/tests/source_files/import-1 - products_export.csv'.format(BASE_DIR)
        source = ImportSource.objects.create(
            vendor=self.vendor,
            file=File(open(file_path, 'rt'), name='import_file')
        )

        self.import_task = ImportTask.objects.create(import_source=source, target_shop=self.shop1)

    def test_base_model(self):
        assert self.import_task.id
        assert not self.import_task.in_progress
        assert not self.import_task.is_finished

    def test_initial_started(self):
        assert not self.import_task.started

    def test_set_started(self):
        self.import_task.set_started()
        assert self.import_task.started
        assert self.import_task.in_progress

    def test_set_stopped_fail_if_not_started(self):
        with pytest.raises(Exception):
            self.import_task.set_stopped()

    def test_set_stopped(self):
        self.import_task.set_started()
        self.import_task.set_stopped(True)
        assert self.import_task.stopped
        assert not self.import_task.in_progress


class TestExternalProductToShopifyProductMapModel(TempMediaMixin, TestCase):
    def setUp(self):
        self.shop1 = Shop.objects.create(
            domain='test-sync-s2s-dest.myshopify.com',
            api_key='a4f66378ca81584d75ea189da0c3b825',
            password='74bf7078426eab655bcf9a9b433840a3',
            shared_secret='a6c0e077d128b0e2375287607f68591c'
        )
        self.vendor = Vendor.objects.create(name='Vendor fake')

    def test_delete_product(self):
        product = Product.objects.create(shop=self.shop1, shopify_id='100')
        map = ExternalProductToShopifyProductMap.objects.create(vendor=self.vendor, vendor_product_id='v-100',
                                                                product=product)

        assert ExternalProductToShopifyProductMap.objects.count() == 1

        product.delete()
        assert ExternalProductToShopifyProductMap.objects.count() == 0
